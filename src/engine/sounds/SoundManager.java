package engine.sounds;

import engine.Constants;
import engine.logics.ResourcesManager;
import javafx.scene.media.*;
import javafx.util.Duration;

public class SoundManager
{
	// music to be stored in verse
	private static boolean		musicChanged;	// flag indicating the game music is about to change
	// sound controling variables	
	private static int			musicVolume; 	// background music volume
	private static int			soundVolume;	// sound effects volume
	private static int			voiceVolume;	// character voices volume
	// players for various media types
	private static MediaPlayer 	musicPlayer;	// background music
	private static MediaPlayer 	soundPlayer;	// sound effects
	private static MediaPlayer 	voicePlayer;	// character voices
	private static MediaPlayer 	systemPlayer;	// system sounds
	// helping variables for game music stopping and resuming
	private static MediaPlayer 	musicPaused;	// stores paused music
	private static Duration 	musicDuration;	// stores pause time
	// predefined sounds for system use
	private static String		menuMusic;		// path to main menu music
	private static String		clickSound;		// path to click sound
	private static String		voiceSample;	// path to voice sample
	
	// --- SETTERS ---	
	public static void setMenuMusic(String m)
	{	menuMusic= m; }
	
	public static void setClickSound(String s)
	{	clickSound = s; }
	
	public static void setVoiceSample(String s)
	{	voiceSample = s; }
	
	public static void setSoundVolume(int type, int v)
	{	
		if (type == Constants.TYPE_MUSIC)
		{	musicVolume = v; }
		else if (type == Constants.TYPE_SOUND)
		{	soundVolume = v; }
		else if (type == Constants.TYPE_VOICE)	
		{	voiceVolume = v;}
	}
	
	public static void toggleMusicChanged(boolean b)
	{	musicChanged = b; }
	
	// --- GETTERS ---	
	public static int getSoundVolume(int type)
	{	
		if (type == Constants.TYPE_MUSIC)
		{	return musicVolume; }
		else if (type == Constants.TYPE_SOUND)
		{	return soundVolume; }
		else if (type == Constants.TYPE_VOICE)	
		{	return voiceVolume;}
		return 0;
	}
	
	public static boolean isMusicChanged()
	{	return musicChanged; }
	
	public static boolean musicDiffers(String path)
	{
		String url = ResourcesManager.getResourceURL(path);
		
		if (musicPlayer!= null && musicPlayer.getMedia().getSource().equals(url))
		{	return false; }
		else
		{	return true;	}
	} 
	
	// --- METHODS ---	
	public static void playMusic(String path)
	{
		try
		{
			String url = ResourcesManager.getResourceURL(path);  	// get the url to the music
			
			if ( url.isEmpty())														// if the url is invalid
			{	System.err.println("The specified audio file doesn't exist");	}	// print the error message
			else
			{
				musicPlayer  = new MediaPlayer(new Media(url));				// if the url is valid, load the music
				musicPlayer.setVolume(0.01 * musicVolume);
				musicPlayer.setAutoPlay(true);
				musicPlayer.setOnEndOfMedia		// loop music until stopped by command
		        (
		        		new Runnable()
		        		{    
		        			public void run()
		        			{ musicPlayer.seek(Duration.ZERO); }
		        		}
		        );
			}
		}
		catch (MediaException e)
		{ System.err.println("Error when trying to load the specified audio file");	}		
	}
	
	public static void playSound(String path)
	{
		try
		{
			String url = ResourcesManager.getResourceURL(path);  	// get the url to the sound
			
			if ( url.isEmpty())														// if the url is invalid
			{	System.err.println("The specified audio file doesn't exist");	}	// print the error message
			else
			{
				soundPlayer  = new MediaPlayer(new Media(url));						// if the url is valid, load the sound
				soundPlayer.setVolume(0.01 * soundVolume);
				soundPlayer.play();
			}
		}
		catch (MediaException e)
		{ System.err.println("Error when trying to load the specified audio file");	}		
	}
	
	public static void playVoice(String path)
	{
		if (ResourcesManager.isVoiceEnabled())
		{
			try
			{
				String url = ResourcesManager.getResourceURL(path);  	// get the url to the voice
				
				if ( url.isEmpty())														// if the url is invalid
				{	System.err.println("The specified audio file doesn't exist");	}	// print the error message
				else
				{
					voicePlayer  = new MediaPlayer(new Media(url));		// if the url is valid, load the voice
					voicePlayer.setVolume(0.01 * voiceVolume);
					voicePlayer.play();
				}
			}
			catch (MediaException e)
			{ System.err.println("Error when trying to load the specified audio file");	}		
		}
	}
	
	public static void playSystemSound(int type, String path)
	{
		try
		{
			String url = ResourcesManager.getResourceURL(path);  	// get the url to the system sound
			
			if ( url.isEmpty())														// if the url is invalid
			{	System.err.println("The specified audio file doesn't exist");	}	// print the error message
			else
			{
				systemPlayer = new MediaPlayer(new Media(url));						// if the url is valid, load the sound
				if (type == Constants.TYPE_SOUND)			// if its normal system sound
				{ systemPlayer.setVolume(0.01 * soundVolume); }
				else if (type == Constants.TYPE_VOICE)	// if it's voice playback preview
				{ systemPlayer.setVolume(0.01 * voiceVolume); }
				systemPlayer.play();			
			}
		}
		catch (MediaException e)
		{	System.err.println("Error when trying to load the specified audio file");	}	
	}
	
	public static void playMenuMusic()
	{	
		if (menuMusic != null)
		{	playMusic(menuMusic); }
	}
	
	public static void playClickSound()
	{	
		if (clickSound != null)
		{	playSystemSound(Constants.TYPE_SOUND, clickSound); }
	}
	
	public static void playVoiceSample()
	{	
		if (voiceSample != null)
		{	playSystemSound(Constants.TYPE_VOICE, voiceSample); }
	}
	
	
	public static void updateMusicVolume()
	{
		if (musicPlayer != null)
		{ musicPlayer.setVolume(0.01 * musicVolume); }
	}
	
	public static void stopMusic()
	{ 
		if (musicPlayer != null)
		{ musicPlayer.stop(); }
	}
	
	public static void stopSound()
	{ 
		if (soundPlayer != null)
		{ soundPlayer.stop(); }
	}
	
	public static void stopVoice()
	{ 
		if (voicePlayer != null)
		{ voicePlayer.stop(); }
	}
	
	public static void clearSceneSound()
	{
		if (soundPlayer != null)
		{ soundPlayer.stop(); }
		
		if (voicePlayer != null)
		{ voicePlayer.stop(); }
		
		soundPlayer = null;
		voicePlayer = null;
	}
	
	public static void clearGameSound()
	{ 
		if (musicPlayer != null)
		{ musicPlayer.stop(); }
		
		if (soundPlayer != null)
		{ soundPlayer.stop(); }
		
		if (voicePlayer != null)
		{ voicePlayer.stop(); }
		
		musicPlayer = null;
		musicPaused = null;
		soundPlayer = null;
		voicePlayer = null;
		
		musicDuration = Duration.ZERO;
	}
	
	public static void pauseGameSound()
	{ 
		if (musicPlayer != null)
		{ 
			musicDuration = musicPlayer.getCurrentTime();
			musicPaused   = musicPlayer;
			musicPlayer.stop();
		}
		
		if (soundPlayer != null)
		{ soundPlayer.stop(); }
		
		if (voicePlayer != null)
		{ voicePlayer.stop(); }
	}
	
	public static void resumeGameSound()
	{ 
		// resume game music
		if (musicPaused != null)
		{ 
			musicPlayer = musicPaused;
			musicPlayer.setVolume(0.01 * musicVolume);
			musicPlayer.setStartTime(musicDuration);	// set start time to a moment the playback stopped
			musicPlayer.play();
			musicPlayer.setStartTime(Duration.ZERO);	// reset the start time for the next repetitions 			
		}
		
		// resume game sound
		if (soundPlayer != null)
		{ soundPlayer.play(); }
		
		// resume game voice
		if (voicePlayer != null)
		{ voicePlayer.play(); }
	}	
}
