package engine.sounds.elements;

import java.io.IOException;
import java.io.Serializable;

import datatypes.Data;
import datatypes.Memorizable;
import datatypes.Memory;
import interpreter.exceptions.InterpreterException;

public class Sound implements Memorizable, Serializable
{
	private static final long serialVersionUID = 8132431530182672011L;

	private static int 		defId = 0;

	private String 			id;
	private String 			path;
	
	
	// ----- CONSTRUCTORS -----
	
	public Sound(String p)
	{
		defId++;
		this.id = "Sound_" + String.format("%03d", defId);
		this.path = p;
	}
	
	public Sound(String i, String p)
	{
		this.id = i;
		this.path = p;
	}
	
	// ----- GETTERS -----
	
	public String getId()
	{ return id; }
	
	
	public String getPath()
	{ return path; }
		
	// ----- SETTERS -----
	
	public void setPath(String p)
	{ this.path = p; }
	
	
	// ----- INTERFACE FUNCTIONS -----
	
	@Override
	public void define(Memory m) throws InterpreterException
	{
		if (!m.isDefined(this.getId()))
		{ m.write(this.getId(), new Data(this)); }
		else
		{ throw new InterpreterException("Sound already defined"); }		
	}
	
	@Override
	public String defineSelf()
	{
		String def = "define sound (" + this.getId() + ", " + this.getPath() + ")";
		return def;
	}
	
	@Override
	public String defineType()
	{ return "sound"; }
	
	@Override
	public String getNameString()
	{ return id; }
	
    private void writeObject(java.io.ObjectOutputStream stream) throws IOException 
    { 
    	stream.writeInt	   (defId);
    	
    	stream.writeObject (id);
    	stream.writeObject (path);
    }
    
	private void readObject(java.io.ObjectInputStream stream) throws IOException, ClassNotFoundException
    {
		defId 	= stream.readInt();
		
    	id 		= (String)stream.readObject();
    	path	= (String)stream.readObject();
    }
}
