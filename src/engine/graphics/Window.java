package engine.graphics;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;

import engine.Constants;
import engine.LogicManager;
import engine.exceptions.GameEngineException;
import engine.graphics.dialogs.*;
import engine.graphics.panels.*;
import engine.logics.ResourcesManager;
import engine.logics.SaveManager;
import engine.logics.elements.Verse;
import engine.sounds.SoundManager;

public class Window extends JFrame implements KeyListener, MouseListener, MouseWheelListener, ActionListener, ChangeListener
{
	private static final long serialVersionUID = 110331739544255827L;
	
	private boolean 	fullscreen;			// true if window is in fullscreen mode
	private boolean		ingame;				// true if ingame panel is active
	private boolean		initialized;		// true if window is fully initialized and prepared
	private boolean		advancing;			// true if advancing by RMB clicking is enabled
	private boolean 	terminating;		// true if game is about to terminate
	
	private Panels[]  	panels;				// array of panels
	private Dialogs[]  	dialogs;			// array of dialogs
	private Panels	 	activePanel;		// actually visible panel to be drawn
	private int			currentPanel;		// number of actually visible panel	
	private	int			currentDialog;		// number of actually visible dialog
	private	int			previousDialog;		// number of previously visible dialog

	private Dimension    scrSize;
	
	private Dimension	 minSize;
	private Dimension	 prefSize;
	private Dimension	 actSize;
	private Point		 actLoc;

	public Window()
	{		
		initialized = false;
		
        scrSize = Toolkit.getDefaultToolkit().getScreenSize();
        adjustRatio();
        
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
    	setMinimumSize(minSize);
        setSize(prefSize);

        int x = (int)((scrSize.getWidth()  - getWidth())  / 2);
        int y = (int)((scrSize.getHeight() - getHeight()) / 2);
        setLocation(x, y);
        
		panels = new Panels[6];
		panels[Constants.P_MAIN_MENU]    = new PanelMenuMain(this);	
		panels[Constants.P_NARRATION]    = new PanelNarration(this);		
		panels[Constants.P_INGAME_MENU]  = new PanelMenuInGame(this);
		panels[Constants.P_OPTIONS_MENU] = new PanelMenuOptions(this);
		panels[Constants.P_SAVES_MENU] 	 = new PanelMenuSaveLoad(this);
		panels[Constants.P_EXTRAS_MENU]  = new PanelMenuExtras(this);
		
		dialogs = new Dialogs[5];
		dialogs[Constants.DG_EXIT_MM]    = new DialogExitMain(this);	
		dialogs[Constants.DG_EXIT_IM]    = new DialogExitInGame(this);		
		dialogs[Constants.DG_OPTIONS] 	 = new DialogOption(this);
		dialogs[Constants.DG_INPUT] 	 = new DialogInput(this);
		dialogs[Constants.DG_CONFIRM] 	 = new DialogConfirm(this);
		
		getContentPane().add(panels[Constants.P_MAIN_MENU]);
		
		previousDialog 	= Constants.DG_NONE;
		currentDialog 	= Constants.DG_NONE;
		currentPanel 	= Constants.P_MAIN_MENU;
		activePanel = panels[currentPanel];		// set game panel as visible by default
		
		fullscreen 	= false;
		ingame		= false;
		advancing 	= false;
		terminating = false;
		
		try 
		{	SaveManager.init(((PanelNarration)panels[Constants.P_NARRATION]), ((PanelMenuSaveLoad)panels[Constants.P_SAVES_MENU])); }
		catch (GameEngineException e)
		{ System.err.println(e.getDescription() + ". Occurred in block " + e.getComponent()); }

        addKeyListener(this);
        addMouseListener(this);
        addMouseWheelListener(this);
    }
	
	// GETTERS
	
	public boolean isFullscreen()
	{ 	return fullscreen; }
	
	public Panels getPanel(int n)
	{ 	return panels[n];	}
	
	public Dimension getScreenSize()
	{ 	return getSize(); }
	
	public Dimension getPreferredRes()
	{ 	return prefSize; }
	
	public double getScaleFactor()
	{
		return ((Math.sqrt(Math.pow(getHeight(), 2)
						 + Math.pow(getWidth(),  2)))/
				(Math.sqrt(Math.pow(prefSize.getHeight(), 2)
						 + Math.pow(prefSize.getWidth(),  2))));
	}
	
	public Font getScalledFont(Font font, int base)
	{ 	return new Font(font.getFontName(), Font.PLAIN, (int)(base * getScaleFactor())); }
	
	public ImageIcon getScalledIcon(int base, ImageIcon i)
	{ 	return GraphicsManager.resizeImage
				(	i,
					(int)(base * getScaleFactor()),
					(int)(base * getScaleFactor())); 
	}
	
	public String getInputText()
	{ 	return ((DialogInput)dialogs[Constants.DG_INPUT]).getAnswer(); }
	
	// --- SETTERS ---	
    public void setSaveLoadMenuContext(boolean b)
    { 	((PanelMenuSaveLoad)panels[Constants.P_SAVES_MENU]).setContext(b); }
    
    public void setConfirmationDialogContext(boolean b, int i)
    { 	((DialogConfirm)dialogs[Constants.DG_CONFIRM]).setContext(b, i); }
    
	public void setQuestion(int dialog, String q)
	{ 
		if (dialog == Constants.DG_INPUT)
		{	((DialogInput)dialogs[Constants.DG_INPUT]).setQuestion(q); }
		
		if (dialog == Constants.DG_OPTIONS)
		{	((DialogOption)dialogs[Constants.DG_OPTIONS]).setQuestion(q);}
	}
	
	public void setOptions(String[] opt)
	{ 	((DialogOption)dialogs[Constants.DG_OPTIONS]).setOptions(opt); }
	
	public void setContent(Verse v)
	{ 	((PanelNarration)panels[Constants.P_NARRATION]).setContent(v); }
	
	public void setDependentContent(Verse v, String s)
	{ 	((PanelNarration)panels[Constants.P_NARRATION]).setDependentContent(v, s); }
    
    public void update()
    {
    	for (Panels p : panels)
    	{ 
    		if (p != null)
    		{	p.update(); }
    	}
    	for (Dialogs p : dialogs)
    	{ 
    		if (p != null)
    		{	p.update(); }
    	}
    }
    
    public void adjustSlider(int s, int v)
    {	((PanelMenuOptions)panels[Constants.P_OPTIONS_MENU]).adjustSlider(s, v);	}
    
	private void adjustRatio()
	{
        int ratio = (int)(10*(scrSize.getWidth()/scrSize.getHeight()));
        switch(ratio)
        {
	        case 16:	// 16:10
	        	minSize = new Dimension(640, 400);
	        	prefSize = new Dimension(1024, 640);
	            break;
	        case 17:	// 16:9
	        	minSize = new Dimension(640, 360);
	        	prefSize = new Dimension(1024, 576);
	            break;
	        default:   // 13 = 4:3
	        	minSize = new Dimension(640, 480);
	            prefSize = new Dimension(1024, 768); 
        }
	}
    
	public void loadScene()
    {
		GraphicsManager.setBackground(LogicManager.getMemory().getActiveBackground());
		
		GraphicsManager.setSprite(Constants.POS_LEFT,   LogicManager.getMemory().getActiveSprite(Constants.POS_LEFT));
		GraphicsManager.setSprite(Constants.POS_CENTER, LogicManager.getMemory().getActiveSprite(Constants.POS_CENTER));
		GraphicsManager.setSprite(Constants.POS_RIGHT,  LogicManager.getMemory().getActiveSprite(Constants.POS_RIGHT));
		update();

		if (LogicManager.getMemory().getActiveMusic() != null)
		{	SoundManager.playMusic(LogicManager.getMemory().getActiveMusic().getPath()); }
		if (LogicManager.getMemory().getActiveSound() != null)
		{ SoundManager.playSound(LogicManager.getMemory().getActiveSound().getPath()); }
		if (LogicManager.getMemory().getActiveVoice() != null)
		{ SoundManager.playVoice(LogicManager.getMemory().getActiveVoice().getPath()); }	
    }
 
    public void showMainMenu(boolean b)
    {
    	if (b)
    	{	((PanelMenuMain)panels[Constants.P_MAIN_MENU]).showMenu(); }
    	else
    	{	((PanelMenuMain) panels[Constants.P_MAIN_MENU]).hideMenu(); }
    }
    
    public void showInputDialog(String q)
    {
		setQuestion(Constants.DG_INPUT, q);
		hideNarration();
		showDialog(Constants.DG_INPUT);
    }
    
    public void showEmptyInputAlert(boolean b)
    { 	((DialogInput)dialogs[Constants.DG_INPUT]).showAlert(b); }
    
    public void clearInputText()
    { 	((DialogInput)dialogs[Constants.DG_INPUT]).clearInput(); }    
    
    public void showOptionsDialog(String q, String[] opt)
    {
		setQuestion(Constants.DG_OPTIONS, q);
		setOptions(opt);
		hideNarration();
		showDialog(Constants.DG_OPTIONS);
    }
	
    public void showDialog(int dialog)
    {
		if(currentPanel == Constants.P_NARRATION)
		{ hideNarration(); }
 
		previousDialog = currentDialog;
		currentDialog  = dialog;
		panels[currentPanel].setDialog(dialogs[dialog]);
		panels[currentPanel].getDialog().setVisible(true);
    }
    
    public void hideDialog()
    { 
    	Dialogs d = panels[currentPanel].getDialog();
    	currentDialog  = previousDialog;
    	previousDialog = Constants.DG_NONE;
    	
    	if ( d != null)
    	{
    		d.clear();
		    d.setVisible(false);
		    if(currentPanel == Constants.P_NARRATION)
			{	showNarration(); }
    	}
    }
    
    public void showNarration()
    {
    	if ( currentPanel == Constants.P_NARRATION)
    	{	((PanelNarration)panels[Constants.P_NARRATION]).showNarration(true); }    	
    }
    
    public void hideNarration()
    {
    	if ( currentPanel == Constants.P_NARRATION)
    	{	((PanelNarration)panels[Constants.P_NARRATION]).showNarration(false); }   
    }
    
    public void toggleFullscreen(boolean fscreen)
    {
		if (fscreen)
		{
			actSize 	= getSize();
			actLoc  	= getLocation();

			dispose(); 
			setUndecorated(true);
			setBounds(0,0,scrSize.width,scrSize.height);
			setVisible(true);
			
			fullscreen 	= true;
		}
		else
		{
			dispose();
			setUndecorated(false);
			
			if (actLoc == null)
			{
				actSize = prefSize;
		        int x = (int)((scrSize.getWidth()  - prefSize.getWidth())  / 2);
		        int y = (int)((scrSize.getHeight() - prefSize.getHeight()) / 2);
				actLoc  = new Point (x,y);
			}
			
			setBounds( (int)actLoc.getX(), 		(int)actLoc.getY(),
					   (int)actSize.getWidth(), (int)actSize.getHeight());
			setVisible(true);
			
			fullscreen = false;
		}

    	((PanelMenuOptions)panels[Constants.P_OPTIONS_MENU]).toggleFullscreenMarked();    	
    }
    
    public void toggleInitialized(boolean b)
    { 	initialized = b; }
    
    public void toggleAdvancing(boolean b)
    { 	advancing = b; }
    
    public void toggleTerminating(boolean b)
    {	terminating = b; }
    
    public void setIngameMenuButtonsActive(boolean b)
    { 	((PanelMenuInGame)panels[Constants.P_INGAME_MENU]).setButtonsActive(b); }
    
    public void setSaveLoadMenuButtonsActive(boolean b)
    { 	((PanelMenuSaveLoad)panels[Constants.P_SAVES_MENU]).setButtonsActive(b); }
    
    public void changePanel(int newPanel)
    { 	 
    	currentPanel = newPanel;
    	remove(activePanel);
		activePanel = panels[currentPanel];					
		add(activePanel);
    }
    
    public void switchIngameMenu()
    {
    	if (currentPanel == Constants.P_NARRATION)
		{
			toggleAdvancing(false);
			changePanel(Constants.P_INGAME_MENU);
			
			SoundManager.pauseGameSound();
			SoundManager.playMenuMusic();
			SoundManager.playClickSound();	// play click sound
		}
		else if (currentPanel == Constants.P_INGAME_MENU)
		{
			changePanel(Constants.P_NARRATION);
			
			SoundManager.stopMusic();
			SoundManager.resumeGameSound();
			SoundManager.playClickSound();	// play click sound
			
			if (currentDialog == Constants.DG_NONE)	// if no dialog shown at the moment
			{	toggleAdvancing(true); }
			else
			{	panels[currentPanel].getDialog().requestFocus(); }
		}
    }
    
    public void advance()
    {
		if (terminating)
		{	endGame(); }
		
		if (advancing)
		{ 
			Verse v = LogicManager.readNextVerse();
			if ( v != null )
			{	present(v); }
			
			if (LogicManager.peekNextVerse() == null) 		// actual verse is last interpreted one, so most recent one
			{
				if (GraphicsManager.isShaded())							// if the screen is already shaded 
				{	GraphicsManager.toggleShading(false); }				// turn the shading off
			}
		} // puchate aączki :)
    }
	
    public void forward()
    {
    	if (advancing && currentDialog == Constants.DG_NONE)
		{
    		Verse v = LogicManager.peekNextVerse();
    		
	    	if (v != null && v.isSeen() == true)
	    	{	
	    		present(LogicManager.readNextVerse());
	    		
	    		if (LogicManager.readActualVerse().getNext() == null) 		// actual verse is last interpreted one, so most recent one
				{
					if (GraphicsManager.isShaded())							// if the screen is already shaded 
					{	GraphicsManager.toggleShading(false); }				// turn the shading off
				}
	    	}
		}
    }
    
    public void backward()
    {
    	if (advancing && currentDialog == Constants.DG_NONE)
		{	
    		Verse v = LogicManager.readPreviousVerse();
		
    		if (v != null) 
    		{
				present(v);
				
				if (!GraphicsManager.isShaded())							// if the screen isn't already shaded
				{	GraphicsManager.toggleShading(true); }					// turn the shading off
    		}
		}
    }
    
    public void processAnswer(int option)
	{	
		int maxAnsw =  ((DialogOption)(dialogs[Constants.DG_OPTIONS])).getButtonsCount() + 1;
	
		if (currentDialog == Constants.DG_OPTIONS && option < maxAnsw)
		{ 
			showNarration();
			hideDialog();
			requestFocus();
			presentResult(LogicManager.readActualVerse(),(Integer.toString(option)));
			SoundManager.playClickSound();		// play click sound
		}
	}
	
	public void processInput()
	{
		if (currentDialog == Constants.DG_INPUT)
		{
			String ans = "";
			
			if(getInputText().equals(""))
			{	showEmptyInputAlert(true); }
			else
			{ 
				ans = getInputText();
				showNarration();
				hideDialog();
				clearInputText();
				requestFocus();
				showEmptyInputAlert(false);
				presentResult(LogicManager.readActualVerse(), ans);
				SoundManager.playClickSound();		// play click sound
			}
		}
	}
    
	public void saveGame(int slotNo)
	{ 	SaveManager.saveGame(slotNo); }

	public void loadGame(int slotNo)
	{
		changePanel(Constants.P_NARRATION);	
		hideDialog();
		
		ingame 		= true;
		toggleAdvancing(true);
		
		SoundManager.stopMusic();
		SoundManager.clearGameSound();
		GraphicsManager.toggleShading(false);
		LogicManager.setMemory(SaveManager.loadGame(slotNo));
										// Hide possible remaining option/input dialog
		present(LogicManager.readFirstLoadedVerse());
		loadScene();
	}
	
	public void endGame()
	{
		ingame 	= false;
		toggleAdvancing(false);
		toggleTerminating(false);
		hideDialog();
		setIngameMenuButtonsActive(true);
		GraphicsManager.toggleShading(false);
		GraphicsManager.setBackground(GraphicsManager.getMenuBackground());
		GraphicsManager.setSprite(Constants.POS_LEFT, null);
		GraphicsManager.setSprite(Constants.POS_CENTER, null);
		GraphicsManager.setSprite(Constants.POS_RIGHT, null);
		SoundManager.clearGameSound();
		SoundManager.playMenuMusic();
		changePanel(Constants.P_MAIN_MENU);
		
		//DEBUG: For debugging purposes
		System.out.println("=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=");
		System.out.println(LogicManager.getMemory().printableStoryState());
	}
	
	public void exitGame()
	{
		ResourcesManager.storeConfig();
		SoundManager.clearGameSound();
		
		//DEBUG: For debugging purposes
		if (LogicManager.getMemory() != null)
		{
			System.out.println("=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=");
			System.out.println(LogicManager.getMemory().printableStoryState());
			System.out.println("=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=");
			System.out.println(LogicManager.getMemory().printableSceneState());
		}
		
		dispose();
		System.exit(0);
	}
	
	public void present(Verse v)
    { 
		if (v != null)
		{	v.present(); }
	}
	
	public void presentResult(Verse v, String ans)
	{ v.presentResult(ans); }
	
	// --- LISTENERS ---	
	// --- MouseListener ---	
	@Override
	public void mouseClicked(MouseEvent e)
	{
		if (e.getButton() == MouseEvent.BUTTON1)
		{	advance(); }
		
		if (e.getButton() == MouseEvent.BUTTON2)
		{	backward();	}
		
		if (e.getButton() == MouseEvent.BUTTON3)
		{	switchIngameMenu(); }
	}

	@Override
	public void mouseEntered(MouseEvent e) {}

	@Override
	public void mouseExited(MouseEvent e) {}

	@Override
	public void mousePressed(MouseEvent e) {}

	@Override
	public void mouseReleased(MouseEvent e) {}

	// --- Mouse Wheel Listener	---
	@Override
	public void mouseWheelMoved(MouseWheelEvent e)
	{
		if (e.getWheelRotation() < 0) 	// Mouse wheel moved UP
        {	backward(); }
        else 							// Mouse wheel moved DOWN
        {	forward(); }
        repaint();	
	}

	// --- KeyboardListener ---	
	@Override
	public void keyPressed(KeyEvent e)
	{
		if ((e.getKeyCode() == KeyEvent.VK_ENTER) && e.isAltDown())		// FullScreen mode handling
		{	toggleFullscreen(!fullscreen);	}		
		
		if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
		{	switchIngameMenu(); }
		
		if (e.getKeyCode() == KeyEvent.VK_ENTER)
		{	processInput(); }
		
		if (e.getKeyCode() == KeyEvent.VK_RIGHT)
		{	forward(); }
		
		if (e.getKeyCode() == KeyEvent.VK_LEFT)
		{	backward(); }
		
		if (e.getKeyCode() == KeyEvent.VK_SPACE)
		{	advance(); }	
		
		for (int i = KeyEvent.VK_1; i < KeyEvent.VK_9; i ++)
		{
			if (e.getKeyCode() == i)		// key code for "1" is 49, so the difference between number and code is 48
			{	processAnswer(i - 49); }	// index of first button is 0, so 49 should be substracted
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {}

	@Override
	public void keyTyped(KeyEvent e) {}
	
	// --- Action Listener ---
	@Override
	public void actionPerformed(ActionEvent e)
	{
		// Main/Ingame menu buttons:		
		if ("new".equals(e.getActionCommand()))							// New Game button pressed 
		{ 
			if (currentPanel == Constants.P_MAIN_MENU)
			{
				SoundManager.stopMusic();
				GraphicsManager.setBackground(null);					// clear background				
				present(LogicManager.readFirstVerse());
				ingame 		= true;
				toggleAdvancing(true);
				changePanel(Constants.P_NARRATION);
			}
			SoundManager.playClickSound();	// play click sound
		}
		
		if ("load".equals(e.getActionCommand()))						// Load Game button pressed 
		{ 
			if (currentPanel == Constants.P_INGAME_MENU)
			{	ingame = true; }
			else if (currentPanel == Constants.P_MAIN_MENU)
			{	ingame = false;	}
			changePanel(Constants.P_SAVES_MENU);
			setSaveLoadMenuContext(Constants.CONTEXT_LOAD);
			SoundManager.playClickSound();	// play click sound
		}
		
		if ("save".equals(e.getActionCommand()))						// Save Game button pressed 
		{ 
			if (currentPanel == Constants.P_INGAME_MENU)
			{	ingame = true; }
			else if (currentPanel == Constants.P_MAIN_MENU)
			{	ingame = false; 	}			changePanel(Constants.P_SAVES_MENU);
			setSaveLoadMenuContext(Constants.CONTEXT_SAVE);
			SoundManager.playClickSound();	// play click sound
		}
		
		if ("options".equals(e.getActionCommand()))						// Options button pressed 
		{ 
			if (currentPanel == Constants.P_INGAME_MENU)
			{
				ingame = true;
				changePanel(Constants.P_OPTIONS_MENU);
			}
			else if (currentPanel == Constants.P_MAIN_MENU)
			{
				ingame = false;
				changePanel(Constants.P_OPTIONS_MENU);
				activePanel.update(); 		// prevents from showing previously loaded sprites
			}
			SoundManager.playClickSound();	// play click sound
		}
		
		if ("extras".equals(e.getActionCommand()))						// Extras button pressed 
		{ 
			if (currentPanel == Constants.P_MAIN_MENU)
			{
				ingame = false;
				changePanel(Constants.P_EXTRAS_MENU);
			}
			SoundManager.playClickSound();	// play click sound
		}
		
		// --- Save/Load menu buttons ---
		if ("goBack".equals(e.getActionCommand()))						// Save Game button pressed 
		{ 
			if (ingame == false)
			{	changePanel(Constants.P_MAIN_MENU);	}
			else // isIngame == true
			{	changePanel(Constants.P_INGAME_MENU);}		
			SoundManager.playClickSound();	// play click sound
		}
		
		for (int i = 0; i < Constants.MAX_SLOTS; i++)
		{
			if (("saveSlot" + i).equals(e.getActionCommand()))			// Save slot button pressed
			{ 
				if (SaveManager.exists(i))
				{
				    setSaveLoadMenuButtonsActive(false);				    
					setConfirmationDialogContext(Constants.CONTEXT_SAVE, i);
					showDialog(Constants.DG_CONFIRM);
				}
				else
				{	saveGame(i); }				
				SoundManager.playClickSound();	// play click sound
			}
			
			if (("loadSlot" + i).equals(e.getActionCommand()))			// Load slot button pressed
			{ 
				if (ingame)
				{
					setSaveLoadMenuButtonsActive(false);				    
					setConfirmationDialogContext(Constants.CONTEXT_LOAD, i);
					showDialog(Constants.DG_CONFIRM);
				}
				else
				{	loadGame(i); }
				SoundManager.playClickSound();	// play click sound
			}
			
			if (("ok_load" + i).equals(e.getActionCommand()))			// Loading game confirmed 
			{ 
				hideDialog();											// Hide load game dialog
				setSaveLoadMenuButtonsActive(true);
				loadGame(i);			
				SoundManager.playClickSound();	// play click sound
			}
			
			if (("ok_overwrite" + i).equals(e.getActionCommand()))		// Overwriting save data confirmed 
			{ 
				saveGame(i);
				hideDialog();
				setSaveLoadMenuButtonsActive(true);
				SoundManager.playClickSound();	// play click sound
			}
		}
		
		if (("cancel_sl").equals(e.getActionCommand()))					// Loading/overwriting cancelled
		{
			hideDialog();
			setSaveLoadMenuButtonsActive(true);
			SoundManager.playClickSound();	// play click sound
		}
		
		if("fullscr".equals(e.getActionCommand()))						// Fullscreen switch checked/unchecked
		{	
			toggleFullscreen(!fullscreen);
			SoundManager.playClickSound();	// play click sound
		}
		
		// --- Dialogs buttons ---
		if ("exit_mm".equals(e.getActionCommand()))
		{
			showDialog(Constants.DG_EXIT_MM );
			showMainMenu(false);
			SoundManager.playClickSound();	// play click sound
		}
		
		if ("exit_im".equals(e.getActionCommand()))
		{
			showDialog(Constants.DG_EXIT_IM );
			setIngameMenuButtonsActive(false);
			SoundManager.playClickSound();	// play click sound
		}
		
		if ("exit".equals(e.getActionCommand()))					// exit the game selected 
		{ 	
			exitGame();
			SoundManager.playClickSound();	// play click sound
		}
				
		if ("main_menu".equals(e.getActionCommand()))				// return to main menu selected
		{ 	
			endGame();
			SoundManager.playClickSound();	// play click sound
		}

		if ("cancel".equals(e.getActionCommand()))					// go back to game selected 
		{ 
			hideDialog();
			SoundManager.playClickSound();	// play click sound
			
			if(currentPanel == Constants.P_INGAME_MENU)
			{ setIngameMenuButtonsActive(true); }
			if(currentPanel == Constants.P_MAIN_MENU)
			{ showMainMenu(true); }
		}
		
		if ("confirm_input".equals(e.getActionCommand()))
		{	processInput(); }
		
		for (int i = 1; i < Constants.NUM_OPTIONS; i++)
		{
			  if (("Option_" + i).equals(e.getActionCommand()))
			  {	processAnswer(i-1); }
		}
	}
	
	// --- Change Listener ---
	@Override
	public void stateChanged(ChangeEvent e)
	{
		if(e.getSource().getClass().equals(JSlider.class))
		{
			JSlider slider = (JSlider)e.getSource();
			
			if (!slider.getValueIsAdjusting())
			{
				if(slider.getName().equals("music"))
				{
					SoundManager.setSoundVolume(Constants.TYPE_MUSIC, slider.getValue());
					SoundManager.updateMusicVolume();
				}
				if(slider.getName().equals("sound"))
				{
					SoundManager.setSoundVolume(Constants.TYPE_SOUND, slider.getValue());
					if (initialized)
					{	SoundManager.playClickSound();	}	// system sound sample
				}
				if(slider.getName().equals("voice"))
				{
					SoundManager.setSoundVolume(Constants.TYPE_VOICE, slider.getValue());
					if (initialized)
					{	SoundManager.playVoiceSample(); }	// system voice sample
				}
				if(slider.getName().equals("opacity"))
				{	
					if (initialized)
					{	SoundManager.playClickSound(); }	// play click sound
				}
			}	
			if(slider.getName().equals("opacity"))
			{	
				GraphicsManager.setMenuOpacity(slider.getValue());
				update();
			}
		}			
	}
		
}
