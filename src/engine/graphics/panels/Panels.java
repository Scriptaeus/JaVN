package engine.graphics.panels;

import javax.swing.*;

import engine.Constants;
import engine.graphics.GraphicsManager;
import engine.graphics.Window;
import engine.graphics.dialogs.Dialogs;
import engine.logics.ResourcesManager;

public abstract class Panels extends JPanel
{
	private static final long 		serialVersionUID = 1L;

	protected Window	window;
	protected Dialogs	dialog;	
	protected JLabel[]	sprites		= new JLabel[3];	
	protected JLabel	background;
	protected JLabel	replay;
	
	protected int 		width;
	protected int 		height;
	
	// --- CONSTRUCTOR ---
	public Panels(Window w)
	{
		window = w;
		
		replay = new JLabel(ResourcesManager.getTextPromptReplay() + "  ");
		replay.setOpaque(true);
		replay.setVisible(false);
        replay.setBackground(Constants.REPLAY_SHADE);
        replay.setForeground(Constants.REPLAY_TEXT);

		background = new JLabel();
        background.setOpaque(true);
        background.setVisible(true);
        
        for (int i = 0; i < 3; i++)
        {
    		sprites[i] = new JLabel();
    		sprites[i].setOpaque(false);
    		sprites[i].setVisible(true);
        }
        
        updateSize();
	}
	
	// --- GETTERS ---
	public Dialogs getDialog()
	{	return dialog; }
	
	// --- SETTERS ---
	public void setDialog(Dialogs d)
	{ 
		dialog = d;
		dialog.setOpaque(true);
		dialog.setVisible(true);
        add(dialog,1);
	}
	
	// --- METHODS ---
	public void update() {}
	
	public void updateSize()
	{
		width   = this.getWidth();
	    height  = this.getHeight();
	}
	
	protected JButton createButton(String name, String command)
	{
		JButton b = new JButton(name);
		b.setOpaque(true);
		b.setVisible(true);
        b.addActionListener(window);
        b.setActionCommand(command);
        b.setFocusable(false);
        
        return b;
	}
	
	protected void prepareBackground()
	{
        ImageIcon bckg = GraphicsManager.getBackground();
        if (bckg != null)
        {         
        	background.setLocation(0, 0);
        	background.setSize(width, height);
        	background.setIcon(GraphicsManager.resizeImage(bckg, width, height));
        }
	}

	protected void prepareSprite()
	{
        ImageIcon sprt;
        
        for (int i = Constants.POS_LEFT; i < Constants.NUM_SPRITES; i++)
        {
        	sprt = GraphicsManager.getSprite(i);
	        if (sprt != null)
	        { 
		        sprites[i].setLocation((int)(Constants.POS_SPRITES[i] * width), (int)(0.1   * height));
		        sprites[i].setSize((int)(0.45  * width), (int)(0.9   * height));        	
	        	sprites[i].setIcon(GraphicsManager.resizeImage(sprt, (int)(0.45 * width), (int)(0.9  * height)));
	        }
        }
	}
	
	protected void prepareReplay()
	{
		replay.setText(ResourcesManager.getTextPromptReplay() + " ");
        replay.setVerticalAlignment(SwingConstants.TOP);
        replay.setHorizontalAlignment(SwingConstants.RIGHT);
        replay.setLocation(0, 0);
    	replay.setSize(width, height-1);
    	replay.setFont(window.getScalledFont(GraphicsManager.getTextFont(),65));
       	replay.setVisible(GraphicsManager.isShaded());
	}
	
	protected void prepareDialog()
	{
        dialog.setLocation 	(dialog.getAbsoluteLocation());
        dialog.setSize		(dialog.getAbsoluteSize());
	}
}
