package engine.graphics.panels;

import java.awt.*;
import javax.swing.*;

import engine.graphics.GraphicsManager;
import engine.graphics.Window;
import engine.graphics.dialogs.DialogExitMain;
import engine.logics.ResourcesManager;

public class PanelMenuMain extends Panels
{
	private static final long 	serialVersionUID = 1L;
		
	private JButton[]	buttons;
	private JLabel		menu;
		
	public PanelMenuMain(Window w)
	{
		super(w);
		
		menu = new JLabel();
        menu.setOpaque(true);
        menu.setVisible(true);
        
        dialog = new DialogExitMain(window);
        
		buttons = new JButton[5];
		buttons[0] = createButton(ResourcesManager.getTextNewGame(), "new");		
		buttons[1] = createButton(ResourcesManager.getTextLoadGame(), "load");
		buttons[2] = createButton(ResourcesManager.getTextOptions(), "options");
		buttons[3] = createButton(ResourcesManager.getTextExtras(), "extras");
		buttons[4] = createButton(ResourcesManager.getTextExit(), "exit_mm");
		        
		for (JButton b: buttons)
		{ menu.add(b);	}
        background.add(menu);
        add(dialog);
        add(background);
        
        update();
	}
		
	public void hideMenu()
	{	menu.setVisible(false); }
	
	public void showMenu()
	{	menu.setVisible(true); }
	
	@Override
	public void update()
	{
        Color textColor = GraphicsManager.getTextColor();
        Color menuColor = GraphicsManager.getMenuColor();
        
    	buttons[0].setText(ResourcesManager.getTextNewGame());
		buttons[1].setText(ResourcesManager.getTextLoadGame());
		buttons[2].setText(ResourcesManager.getTextOptions());
		buttons[3].setText(ResourcesManager.getTextExtras());
		buttons[4].setText(ResourcesManager.getTextExit());
		
		for (JButton b : buttons)
		{
			b.setForeground(textColor);
			b.setBackground(menuColor);
		}
		
        menu.setBackground(menuColor);
        
        dialog.update();
        // mautkie narazie 
        // brybrusie
	}	
	
	@Override
    public void paintComponent(Graphics g)  			// draw game's Narration Window
    {
        int menuItm = (ResourcesManager.isExtrasEnabled() ? 6 : 5);
        int mWidth  = (int)(0.25 * width);
        int mHeight = (int)(0.09 * height * menuItm);
        double pos  = 0.03;
        
		updateSize();
		
		prepareBackground();
		prepareDialog();
        
        for (JButton b : buttons)
        {
        	if (!b.getText().equals(ResourcesManager.getTextExtras()) || ResourcesManager.isExtrasEnabled())
        	{
	        	b.setSize		((int)(0.75  * mWidth),  (int)(0.08 * height));
	        	b.setLocation	((int)(0.125 * mWidth),  (int)(pos * height));
				b.setFont(window.getScalledFont(GraphicsManager.getTextFont(),18));
		        pos += 0.1;
        	}
        }
        
        menu.setSize	 (mWidth, mHeight);
        menu.setLocation ((int)(0.725 * width), (int)(0.95 * height - mHeight));        
       
    }
}
