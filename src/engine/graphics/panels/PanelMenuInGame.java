package engine.graphics.panels;

import java.awt.*;
import javax.swing.*;

import engine.Constants;
import engine.graphics.GraphicsManager;
import engine.graphics.Window;
import engine.graphics.dialogs.DialogExitInGame;
import engine.logics.ResourcesManager;

public class PanelMenuInGame extends Panels
{
	private static final long serialVersionUID = 1L;
	
	private JButton[]	buttons;
	private JLabel		menu;
	
	public PanelMenuInGame(Window w)
	{
		super(w);
		
		menu = new JLabel();
        menu.setOpaque(true);
        menu.setVisible(true);
		
		buttons = new JButton[4];		
		buttons[0] = createButton(ResourcesManager.getTextLoadGame(), "load");
		buttons[1] = createButton(ResourcesManager.getTextSaveGame(), "save");
		buttons[2] = createButton(ResourcesManager.getTextOptions(), "options");
		buttons[3] = createButton(ResourcesManager.getTextExit(), "exit_im");
				
        dialog = new DialogExitInGame(window);
        
		for (JButton b: buttons)
		{ menu.add(b);	}
        background.add(menu);
        for(JLabel s : sprites)
        {	background.add(s); }
        add(dialog);
        add(background);
        
        update();
	}
	
	@Override
	public void update()
	{
       Color textColor = GraphicsManager.getTextColor();
       Color menuColor = GraphicsManager.getMenuColor();
		
       for(int i = Constants.POS_LEFT; i < Constants.NUM_SPRITES; i++)
        {	
        	if (GraphicsManager.getSprite(i) == null)
        	{	sprites[i].setVisible(false); }
        	else
        	{	sprites[i].setVisible(true); }
        }
		
		for (JButton b : buttons)
		{
			b.setForeground(textColor);
			b.setBackground(menuColor);
		}
        
		buttons[0].setText(ResourcesManager.getTextLoadGame());
		buttons[1].setText(ResourcesManager.getTextSaveGame());
		buttons[2].setText(ResourcesManager.getTextOptions());
		buttons[3].setText(ResourcesManager.getTextExit());
		
        menu.setBackground(menuColor);
        
        dialog.update();
	}
	
	public void setButtonsActive(boolean a)
	{
		for (JButton b : buttons)
		{ b.setEnabled(a);	}
	}
		
	@Override
    public void paintComponent(Graphics g)  			// draw game's Narration Window
    {        
        updateSize();
        
        prepareBackground();
        prepareSprite();
        prepareDialog();
     
        int  pos 	= 1;
        for (JButton b : buttons)
        {
        	b.setSize		((int)(0.3 * width), (int)(0.1 * height));
        	b.setLocation	((int)(0.3 * width), (int)(0.1 * pos * height));
			b.setFont(window.getScalledFont(GraphicsManager.getTextFont(),18));
	        pos += 2;
        }
		
        menu.setLocation ((int)(0.05 * width), (int)(0.05 * height));
        menu.setSize	 ((int)(0.9 * width),  (int)(0.9 * height));
    }
}
