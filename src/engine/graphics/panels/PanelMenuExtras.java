package engine.graphics.panels;

import java.awt.*;
import javax.swing.*;

import engine.Constants;
import engine.graphics.GraphicsManager;
import engine.graphics.Window;
import engine.logics.ResourcesManager;

public class PanelMenuExtras extends Panels
{
	private static final long serialVersionUID = 1L;
	
	private JLabel		title;
	private JLabel		menu;
	private JButton 	back;
	
	public PanelMenuExtras(Window w)
	{
		super(w);
		
		title = new JLabel(ResourcesManager.getTextExtras());
		title.setOpaque(true);
		title.setVisible(true);
		
		back = createButton(ResourcesManager.getTextBack(), "goBack");
				
		menu = new JLabel();
        menu.setOpaque(true);
        menu.setVisible(true);
        
        menu.add(title);
        menu.add(back);
        background.add(menu);
        add(background);
        
        update();
	}
	
	@Override
	public void update()
	{ 
        Color textColor = GraphicsManager.getTextColor();
        Color menuColor = GraphicsManager.getMenuColor();
		
		title.setBackground(Constants.TRANSPARENT);
		title.setForeground(textColor);
		
		back.setBackground(menuColor);
		back.setForeground(textColor);
		
		back.setText(ResourcesManager.getTextBack());
		
		menu.setBackground(menuColor);
	}
		
	@Override
    public void paintComponent(Graphics g)  			// draw game's Narration Window
    {
        updateSize();
        
        prepareBackground();

        title.setSize		((int)(0.8  * width), (int)(0.1  * height));
        title.setLocation	((int)(0.05 * width), (int)(0.03 * height));
        title.setHorizontalAlignment(SwingConstants.CENTER);
		title.setFont(window.getScalledFont(GraphicsManager.getTextFont(),25));
        
        back.setSize		((int)(0.1 * width),  (int)(0.05 * height));
    	back.setLocation	((int)(0.77 * width), (int)(0.8  * height));
		back.setFont(window.getScalledFont(GraphicsManager.getTextFont(),18));
        
        menu.setLocation ((int)(0.05 * width), (int)(0.05 * height));
        menu.setSize	 ((int)(0.9 * width),  (int)(0.9 * height));
       
    }
}
