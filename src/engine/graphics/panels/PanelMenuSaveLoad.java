package engine.graphics.panels;

import java.awt.*;
import java.awt.font.FontRenderContext;
import java.awt.geom.Rectangle2D;

import javax.swing.*;

import engine.Constants;
import engine.graphics.GraphicsManager;
import engine.graphics.Window;
import engine.graphics.dialogs.DialogConfirm;
import engine.logics.ResourcesManager;
import engine.logics.SaveManager;

public class PanelMenuSaveLoad extends Panels
{
	private static final long serialVersionUID = 1L;
	
	// === FIELDS ===
	private boolean			context;		// actual panel context
	private SaveButton[]	slots;			// save slots buttons
	private JButton			back;			// return button
	private JLabel			title;			// window title
	private JLabel			selectionPane;	// window frame
	
	// === SaveButton nested class ===
	private class SaveButton extends JButton
	{
		private static final long serialVersionUID = 1L;
		
		private ImageIcon thumbnail;	
		
		public SaveButton(String name)
		{ super(name); }

		public void setBackgroundImg(ImageIcon image)
		{ thumbnail = image; }
		
		public ImageIcon getBackgroundImg()
		{ return thumbnail; }
		
		@Override
		public void paintComponent(Graphics g)
		{	
			ImageIcon i 	= this.getBackgroundImg();
	        Font 	  font 	= GraphicsManager.getTextFont();
			
			if ( i != null)		
			{	g.drawImage(GraphicsManager.resizeImage(i, this.getWidth(), this.getHeight()).getImage(), 0, 0, this); }
			else				// no save file or thumbnail not loaded properly
			{ 
				g.setColor(GraphicsManager.getPopupColor());
				g.fillRect(0, 0, this.getWidth(), this.getHeight());
			}
			
			// == Printing button label ==			
			g.setFont (window.getScalledFont(GraphicsManager.getTextFont(), 18));
			
			if (isEnabled())
			{ g.setColor(GraphicsManager.getTextColor()); }
			else
			{ g.setColor(GraphicsManager.getInactiveTextColor()); }
		
			Rectangle2D tbounds = font.getStringBounds(this.getText(), new FontRenderContext(null, true, true));
			int twidth  = (int)Math.round(tbounds.getWidth()/2  + tbounds.getX());
			int theight = (int)Math.round(tbounds.getHeight()/2 + tbounds.getY());			

			g.drawString(this.getText(), this.getWidth()/2 - twidth, this.getHeight()/2 - theight);
        }
	}
	
	public PanelMenuSaveLoad(Window w)
	{
		super(w);
		
		title = new JLabel();
		title.setOpaque(true);
		title.setVisible(true);
			
		dialog = new DialogConfirm(window);
        dialog.setOpaque(true);
        dialog.setVisible(false);
        
		selectionPane = new JLabel();
		selectionPane.setOpaque(true);
		selectionPane.setVisible(true);
		
		slots = new SaveButton[Constants.MAX_SLOTS];
		for (int i = 0; i< slots.length; i++)
		{
			slots[i] = new SaveButton("Slot " + (i+1));
			slots[i].addActionListener(window);
			slots[i].setFocusable(false);
			slots[i].setOpaque(true);
			slots[i].setVisible(true);			
		}

		back = createButton(ResourcesManager.getTextBack(), "goBack");
		
		selectionPane.add(title);
		for (SaveButton b: slots)
		{ 	selectionPane.add(b);	}
		selectionPane.add(back);
        background.add(selectionPane); 
        for(JLabel s : sprites)
        {	background.add(s); }
        add(dialog);  
        add(background);
      
        update();
	}
	
	public void setButtonsActive(boolean b)
	{
		if (b && context == Constants.CONTEXT_LOAD) 	// buttons are to be enabled and panel in load context
		{
			for(int i = 0; i<slots.length; i++)
			{ slots[i].setEnabled(SaveManager.exists(i)); }
		}
		else
		{
			for (SaveButton s : slots)
			{ s.setEnabled(b); }
		}
		
		back.setEnabled(b);
	}
	
	public void setContext(boolean c)
	{
		context = c;
		
		if (context == Constants.CONTEXT_SAVE)
		{
			title.setText(ResourcesManager.getTextSaveGame());
			for(int i = 0; i<slots.length; i++)
			{ 
				slots[i].setEnabled(true);
				slots[i].setActionCommand("saveSlot" + i);
			}
		}
		else if (context == Constants.CONTEXT_LOAD)
		{
			title.setText(ResourcesManager.getTextLoadGame());
			for(int i = 0; i<slots.length; i++)
			{ 
				slots[i].setEnabled(SaveManager.exists(i));
				slots[i].setActionCommand("loadSlot" + i);
			}
		}
	}
	
	public void setSlotThumbnail(int slotNo, ImageIcon thumb)
	{ slots[slotNo].setBackgroundImg(thumb); }
	
	@Override
	public void update()
	{
       Color textColor = GraphicsManager.getTextColor();
       Color menuColor = GraphicsManager.getMenuColor();
		
       for(int i = 0; i < Constants.NUM_SPRITES; i++)
        {	
    	   	if (GraphicsManager.getSprite(i) == null)
        	{	sprites[i].setVisible(false); }
        	else
        	{	sprites[i].setVisible(true); }
    	   	
    	   	back.setText(ResourcesManager.getTextBack());
        }
       
		title.setBackground(Constants.TRANSPARENT);
		title.setForeground(textColor);
		
		for (SaveButton b : slots)
		{
			b.setForeground(textColor);
			b.setBackground(menuColor);
		}
		
		back.setForeground(textColor);
		back.setBackground(menuColor);
		
        selectionPane.setBackground(menuColor);

        dialog.update();
	}
		
	@Override
    public void paintComponent(Graphics g)  			// draw Save/Load Game Window
    {
        int  pos 	= 0;
        Font font 	= GraphicsManager.getTextFont();
        
        updateSize();
        
        prepareBackground();
        prepareSprite();
               
        title.setSize		((int)(0.8  * width), (int)(0.1  * height));
        title.setLocation	((int)(0.05 * width), (int)(0.03 * height));
        title.setHorizontalAlignment(SwingConstants.CENTER);
		title.setFont(window.getScalledFont(font,25));
        
        for (SaveButton b : slots)
        {
        	b.setSize		((int)(0.2 * width), (int)(0.2 * height));
        	b.setLocation	((int)((0.03 + (pos%3) * 0.23) * width), (int)((0.15 + (pos/3) * 0.25) * height));
			b.setFont(window.getScalledFont(font, 18));
	        pos += 1;
        }

        back.setSize		((int)(0.1 * width), (int)(0.05 * height));
    	back.setLocation	((int)(0.77 * width), (int)(0.8 * height));
		back.setFont(window.getScalledFont(font, 18));
        
        selectionPane.setLocation 	((int)(0.05 * width), (int)(0.05 * height));
        selectionPane.setSize	 	((int)(0.9 * width),  (int)(0.9 * height));
        
        dialog.setLocation 	(dialog.getAbsoluteLocation());
        dialog.setSize		(dialog.getAbsoluteSize());
    }
}
