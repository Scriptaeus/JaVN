package engine.graphics.panels;

import java.awt.*;

import javax.swing.*;
import javax.swing.border.*;

import engine.Constants;
import engine.LogicManager;
import engine.graphics.GraphicsManager;
import engine.graphics.Window;
import engine.graphics.dialogs.DialogExitInGame;
import engine.logics.ResourcesManager;
import engine.logics.elements.Verse;

public class PanelNarration extends Panels
{
	private static final long serialVersionUID = 1L;
	
	private JLabel 		narration;
	
	public PanelNarration(Window w)
	{
		super(w);

		narration = new JLabel("Default");
		narration.setBorder ( new CompoundBorder(narration.getBorder(), new EmptyBorder(10,30,10,30) ));	// adding a margin to narration panel content		
        narration.setOpaque(true);
        narration.setVisible(true);
        
        dialog = new DialogExitInGame(window);
        dialog.setOpaque(true);
        dialog.setVisible(false);

        background.add(replay); 
        background.add(narration);
        for(JLabel s : sprites)
        {	background.add(s); }
        
        add(dialog);
        add(background);
   
        update();
	}
	
	public void showNarration(boolean b)					// switch narration pane visibility
	{ narration.setVisible(b);	}

	public void setContent(Verse v)
	{
		String head;
		
		if (v.getSpeaker() == Constants.ACTOR_DEFAULT)
		{ head = "<html><br><b>"; }
		else
		{	head = "<html><font color = \""	+ v.getSpeakerColor() + "\"><b>" + v.getSpeakerName() + "</b></font><br><b>"; }
		
		narration.setText(head + v.getStatement() + "</b></html>");
	}
	
	public void setDependentContent(Verse v, String a)
	{		
		Verse answered = new Verse(v);			// copy parameters of previous verse (media change included)		
		answered.setStatement(answered.getStatement() + "<br> " + ResourcesManager.getTextPromptAnswer() + " " + a);
		
		answered.setSeen();						// mark question + fetched answer verse as seen
		setContent(answered);					// present question + fetched answer
		
		// --- should be active if blocking possibility to change decision
			LogicManager.removeLastVerse();		// removes input/question dialog action
			LogicManager.removeLastVerse();		// removes input/question verse

			LogicManager.addVerse(answered);	// add it to the story
			LogicManager.readLastVerse();		// proceed as when mouse clicked
		// --- end of block 
	}
		
	@Override
	public void update()
	{
         for(int i = 0; i < Constants.NUM_SPRITES; i++)
        {	
        	if (GraphicsManager.getSprite(i) == null)
        	{	sprites[i].setVisible(false); }
        	else
        	{	sprites[i].setVisible(true); }
        }
		
        narration.setForeground(GraphicsManager.getTextColor());
        narration.setBackground(GraphicsManager.getMenuColor());

        dialog.update();
	}

	@Override
    public void paintComponent(Graphics g)  			// draw game's Narration Window
    {
        Font font 	= GraphicsManager.getTextFont();
 
        updateSize();
        
        prepareBackground();
        prepareSprite();
        prepareReplay();
        
        narration.setVerticalAlignment(SwingConstants.TOP);
        narration.setHorizontalAlignment(SwingConstants.LEFT);
        narration.setLocation(0, 	 (int)(0.7 * height));
        narration.setSize	 (width, (int)(0.3 * height)+1);
        narration.setFont(window.getScalledFont(font,18));
        
        prepareDialog();        
    }
}