package engine.graphics.panels;

import java.awt.*;
import java.util.*;
import javax.swing.*;

import engine.Constants;
import engine.graphics.GraphicsManager;
import engine.graphics.Window;
import engine.logics.ResourcesManager;
import engine.sounds.SoundManager;

public class PanelMenuOptions extends Panels
{
	private static final long serialVersionUID = 1L;
	
	// tables of labels
	private Hashtable<Integer, JLabel> vLabels;
	private Hashtable<Integer, JLabel> oLabels;
	// icons used on options panel
	private ImageIcon		speakerS;
	private ImageIcon		speakerL;
	private ImageIcon		selected;
	// labels describing the elements of this panels 
	private JLabel			title;
	private JLabel			optionsPane;
	private JLabel			fullscrDesc;
	private JLabel			opacityDesc;
	private JLabel[]		volumeDesc; 
	// sliders adjusting sounds volume and panels opacity
	private JSlider[]		volumeSliders;
	private JSlider			opacitySlider;
	// buttons used on this panel
	private JToggleButton 	fullscreen;
	private JButton 		back;	
	
	public PanelMenuOptions(Window w)
	{
		super(w);
		
		speakerS = GraphicsManager.loadImage(Constants.PATH_SPEAKER_S);
		speakerL = GraphicsManager.loadImage(Constants.PATH_SPEAKER_L);
		selected = GraphicsManager.loadImage(Constants.PATH_MARKER);
		
		title = new JLabel(ResourcesManager.getTextOptions());
		title.setOpaque(true);
		title.setVisible(true);
		
		optionsPane = new JLabel();
		optionsPane.setOpaque(true);
		optionsPane.setVisible(true);
		
		fullscrDesc = new JLabel(ResourcesManager.getTextFullscreen());
		fullscrDesc.setOpaque(true);
		fullscrDesc.setVisible(true);
		
		volumeDesc = new JLabel[Constants.NUM_SLIDERS];
		volumeDesc[Constants.TYPE_MUSIC] = new JLabel(ResourcesManager.getTextMusic());
		volumeDesc[Constants.TYPE_SOUND] = new JLabel(ResourcesManager.getTextSound());
		volumeDesc[Constants.TYPE_VOICE] = new JLabel(ResourcesManager.getTextVoice());
		for (JLabel l : volumeDesc)
		{
			l.setOpaque(true);
			l.setVisible(true);
		}
		
		opacityDesc = new JLabel(ResourcesManager.getTextOpacity());
		opacityDesc.setOpaque(true);
		opacityDesc.setVisible(true);
		
		vLabels = new Hashtable<Integer, JLabel>();
	    vLabels.put(Constants.VOL_MIN, new JLabel());
	    vLabels.put(Constants.VOL_MAX, new JLabel());
	    
	    oLabels = new Hashtable<Integer, JLabel>();
	    oLabels.put(Constants.OP_MIN, new JLabel("min"));
	    oLabels.put(Constants.OP_MAX, new JLabel("max"));

	    volumeSliders = new JSlider[Constants.NUM_SLIDERS];
	    for (int i = 0; i < Constants.NUM_SLIDERS; i++)
		{
			volumeSliders[i] = new JSlider
			(
				JSlider.HORIZONTAL,
				Constants.VOL_MIN,
				Constants.VOL_MAX,
				SoundManager.getSoundVolume(i)
			);
					
			volumeSliders[i].setPaintLabels(true);
			volumeSliders[i].setLabelTable(vLabels);
			volumeSliders[i].setMajorTickSpacing(5);
			volumeSliders[i].setSnapToTicks(true);
			volumeSliders[i].addChangeListener(window);
			volumeSliders[i].setFocusable(false);
			volumeSliders[i].setOpaque(true);
			volumeSliders[i].setVisible(true);
		}

	    volumeSliders[Constants.TYPE_MUSIC].setName("music");
	    volumeSliders[Constants.TYPE_SOUND].setName("sound");
	    volumeSliders[Constants.TYPE_VOICE].setName("voice");
	    
	    opacitySlider = new JSlider
	    (
	    	JSlider.HORIZONTAL,
	    	Constants.OP_MIN,
	    	Constants.OP_MAX,
	    	GraphicsManager.getOpacity()
	    );
	    opacitySlider.setName("opacity");
	    
	    opacitySlider.setPaintLabels(true);
	    opacitySlider.setLabelTable(oLabels);
	    opacitySlider.setMajorTickSpacing(5);
	    opacitySlider.setSnapToTicks(true);
	    opacitySlider.addChangeListener(window);
	    opacitySlider.setFocusable(false);
	    opacitySlider.setOpaque(true);
	    opacitySlider.setVisible(true);
		
		fullscreen = new JToggleButton();
		fullscreen.setIcon(window.isFullscreen() ? window.getScalledIcon(22, selected) : null);
		fullscreen.setActionCommand("fullscr");
		fullscreen.addActionListener(window);
		fullscreen.setFocusable(false);
		fullscreen.setOpaque(true);
		fullscreen.setVisible(true);		
		
		back = createButton(ResourcesManager.getTextBack(), "goBack");
		
		optionsPane.add(title);
		optionsPane.add(fullscrDesc);
		for (JLabel l : volumeDesc)
		{ optionsPane.add(l); }
		for (JSlider s : volumeSliders)
		{ optionsPane.add(s); }
		optionsPane.add(opacitySlider);
		optionsPane.add(opacityDesc);
		optionsPane.add(fullscreen);
		optionsPane.add(back);

        background.add(optionsPane);
        for(JLabel s : sprites)
        {	background.add(s); }
        add(background);
        
        update();
	}
	
	public void toggleFullscreenMarked()
	{
		if (window.isFullscreen())
		{ 
			fullscreen.setIcon(window.getScalledIcon(22, selected));
			fullscreen.setSelected(true);
		}
		else
		{ 
			fullscreen.setIcon(null);
			fullscreen.setSelected(false);
		}
	}
	
	public void adjustSlider(int s, int v)
	{
		if (s < Constants.NUM_SLIDERS)
		{	volumeSliders[s].setValue(v); }
		else if (s == Constants.OPACITY)
		{	opacitySlider.setValue(v); }
	}
	
	public void update()
	{
        for(int i = 0; i < Constants.NUM_SPRITES; i++)
        {	
        	if (GraphicsManager.getSprite(i) == null)
        	{	sprites[i].setVisible(false); }
        	else
        	{	sprites[i].setVisible(true); }
        	
        	title.setText(ResourcesManager.getTextOptions());    		
    		fullscrDesc.setText(ResourcesManager.getTextFullscreen());
    		volumeDesc[Constants.TYPE_MUSIC].setText(ResourcesManager.getTextMusic());
    		volumeDesc[Constants.TYPE_SOUND].setText(ResourcesManager.getTextSound());
    		volumeDesc[Constants.TYPE_VOICE].setText(ResourcesManager.getTextVoice());
    		opacityDesc.setText(ResourcesManager.getTextOpacity());
    		back.setText(ResourcesManager.getTextBack());

        }
        
        Color textColor = GraphicsManager.getTextColor();
        Color menuColor = GraphicsManager.getMenuColor();
		
		title.setBackground(Constants.TRANSPARENT);
		title.setForeground(textColor);
		
		fullscrDesc.setBackground(Constants.TRANSPARENT);
		fullscrDesc.setForeground(textColor);
		
		for (JLabel l : volumeDesc)
		{
			l.setBackground(Constants.TRANSPARENT);
			l.setForeground(textColor);
		}
		
		for (JSlider s : volumeSliders)
		{
			s.setBackground(Constants.TRANSPARENT);
			s.setForeground(textColor);
		}
		
		opacitySlider.setBackground(Constants.TRANSPARENT);
		oLabels.get(Constants.OP_MIN).setForeground(textColor);
		oLabels.get(Constants.OP_MAX).setForeground(textColor);
		
		opacityDesc.setBackground(Constants.TRANSPARENT);
		opacityDesc.setForeground(textColor);
		
		fullscreen.setBackground(menuColor);
		fullscreen.setForeground(textColor);
		
		back.setBackground(menuColor);
		back.setForeground(textColor);
		
		optionsPane.setBackground(menuColor);
	}
	
	@Override
    public void paintComponent(Graphics g)  			// draw game's Narration Window
    {
        float pos 	 = 1.5f;
        Font  font 	 = GraphicsManager.getTextFont();

        updateSize();
        
        prepareBackground();
        prepareSprite();
    
        title.setSize		((int)(0.8  * width), (int)(0.1  * height));
        title.setLocation	((int)(0.05 * width), (int)(0.03 * height));
        title.setHorizontalAlignment(SwingConstants.CENTER);
		title.setFont(window.getScalledFont(font,25));
        
        for (JLabel b : volumeDesc)
        {
        	b.setSize		((int)(0.3  * width), (int)(0.1 * height));
        	b.setLocation	((int)(0.08 * width), (int)(0.1 * pos * height));
			b.setFont(window.getScalledFont(font,18));
	        pos += 1.5f;
        }
        
        if ( speakerS != null && speakerL != null)
        {	
        	vLabels.get(Constants.VOL_MIN).setIcon(window.getScalledIcon(22, speakerS));
        	vLabels.get(Constants.VOL_MAX).setIcon(window.getScalledIcon(22, speakerL));
        }
        
        pos = 2.3f;
        
        for (JSlider s : volumeSliders)
        {
        	s.setSize		((int)(0.3  * width), (int)(0.1 * height));
        	s.setLocation	((int)(0.05 * width), (int)(0.1 * pos * height));
			s.setFont(window.getScalledFont(font,18));
	        pos += 1.5f;
        }
        
       	volumeSliders[Constants.TYPE_VOICE].setVisible(ResourcesManager.isVoiceEnabled());
       	volumeDesc[Constants.TYPE_VOICE].setVisible(ResourcesManager.isVoiceEnabled());
        
        opacityDesc.setSize		((int)(0.15 * width), (int)(0.1  * height));
        opacityDesc.setLocation	((int)(0.55 * width), (int)(0.15 * height));
        opacityDesc.setFont(window.getScalledFont(font,18));
        
		oLabels.get(Constants.OP_MIN).setFont(window.getScalledFont(font,18));
		oLabels.get(Constants.OP_MAX).setFont(window.getScalledFont(font,18));
        
    	opacitySlider.setSize		((int)(0.3  * width), (int)(0.1  * height));
    	opacitySlider.setLocation	((int)(0.55 * width), (int)(0.23 * height));
    	opacitySlider.setFont(window.getScalledFont(font,18));
        
		fullscrDesc.setSize		((int)(0.15 * width), (int)(0.1  * height));
		fullscrDesc.setLocation	((int)(0.55 * width), (int)(0.35 * height));
		fullscrDesc.setFont(window.getScalledFont(font,18));
		
		fullscreen.setSize		((int)(0.05 * height), (int)(0.05 * height));
		fullscreen.setLocation	((int)(0.70 * width),  (int)(0.375 * height));
        
        back.setSize		((int)(0.1 * width),  (int)(0.05 * height));
    	back.setLocation	((int)(0.77 * width), (int)(0.8  * height));
		back.setFont(window.getScalledFont(font,18));
        
        optionsPane.setLocation ((int)(0.05 * width), (int)(0.05 * height));
        optionsPane.setSize	 	((int)(0.9 * width),  (int)(0.9 * height));       
    }
}
