package engine.graphics;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.net.URL;

import javax.swing.ImageIcon;

import engine.Constants;
import engine.graphics.elements.Background;
import engine.graphics.elements.Sprite;
import engine.logics.ResourcesManager;

public class GraphicsManager
{
	// graphics to be stored in verse
	private static boolean		backgroundChanged;	// flag indicating the background is about to change
	private static boolean		spritesChanged;		// flag indicating the sprites are about to change
	private static boolean		shadeVisible;		// flag indicating replay flag is visible on background

	// graphics parameters
	private static int			 opacity;			// menu opacity
	private static Color		 menuColor; 		// color of normal menu items
	private static Color		 popupColor; 		// color of pop-up menu items
	private static Color		 defaultBckgColor;	// color of default background
	private static Color		 textColor;			// normal font color
	private static Color		 inactiveTextColor;	// inactive elements font color
	private static Font			 textFont;			// used font
	// window properties
	private static Window		 window;			// frame containing this game instance GUI
	private static String		 title;				// game window title
	private static ImageIcon  	 icon;	 			// game window icon
	private	static Background 	 menuBckg; 			// main menu background	
	// game panel elements
	protected static ImageIcon[] sprites;			// actually loaded sprites	
	protected static ImageIcon   background;		// actual background

	// --- INITIALIZATION ---
	public static void initialize()
	{	
		sprites = new ImageIcon[Constants.NUM_SPRITES];
		window 	= new Window();
	}
	
	public static void show()
	{ 	
		window.toggleInitialized(true);	//consider window preparation done
		window.setVisible(true);
	}
	
	// --- SETTERS ---
	public static void setTitle(String s)
	{	
		title = s;
		window.setTitle(title);
	}
	
	public static void setWindowIcon(String s)
	{	
		icon = loadImage(s);
		if (icon != null)
		{	window.setIconImage(icon.getImage()); }
	}
	
	public static void setMenuBackground(Background b)
	{	menuBckg = b; }
	
	public static void setMenuColor(Color c)
	{	menuColor = c; }
	
	public static void setPopupColor(Color c)
	{	popupColor = c; }
	
	public static void setDefaultBckgColor(Color c)
	{	defaultBckgColor = c; }
	
	public static void setMenuOpacity(int opc)
	{	
		opacity = opc;
	    	
	    menuColor = new Color(menuColor.getRed(), menuColor.getGreen(), menuColor.getBlue(), opacity);    	
	    if (opacity < 150)
	    { popupColor = new Color(popupColor.getRed(), popupColor.getGreen(), popupColor.getBlue(), 150); }
	    else
	    { popupColor = new Color(popupColor.getRed(), popupColor.getGreen(), popupColor.getBlue(), opacity); }
	 }
	
	public static void setTextColor(Color c)
	{	textColor = c; }
	
	public static void setInactiveTextColor(Color c)
	{	inactiveTextColor = c; }
	
	public static void setTextFont(Font f)
	{	textFont = f; }
	
	public static void setSprite (int pos, Sprite s)				// set character sprite
	{	
		if (s != null)
		{	sprites[pos] = loadImage(s.getPath());	}
		else
		{	sprites[pos] = null; }
		
		updateWindow();
	}
	
	public static void setBackground ()								// set main menu background image
	{ 	setBackground(menuBckg); }
	
	public static void setBackground (Background b)					// set background image
	{ 	
		if (b != null)												// if the specified background is valid
		{				
			if (b.getPath() != null)								//	* and has defined background graphics
			{	
				background = loadImage(b.getPath());
				
				if (background == null || background.getImageLoadStatus() != MediaTracker.COMPLETE)					
				{
					BufferedImage img = new BufferedImage (800, 600, BufferedImage.TYPE_INT_RGB);
					Graphics g = img.getGraphics();
					g.setColor(GraphicsManager.getDefaultBckgColor());
					g.fillRect(0, 0, 800, 600);

					background = new ImageIcon(img);   
				}
			}
			else
			{
				if (b.getColor() != null)
				{
			    	BufferedImage img = new BufferedImage (800, 600, BufferedImage.TYPE_INT_RGB);
					Graphics g = img.getGraphics();
					g.setColor(b.getColor());
					g.fillRect(0, 0, 800, 600);
		
					background = new ImageIcon(img);   	
				}
				else
				{ System.err.println("Corrupted background object!"); }	
		    }
		}
		else
		{
	    	BufferedImage img = new BufferedImage (800, 600, BufferedImage.TYPE_INT_RGB);
			Graphics g = img.getGraphics();
			g.setColor(GraphicsManager.getDefaultBckgColor());
			g.fillRect(0, 0, 800, 600);
			
			background = new ImageIcon(img);   	
		}
	}
	
	public static void toggleBackgroundChanged(boolean b)
	{	backgroundChanged = b; }
	
	public static void toggleSpritesChanged(boolean b)
	{	spritesChanged = b; }
	
	// --- GETTERS ---	
	public static Window getWindow()
	{ 	return window; }
	
	public static String getTitle()
	{	return title; }
	
	public static int getOpacity()
	{	return opacity; }
	
	public static Color getMenuColor()
	{	return menuColor; }
	
	public static Color getPopupColor()
	{	return popupColor; }
	
	public static Color getDefaultBckgColor()
	{	return defaultBckgColor; }
	
	public static Color getTextColor()
	{	return textColor; }
	
	public static Color getInactiveTextColor()
	{	return inactiveTextColor; }
	
	public static Font getTextFont()
	{	return textFont; }
	
	public static ImageIcon getWindowIcon()
	{ 	return icon; }
	
	public static Background getMenuBackground()
	{ 	return menuBckg; }
	
	public static boolean getFullscreen()
	{	return window.isFullscreen(); }
	
	public static ImageIcon getBackground()
	{	return background; }
	
	public static ImageIcon getSprite(int pos)
	{ 	return sprites[pos]; }
	
	public static boolean isBackgroundChanged()
	{ return backgroundChanged; }
	
	public static boolean areSpritesChanged()
	{ return spritesChanged; }
	
	public static boolean isShaded()
	{	return shadeVisible;}
	
	// --- METHODS ---
	public static void updateWindow()
	{	window.update(); }
	
	public static void toggleFullscreen(boolean b)
	{	window.toggleFullscreen(b); }
	
	public static void toggleTerminating(boolean b)
	{	window.toggleTerminating(b); }
	
	public static void toggleShading(boolean b)
	{	shadeVisible = b; }
		
	public static void adjustSlider(int s, int v)
	{	window.adjustSlider(s, v); }
	
	public static ImageIcon loadImage(String path)
	{
		String url = ResourcesManager.getResourceURL(path);  	// get the url to the graphic file
		
		if ( url.isEmpty())																	// if the url is invalid
		{	System.err.println("The specified graphic file doesn't exist: " + path);	}	// print the error message
		else
		{	
			try
			{	return new ImageIcon(new URL(url));	}
			catch (Exception e)
			{ System.err.println("Error when trying to load the specified graphics file"); }
		}
		return null;
	}
	
	public static ImageIcon resizeImage(ImageIcon img, int w, int h)
	{
	    BufferedImage b = new BufferedImage(w, h, BufferedImage.TRANSLUCENT);
	    Graphics2D g = (Graphics2D) b.createGraphics();
	    g.addRenderingHints(new RenderingHints
	    		(	RenderingHints.KEY_RENDERING,
	    			RenderingHints.VALUE_RENDER_QUALITY	));
	    g.drawImage(img.getImage(), 0, 0, w, h, null);
	    g.dispose();
	    return new ImageIcon(b);
	}
}
