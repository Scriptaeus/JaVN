package engine.graphics.elements;

import java.awt.Color;
import java.io.IOException;
import java.io.Serializable;

import datatypes.Data;
import datatypes.Memorizable;
import datatypes.Memory;
import interpreter.exceptions.InterpreterException;

public class Background implements Memorizable, Serializable
{
	private static final long serialVersionUID = 3896933169140142114L;

	private static int 		defId = 0;
	
	private String 			id;
	private String 			path;
	private Color			color;
	
	// --- CONSTRUCTORS ---	
	public Background(String p)
	{
		id	 	= "Bckg_" + String.format("%03d", defId);
		path 	= p;
		color 	= null;
		
		defId++;
	}
		
	public Background(String i, String p)
	{
		id = i;
		path = p;
		color = null;
	}
	
	public Background(String i, Color c)
	{
		id = i;
		path = null;
		color = c;
	}
	
	// --- GETTERS ---
	public String getId()
	{	return id; }
	
	public String getPath()
	{	return path; }
	
	public Color getColor()
	{ return color; }
		
	// --- SETTERS ---
	public void setPath(String p)
	{	this.path = p; }
	
	
	// --- INTERFACE FUNCTIONS ---
	@Override
	public void define(Memory m) throws InterpreterException
	{
		if (!m.isDefined(this.getId()))
		{	m.write(this.getId(), new Data(this)); }
		else
		{	throw new InterpreterException("Background already defined"); }		
	}
	
	@Override
	public String defineSelf()
	{
		String def = "define background (" + this.getId() + ", " + this.getPath() + ")";
		return def;
	}
	
	@Override
	public String defineType()
	{	return "graphics"; }
	
	@Override
	public String getNameString()
	{	return id; }
	
    private void writeObject(java.io.ObjectOutputStream stream) throws IOException 
    { 
    	stream.writeInt	   (defId);
    	
    	stream.writeObject (id);
    	stream.writeObject (path);
    	stream.writeObject (color);
    }
    
	private void readObject(java.io.ObjectInputStream stream) throws IOException, ClassNotFoundException
    {
		defId 	= stream.readInt();
		
    	id 		= (String)stream.readObject();
    	path	= (String)stream.readObject();
    	color 	= (Color)stream.readObject();
    }
}





