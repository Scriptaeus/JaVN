package engine.graphics.dialogs;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

import javax.swing.JButton;
import javax.swing.SwingConstants;

import engine.Constants;
import engine.graphics.GraphicsManager;
import engine.graphics.Window;
import engine.logics.ResourcesManager;

public class DialogExitMain extends Dialogs
{
	private static final long serialVersionUID = 1L;
	
	private JButton		cancel;
	private JButton		exit;

	
	public DialogExitMain(Window w)
	{
		super(w);
		
		setRelativeSize		(0.5,  0.4);
		setRelativeLocation	(0.25 ,0.3);
		
		desc.setText(ResourcesManager.getTextPromptExit());
		
		exit 	= createButton(ResourcesManager.getTextExitGame(), "exit");
        cancel  = createButton(ResourcesManager.getTextCancel(), "cancel");
        		
		menu.add(cancel);
		menu.add(exit);
	}

	@Override
	public void update()
	{
        Color textColor = GraphicsManager.getTextColor();
        Color bckgColor = GraphicsManager.getPopupColor();

        desc.setForeground(textColor);
        desc.setBackground(Constants.TRANSPARENT);
        
        cancel.setForeground(textColor);
        cancel.setBackground(bckgColor);
        
        exit.setForeground(textColor);
        exit.setBackground(bckgColor);
        
        menu.setBackground(bckgColor);
        
		desc.setText(ResourcesManager.getTextPromptExit());
		exit.setText(ResourcesManager.getTextExitGame());
        cancel.setText(ResourcesManager.getTextCancel());
	}
	
	
	@Override
    public void paintComponent(Graphics g)  			// draw game's Narration Window
    {
        int  width  = this.getWidth();
        int  height = this.getHeight();
        Font font 	= GraphicsManager.getTextFont();

        menu.setSize	 (width,  height);
        menu.setLocation (0,	  0);
        
        desc.setSize	 ((int)(0.7 * width), (int)(0.2  * height));
        desc.setLocation ((int)(0.2 * width), (int)(0.25 * height));
        desc.setFont(window.getScalledFont(font,22));
        desc.setVerticalAlignment(SwingConstants.TOP);
        desc.setHorizontalAlignment(SwingConstants.LEFT);
         
        exit.setSize	 	((int)(0.3  * width), (int)(0.15  * height));
        exit.setLocation 	((int)(0.2  * width), (int)(0.5   * height));
        exit.setFont(window.getScalledFont(font,18));
        
        cancel.setSize		((int)(0.3  * width), (int)(0.15  * height));
        cancel.setLocation 	((int)(0.55 * width), (int)(0.5   * height));
        cancel.setFont(window.getScalledFont(font,18));
        
    }
}
