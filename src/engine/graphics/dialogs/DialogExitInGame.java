package engine.graphics.dialogs;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

import javax.swing.JButton;
import javax.swing.SwingConstants;

import engine.Constants;
import engine.graphics.GraphicsManager;
import engine.graphics.Window;
import engine.logics.ResourcesManager;

public class DialogExitInGame extends Dialogs
{
	private static final long serialVersionUID = 1L;
	
	private JButton		cancel;
	private JButton		mainm;
	private JButton		exit;

	public DialogExitInGame(Window w)
	{
		super(w);
		
		setRelativeSize		(0.7,   0.75);
		setRelativeLocation	(0.15 , 0.1 );
		
		desc.setText("<html>" + ResourcesManager.getTextPromptExit() + "<br>" + ResourcesManager.getTextPromptLose() + "</html>");
				
		cancel 	= createButton(ResourcesManager.getTextGoBack(), "cancel");
		mainm 	= createButton(ResourcesManager.getTextExitMenu(), "main_menu");
        exit 	= createButton(ResourcesManager.getTextExitGame(), "exit");
		
		menu.add(cancel);
		menu.add(mainm);
		menu.add(exit);
	}

	@Override
	public void update()
	{
        Color textColor = GraphicsManager.getTextColor();
        Color bckgColor = GraphicsManager.getPopupColor();
        
        desc.setForeground(textColor);
        desc.setBackground(Constants.TRANSPARENT);
        
        cancel.setForeground(textColor);
        cancel.setBackground(bckgColor);

        mainm.setForeground(textColor);
        mainm.setBackground(bckgColor);
        
        exit.setForeground(textColor);
        exit.setBackground(bckgColor);
        
        menu.setBackground(bckgColor);
        
		desc.setText("<html>" + ResourcesManager.getTextPromptExit() + "<br>" + ResourcesManager.getTextPromptLose() + "</html>");
		cancel.setText(ResourcesManager.getTextGoBack());
		mainm.setText(ResourcesManager.getTextExitMenu());
        exit.setText(ResourcesManager.getTextExitGame());
	}
	
	@Override
    public void paintComponent(Graphics g)  			// draw game's Narration Window
    {
        int width   = this.getWidth();
        int height  = this.getHeight();
        Font font 	= GraphicsManager.getTextFont();

        menu.setSize	 (width,  height);
        menu.setLocation (0,	  0);
        
        desc.setSize	 ((int)(0.7  * width), (int)(0.2  * height));
        desc.setLocation ((int)(0.15 * width), (int)(0.15 * height));
        desc.setFont(window.getScalledFont(font,22));
        desc.setVerticalAlignment(SwingConstants.TOP);
        desc.setHorizontalAlignment(SwingConstants.CENTER);
         
        cancel.setSize		((int)(0.3  * width), (int)(0.1  * height));
        cancel.setLocation 	((int)(0.35 * width), (int)(0.4  * height));
        cancel.setFont(window.getScalledFont(font,18));
        
        mainm.setSize	 	((int)(0.3  * width), (int)(0.1   * height));
        mainm.setLocation 	((int)(0.35 * width), (int)(0.55  * height));
        mainm.setFont(window.getScalledFont(font,18));
        
        exit.setSize	 	((int)(0.3  * width), (int)(0.1  * height));
        exit.setLocation 	((int)(0.35 * width), (int)(0.7  * height));
        exit.setFont(window.getScalledFont(font,18));      
    }
}
