package engine.graphics.dialogs;

import java.awt.Dimension;
import java.awt.Point;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import engine.graphics.Window;

public abstract class Dialogs extends JPanel
{
	private static final long serialVersionUID = 1L;
	
	protected Window			window;
	
	protected double 	relSizeW;
	protected double 	relSizeH;
	protected double	relLocW;
	protected double	relLocH;
	
	protected JLabel	menu;
	protected JLabel	desc;
	
	public Dialogs(Window w)
	{
		window = w;
		
        menu = new JLabel();
        menu.setOpaque(true);
        menu.setVisible(true);
        
        desc = new JLabel("Some text");
        desc.setOpaque(true);
        desc.setVisible(true);

        menu.add(desc);
        add(menu);
        
        this.setOpaque(true);
        this.setVisible(false);
	}
	
	public void setRelativeLocation(double w, double h)
	{ 
		relLocW = w;
		relLocH = h;
	}
	
	protected JButton createButton(String name, String command)
	{
		JButton b = new JButton(name);
		b.setOpaque(true);
		b.setVisible(true);
        b.addActionListener(window);
        b.setActionCommand(command);
        b.setFocusable(false);
        
        return b;
	}
	
	public void setRelativeSize(double w, double h)
	{
		relSizeW = w;
		relSizeH = h;
	}
	
	public Point getAbsoluteLocation()
	{ 
		Dimension d = window.getScreenSize();
		return new Point ((int)(relLocW * d.getWidth()), (int)(relLocH * d.getHeight()));
	}
	
	public Dimension getAbsoluteSize()
	{ 
		Dimension d = window.getScreenSize();
		return new Dimension((int)(relSizeW * d.getWidth()), (int)(relSizeH * d.getHeight()));
	}
	
	public void clear() {}
	
	public void update() {}

}
