package engine.graphics.dialogs;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

import javax.swing.JButton;
import javax.swing.SwingConstants;

import engine.Constants;
import engine.graphics.GraphicsManager;
import engine.graphics.Window;
import engine.logics.ResourcesManager;

public class DialogConfirm extends Dialogs
{
	private static final long serialVersionUID = 1L;

	private JButton		ok;
	private JButton		cancel;

	public DialogConfirm(Window w)
	{
		super(w);
		
		setRelativeSize		(0.5,  0.3);
		setRelativeLocation	(0.25 ,0.35);
		
		desc.setText("Placeholder");
		
		ok 		= createButton("OK", "");
        cancel  = createButton(ResourcesManager.getTextCancel(), "cancel_sl");

		menu.add(ok);
		menu.add(cancel);
	}
	
	public void setContext(boolean c, int i)
	{
		if (c == Constants.CONTEXT_SAVE)
		{
			desc.setText("<html><center>" + ResourcesManager.getTextPromptSave() + "<br>" + ResourcesManager.getTextPromptOverwrite() + "</center></html>");
			ok.setActionCommand("ok_overwrite"+i);
			
		}
		else if (c == Constants.CONTEXT_LOAD)
		{
			desc.setText("<html><center>" + ResourcesManager.getTextPromptLoad() + "<br>" + ResourcesManager.getTextPromptLose() + "</center></html>");
			ok.setActionCommand("ok_load"+i);
		}
	}

	@Override
	public void update()
	{
        Color textColor = GraphicsManager.getTextColor();
        Color bckgColor = GraphicsManager.getPopupColor();

        desc.setForeground(textColor);
        desc.setBackground(Constants.TRANSPARENT);
        
        ok.setForeground(textColor);
        ok.setBackground(bckgColor);
        
        cancel.setForeground(textColor);
        cancel.setBackground(bckgColor);
        
        menu.setBackground(bckgColor);
        
        cancel.setText(ResourcesManager.getTextCancel());
	}
	
	@Override
    public void paintComponent(Graphics g)  			// draw game's Narration Window
    {
        int  width  = this.getWidth();
        int  height = this.getHeight();
        Font font 	= GraphicsManager.getTextFont();

        menu.setSize	 (width,  height);
        menu.setLocation (0,	  0);
        
        desc.setSize	 ((int)(0.8 * width), (int)(0.4  * height));
        desc.setLocation ((int)(0.1 * width), (int)(0.1  * height));
        desc.setFont(window.getScalledFont(font,22));
        desc.setVerticalAlignment(SwingConstants.BOTTOM);
        desc.setHorizontalAlignment(SwingConstants.CENTER);
         
        cancel.setSize	 	((int)(0.3  * width), (int)(0.2 * height));
        cancel.setLocation 	((int)(0.2  * width), (int)(0.6 * height));
        cancel.setFont(window.getScalledFont(font,18));
        
        ok.setSize		((int)(0.3  * width), (int)(0.2  * height));
        ok.setLocation 	((int)(0.55 * width), (int)(0.6  * height));
        ok.setFont(window.getScalledFont(font,18));
        
    }
}
