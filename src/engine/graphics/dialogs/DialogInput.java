package engine.graphics.dialogs;

import java.awt.*;
import javax.swing.*;

import engine.Constants;
import engine.graphics.GraphicsManager;
import engine.graphics.Window;
import engine.logics.ResourcesManager;

public class DialogInput extends Dialogs
{
	private static final long serialVersionUID = 1L;
	
	private JLabel		alert;
	private JButton 	confirm;
	private JTextField	input;
	
	public DialogInput(Window w)
	{
		super(w);

		setRelativeSize		(0.5,   0.3);
		setRelativeLocation	(0.25,  0.25);
		
		alert = new JLabel("<html>" + ResourcesManager.getTextPromptEmpty() + "</html>");
		alert.setOpaque(true);
		alert.setVisible(false);

		confirm = createButton("OK", "confirm_input");
				
		input = new JTextField();
		input.setOpaque(true);
		input.setVisible(true);
		input.setCursor(Cursor.getDefaultCursor());
		input.addKeyListener(window);
		
		menu.add(alert);
		menu.add(input);
		menu.add(confirm);		
	}

	public void showAlert(boolean b)
	{ alert.setVisible(b); }

	public void setQuestion(String q)
	{ desc.setText("<html>" + q + "</html>"); }
	
	public void clearInput()
	{ input.setText(""); }
	
	public String getAnswer()
	{ return input.getText(); }
	
	// --- METHODS ---	
	@Override
	public void clear()
	{	input.setText(""); }
	
	@Override
	public void update()
	{
        Color textColor = GraphicsManager.getTextColor();
        Color bckgColor = GraphicsManager.getPopupColor();
		
        desc.setForeground(textColor);
        desc.setBackground(Constants.TRANSPARENT);
        
        alert.setForeground(textColor);
        alert.setBackground(Constants.TRANSPARENT);
        
        confirm.setForeground(textColor);
        confirm.setBackground(bckgColor);
        
        input.setForeground(textColor);
        input.setCaretColor(textColor);
        input.setBackground(new Color( bckgColor.darker().getRed(),
        							   bckgColor.darker().getGreen(),
        							   bckgColor.darker().getBlue(),
        							   bckgColor.getAlpha() ));
        
        menu.setBackground(bckgColor);
        
		alert.setText("<html>" + ResourcesManager.getTextPromptEmpty() + "</html>");
	}
	
	@Override
	public void setVisible(boolean b)
	{
		super.setVisible(b);
		if (b && input != null)
		{	input.requestFocus(); }
	}
	
	@Override
    public void paintComponent(Graphics g)  			// draw game's Narration Window
    {
        int width  = this.getWidth();
        int height = this.getHeight();
        Font font 	= GraphicsManager.getTextFont();

        menu.setSize	 (width,  height);
        menu.setLocation (0,	  0);
        
        desc.setSize	 ((int)(0.85  * width), (int)(0.45  * height));
        desc.setLocation ((int)(0.075 * width), (int)(0.1  * height));
        desc.setFont(window.getScalledFont(font,18));
        desc.setVerticalAlignment(SwingConstants.CENTER);
        desc.setHorizontalAlignment(SwingConstants.LEFT);
        
        alert.setSize	  ((int)(0.9 * width),  (int)(0.1 * height));
        alert.setLocation ((int)(0.05 * width), (int)(0.85 * height));
        alert.setFont(window.getScalledFont(font,18));
        alert.setVerticalAlignment(SwingConstants.CENTER);
        alert.setHorizontalAlignment(SwingConstants.CENTER);
         
        input.setSize		((int)(0.55  * width), (int)(0.15 * height));
        input.setLocation 	((int)(0.075 * width), (int)(0.65  * height));
        input.setFont(window.getScalledFont(font,18));
        input.setHorizontalAlignment(SwingConstants.CENTER);
       
        confirm.setSize	 	((int)(0.225 * width), (int)(0.15 * height));
        confirm.setLocation ((int)(0.7   * width), (int)(0.65  * height));
        confirm.setFont(window.getScalledFont(font,18));   
    }
}
