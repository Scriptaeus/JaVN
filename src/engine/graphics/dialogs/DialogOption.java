package engine.graphics.dialogs;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

import javax.swing.JButton;
import javax.swing.SwingConstants;

import engine.Constants;
import engine.graphics.GraphicsManager;
import engine.graphics.Window;

public class DialogOption extends Dialogs
{

	private static final long serialVersionUID = 1L;
	
	private JButton[] 	buttons;
	private int			buttonsCount;

	// --- CONTRUCTOR ---
	public DialogOption(Window w)
	{
		super(w);
		
		setRelativeSize		(0.9,  0.8);
		setRelativeLocation	(0.05, 0.05);
		
		buttonsCount = 0;
		buttons = new JButton[Constants.NUM_OPTIONS];		
		for (int i = 0 ; i < Constants.NUM_OPTIONS ; i++)
		{
			buttons[i] = createButton("", "Option_" + (i+1));
			menu.add(buttons[i]);
		}
	}
	
	// --- GETTERS ---
	public int getButtonsCount()
	{	return buttonsCount; }
	
	// --- SETTERS ---
	public void setQuestion(String q)
	{ desc.setText("<html>" + q + "</html>"); }
	
	public void setOptions(String[] questions)
	{
		buttonsCount = questions.length < Constants.NUM_OPTIONS ? questions.length : Constants.NUM_OPTIONS; 
				
		for (int i = 0; i < buttonsCount ; i++)
		{	
			buttons[i].setText((i+1) + ". " + questions[i]);
			buttons[i].setVisible(true);
		}
	}
	
	// --- METHODS ---
	@Override
	public void clear()
	{
		buttonsCount = 0; 
				
		for (JButton b : buttons)
		{	
			b.setText("");
			b.setVisible(false);
		}
	}
	
	@Override
	public void update()
	{
        Color textColor = GraphicsManager.getTextColor();
        Color bckgColor = GraphicsManager.getPopupColor();
        
        desc.setForeground(textColor);
        desc.setBackground(Constants.TRANSPARENT);
        
        for (JButton b : buttons)
        {
	        b.setForeground(textColor);
	        b.setBackground(Constants.TRANSPARENT);
        }
        
        menu.setBackground(bckgColor);
	}
	
	@Override
    public void paintComponent(Graphics g)  			// draw game's Narration Window
    {
        int  width  = this.getWidth();
        int  height = this.getHeight();
        Font font 	= GraphicsManager.getTextFont();

        menu.setSize	 (width,  height);
        menu.setLocation (0,	  0);
        
        desc.setSize	 ((int)(0.85  * width), (int)(0.3   * height));
        desc.setLocation ((int)(0.075 * width), (int)(0.05  * height));
        desc.setFont(window.getScalledFont(font,20));
        desc.setVerticalAlignment(SwingConstants.CENTER);
        desc.setHorizontalAlignment(SwingConstants.CENTER);
        
        if (buttonsCount < 6)
        {
	        for (int i = 0; i < buttonsCount; i++)
	        {
	        	buttons[i].setSize	   ((int)(0.6  * width), (int)(0.1   * height));
	        	buttons[i].setLocation ((int)(0.2  * width), (int)(((0.4 + (0.115 * i))  * height)));
	        	buttons[i].setFont(window.getScalledFont(font,18));
	        }
        }
        else
        {
	        for (int i = 0; i < buttonsCount; i++)
	        {
	        	buttons[i].setSize	   ((int)(0.3  * width), (int)(0.1   * height));
	        	buttons[i].setLocation ((int)((0.15 + (0.4 * (i % 2))) * width), (int)((0.4 + 0.115 * (i / 2))  * height));
	        	buttons[i].setFont(window.getScalledFont(font,18));
	        }
        }
        
    }
}
