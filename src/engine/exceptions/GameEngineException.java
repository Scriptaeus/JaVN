package engine.exceptions;

public class GameEngineException extends Exception
{
	private static final long serialVersionUID = 6678895716684197692L;
	private String description;
	private String component;

	public GameEngineException(String c, String component)
	{
		this.description = c;
		this.component = component;
	}
	
	public String getDescription()
	{ return description; }
	
	public String getComponent()
	{ return component; }

}
