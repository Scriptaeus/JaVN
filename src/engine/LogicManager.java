package engine;

import datatypes.Memory;
import engine.graphics.GraphicsManager;
import engine.logics.ResourcesManager;
import engine.logics.elements.Verse;
import engine.sounds.SoundManager;
import interpreter.Interpreter;
import interpreter.exceptions.InterpreterException;

public class LogicManager
{
    private static 			Interpreter interpreter;				// interpreter used by this program instance

    // --- INIT FUNCTIONS ---    
	public static void start (Interpreter i)
	{
        interpreter = i;
		GraphicsManager.initialize();
		ResourcesManager.loadConfig();
		SoundManager.playMenuMusic();
		GraphicsManager.setBackground();
		GraphicsManager.show();
	}	

	// --- GETTERS ---
	public static Memory getMemory()
	{ return interpreter.getMemory(); }
	
	// --- SETTERS ---
	public static void setMemory(Memory m)
	{ interpreter.setMemory(m); }
	
	// --- METHODS ---	
	public static void resumeInterpretation()
	{ interpreter.resume(); }
	
	public static Verse peekNextVerse()
	{ return interpreter.getStoryNextVerse(); }
	
	public static Verse readFirstVerse()
	{
		interpreter.initialize();
		
        Verse v;
		try
		{
	        interpreter.process();
        	v = interpreter.getStoryFirstVerse();
        	v.useBoundedMedia();
		}
		catch (InterpreterException e)
		{ 
			v = new Verse(Constants.ACTOR_ERROR, "\n"
			+ "line " + e.getLine() + ":\n"
			+ "    " + e.getDescription());
		}
		
		return v;
	}
	
	public static Verse readFirstLoadedVerse()
	{
		interpreter.initializeLoaded();
		
        Verse v;
		try
		{			
	        interpreter.process();
        	v = interpreter.getStoryFirstVerse();
        	v.useBoundedMedia();
		}
		catch (InterpreterException e)
		{	v = new Verse(Constants.ACTOR_ERROR, "\n" + "line " + e.getLine() + ":\n" + "    " + e.getDescription()); }
		
		return v;
	}

	public static Verse readNextVerse()
    {
		Verse v;
		
		try
		{ 
			if (interpreter.getStoryNextVerse() == null)			// in case when a new verse shall be parsed
			{	
				SoundManager.clearSceneSound(); 					// clear active sound so they won't resume 
				interpreter.getMemory().setActiveSound(null);		// clear active sound as it won't belong to new verse
				interpreter.getMemory().setActiveVoice(null);		// clear active voice as it won't belong to new verse
				interpreter.process();
				
				if (interpreter.getStorySize() > Constants.NUM_VERSES +1)	// check if number of story verses is within the limit (+1 for actual verse)
				{	interpreter.rebase(); }							// if not, cut the first verse
			}
			
			v = interpreter.moveToNextVerse();
			
			if(v != null)
			{	v.useBoundedMedia(); }					// Uses the media changed around this verse 
			
		}
		catch (InterpreterException e)
		{ 
			v = new Verse(Constants.ACTOR_ERROR, "\n" + "line " + e.getLine() + ":\n" + "    " + e.getDescription());
			GraphicsManager.toggleTerminating(true);
		}
		
		if (v == null)
		{	GraphicsManager.toggleTerminating(true); }
		
		return v;
    }
	
	public static Verse readPreviousVerse()
    {
		Verse v;
		try
		{	
			v = interpreter.moveToPreviousVerse();
			if(v != null)
			{	v.useBoundedMedia(); }					// Plays the music specified in argument 
		}
		catch (InterpreterException e)
		{
			v = new Verse(Constants.ACTOR_ERROR, "\n" + "line " + e.getLine() + ":\n" + "    " + e.getDescription());			
			GraphicsManager.toggleTerminating(true);
		}
		
		return v;
    }
	
	public static Verse readActualVerse()
	{	return interpreter.getStoryActualVerse(); }
	
	public static Verse readLastVerse()
	{	return interpreter.moveToLastVerse(); }
	
	public static void removeLastVerse()
	{	interpreter.remove(); }
	
	public static void addVerse(Verse v)
	{	interpreter.add(v); }
	
	
}
