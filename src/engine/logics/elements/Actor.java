package engine.logics.elements;

import datatypes.Data;
import datatypes.Memorizable;
import datatypes.Memory;
import engine.Constants;
import interpreter.exceptions.InterpreterException;

public class Actor implements Memorizable
{
	private static final long serialVersionUID = 4464476361668924225L;
	
	private static int 			luID = 0;
		
	private String 				ID;
	private String 				name;
	private String				colorHex;
	
	// ----- CONSTRUCTORS -----
	
	public Actor(String n, String c)
	{
		Actor.luID++;
		
		this.ID = "Act" + String.format("%03d", luID);
		this.name = n;
		this.colorHex = c;
	}
	
	public Actor(String id, String n, String c)
	{
		this.ID = id;
		this.name = n;
		this.colorHex = c;
	}
	
	// ----- GETTERS -----
	
	public String getID()
	{ return ID; }
	
	public String getName()
	{ return name; }
	
	public String getColor()
	{ return colorHex; }
	
	// ----- SETTERS -----
	
	public void setName(String n)
	{ name = n; }
	
	public void setColor(String c)
	{ colorHex = c; }
	
	
	// ----- DEFAULT ACTOR AND METHODS DEALING WITH IT -----
	
	public static void redefineDefaultActorName(String n)
	{ Constants.ACTOR_DEFAULT.setName(n);	}
	
	public static void redefineDefaultActorColor(String c)
	{ Constants.ACTOR_DEFAULT.setColor(c); }

	@Override
	public void define(Memory m) throws InterpreterException
	{
		if (!m.isDefined(this.getID()))
		{ m.write(this.getID(), new Data(this)); }
		else
		{ throw new InterpreterException("Actor already defined");	}		
	}
	
	@Override
	public String defineSelf()
	{
		String def = "define actor (" + this.getID() + ", " + this.getName()
				+ ", " + this.getColor() + ")";
		return def;
	}
	
	@Override
	public String defineType()
	{ return "actor"; }
	
	@Override
	public String getNameString()
	{ return name; }
}
