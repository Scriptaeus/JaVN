package engine.logics.elements;

import java.io.Serializable;

import datatypes.Memory;
import engine.Constants;
import engine.LogicManager;
import engine.graphics.GraphicsManager;
import engine.graphics.elements.Background;
import engine.graphics.elements.Sprite;
import engine.sounds.SoundManager;
import engine.sounds.elements.Music;
import interpreter.Analyzer;

public class Verse implements Serializable
{
	private static final long serialVersionUID = 6715913991908244819L;

	protected int lineNr;
	
	private boolean seen;
	private boolean question;
	private String  statement;
	private Actor   speaker;
	
	private Music 		changeMusic;
	private Background	changeBackground;
	private Sprite[]	changeSprites;
		
	private transient Verse previous;
	private transient Verse next;
	
	// ----- CONSTRUCTORS -----
	
	public Verse (Actor a, String st)
	{
		lineNr = Analyzer.getPreviousLine();
		seen = false;
		speaker = a;
		statement = st;
		setQuestion(false);
		fetchMedia();
		
	}
	
	public Verse (String st)
	{
		lineNr = Analyzer.getPreviousLine();
		seen = false;
		speaker = Constants.ACTOR_DEFAULT;
		statement = st;
		fetchMedia();
		setQuestion(false);		
		setPrevious(null);
		setNext(null);
		fetchMedia();
	}
	
	public Verse (Verse v)
	{
		lineNr 			 = Analyzer.getPreviousLine();
		seen 			 = false;
		speaker 		 = v.speaker;
		statement 		 = v.statement;
		changeMusic 	 = v.changeMusic;
		changeBackground = v.changeBackground;
		changeSprites 	 = v.changeSprites;
		setQuestion(false);
		setPrevious(null);
		setNext(null);
		fetchMedia();
	}
	
	// ----- GETTERS & SETTERS -----
	
	
	public void setNext(Verse v)
	{ next = v; }
	
	public void setPrevious(Verse v)
	{ previous = v; }
	
	public void setStatement(String s)
	{ statement = s; }
	
	public void setQuestion(boolean q)
	{ question = q; }
	
	public void setMusicChange(Music m)
	{ changeMusic = m; }
	
	public void setBackgroundChange(Background b)
	{ 	changeBackground = b; }
	
	public void setSpritesChange(Sprite[] s)
	{ 	
		Sprite[] activeSprites = LogicManager.getMemory().getActiveSprites();
		
		if (activeSprites != null)
		{
			changeSprites = new Sprite[Constants.NUM_SPRITES];
			changeSprites[Constants.POS_LEFT] 	= s[Constants.POS_LEFT];
			changeSprites[Constants.POS_CENTER] = s[Constants.POS_CENTER];
			changeSprites[Constants.POS_RIGHT] 	= s[Constants.POS_RIGHT];
		}
	}
	
	public void setSeen()
	{ seen = true; }
	
	// -----

	public Verse getNext()
	{ return next; }
	
	public Verse getPrevious()
	{ return previous; }
	
	public boolean getQ()
	{ return question; }
	
	public boolean isSeen()
	{ return seen; }
	
	public int getLineNr()
	{ return lineNr; }	
	
	public Actor getSpeaker()
    { return speaker; }
	
	public String getSpeakerColor()
    { return speaker.getColor(); }
        
	public String getSpeakerName()
	{ return speaker.getName(); }
	
	public String getStatement()
	{ return statement; }
	
	public void useBoundedMedia()
	{ 
		if (changeMusic != null)
		{
			if (SoundManager.musicDiffers(changeMusic.getPath()))
			{
				SoundManager.stopMusic(); 							// Stops currently playing music
				SoundManager.playMusic(changeMusic.getPath());		// Plays the music stored in this verse
			}
		}
		
		if (changeBackground != null)
		{	GraphicsManager.setBackground(changeBackground); }
		
		if (changeSprites != null)
		{	
			GraphicsManager.setSprite(Constants.POS_LEFT, changeSprites[Constants.POS_LEFT]);
			GraphicsManager.setSprite(Constants.POS_CENTER, changeSprites[Constants.POS_CENTER]);
			GraphicsManager.setSprite(Constants.POS_RIGHT, changeSprites[Constants.POS_RIGHT]);
		}
	}
	
	public Background getBackgroundChange()
	{ return changeBackground; }
	
	public Sprite[] getSpritesChange()
	{ return changeSprites; }
	
	// ----- METHODS -----
	
	public void addToStory(Memory m)	// add verse to actual memory's story
	{ m.getStory().add(this); }
	
	public void present()				// presents verse in window interface
	{
		GraphicsManager.getWindow().setContent(this);
		setSeen();
	}
	
	public void fetchMedia()
	{
		if(SoundManager.isMusicChanged())
		{	
			changeMusic = LogicManager.getMemory().getActiveMusic();
			SoundManager.toggleMusicChanged(false);
		}
		
		if(GraphicsManager.isBackgroundChanged())
		{	
			changeBackground = LogicManager.getMemory().getActiveBackground();
			GraphicsManager.toggleBackgroundChanged(false);
		}
		
		if(GraphicsManager.areSpritesChanged())
		{	
			Sprite[] activeSprites = LogicManager.getMemory().getActiveSprites();
			
			if (activeSprites != null)
			{
				changeSprites = new Sprite[Constants.NUM_SPRITES];
				changeSprites[Constants.POS_LEFT] 	= activeSprites[Constants.POS_LEFT];
				changeSprites[Constants.POS_CENTER] = activeSprites[Constants.POS_CENTER];
				changeSprites[Constants.POS_RIGHT] 	= activeSprites[Constants.POS_RIGHT];
			}			
			GraphicsManager.toggleSpritesChanged(false);
		}
	}
	
	public void presentResult(String ans) {}
}
