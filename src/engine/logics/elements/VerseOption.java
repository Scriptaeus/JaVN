package engine.logics.elements;

import datatypes.Memory;
import datatypes.StoryData;
import engine.LogicManager;
import engine.graphics.GraphicsManager;

import interpreter.Parser;
import interpreter.exceptions.InterpreterException;
import interpreter.programs.Program;

public class VerseOption extends Verse
{
	private static final long serialVersionUID = 3718533046516760945L;
	
	private StoryData[] options;
		
	// ----- CONSTRUCTORS -----
	
	public VerseOption (Verse p, StoryData[] o, int ln)
	{
		super(p);

		lineNr = ln;
		options = o;
		setQuestion(true);
		
		setPrevious(p);
		setNext(null);
	}
	
	// ----- GETTERS -----
	
	public StoryData[] getOptions()
	{ return options; }
	
   	// ----- METHODS -----
	
	public void evaluate (Memory m, int i)
	{
		Parser.setInput(options[i].getInstruction());							// send instruction to parser
		try
		{
			Program p = Parser.parseProgram();									// parse instruction
			p.evaluate(m);														// add/modify variable in memory
			System.out.println("Evaluated: " + options[i].getInstruction() + "!");	
		}
		catch (InterpreterException e)
		{ System.out.println("Bad instruction: " + options[i].getInstruction() + "!"); }
	}
	

	@Override
	public void present() 							// presents dialog with options
	{
    	String[] answers = new String[options.length];
    	
	   	for (int i = 0; i< options.length; i++)
	   	{ answers[i] = options[i].getAnswer(); }
	   	
	   	GraphicsManager.getWindow().showOptionsDialog(getStatement(), answers);
	   	GraphicsManager.getWindow().toggleAdvancing(false);	   
	}
	
	@Override
	public void presentResult(String ans)			// presents results fetched from dialog
	{
		int an = Integer.parseInt(ans);
	    String answer = options[an].getAnswer();
		
	    evaluate(LogicManager.getMemory(), an);
		LogicManager.resumeInterpretation();
	    GraphicsManager.getWindow().setDependentContent(this, answer);
	    this.setSeen();
	    GraphicsManager.getWindow().toggleAdvancing(true);   	   
	}
	
}
