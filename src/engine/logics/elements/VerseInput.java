package engine.logics.elements;

import datatypes.Data;
import datatypes.Memory;
import engine.LogicManager;
import engine.graphics.GraphicsManager;

public class VerseInput extends Verse
{
	private static final long serialVersionUID = 8752346540365217679L;
	
	private String var;			// variable name
		
	// ----- CONSTRUCTORS -----
	
	public VerseInput (Verse p, String v, int ln)
	{
		super(p);
		
		lineNr = ln;
		var = v;
		
		setQuestion(true);
		setPrevious(p);
		setNext(null);
	}
	
   	// ----- METHODS -----
	
	public void evaluate (Memory m, String s)
	{ m.write(var, new Data(s)); }								// TODO: need to be varied depending on data type!
	
	@Override
	public void present() 							// presents dialog with input
	{ 
		GraphicsManager.getWindow().showInputDialog(getStatement());
		GraphicsManager.getWindow().toggleAdvancing(false);
	}
	
	@Override
	public void presentResult(String ans)					// presents results fetched from dialog
	{
	    evaluate(LogicManager.getMemory(), ans);
		LogicManager.resumeInterpretation();
	    GraphicsManager.getWindow().setDependentContent(this, ans);
	    setSeen();
	    GraphicsManager.getWindow().toggleAdvancing(true);
	}
}
