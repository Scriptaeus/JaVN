package engine.logics;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.awt.Color;
import java.awt.Font;
import java.awt.GraphicsEnvironment;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Properties;
import java.util.TreeSet;

import engine.Constants;
import engine.LogicManager;
import engine.exceptions.GameEngineException;
import engine.graphics.GraphicsManager;
import engine.graphics.elements.Background;
import engine.sounds.SoundManager;

public class ResourcesManager
{
	private static boolean		extrasEnabled;					// true if the extras menu is enabled in configuration file
	private	static boolean		voicesEnabled;					// true if voiceover is enabled in configuration file
	
	// localization slogans
	private static String textButtonNewGame		= "";
	private static String textButtonLoadGame	= "";
	private static String textButtonSaveGame	= "";
	private static String textButtonOptions		= "";
	private static String textButtonExtras		= "";
	private static String textButtonExit		= "";
	private static String textButtonBack		= "";
	private static String textButtonCancel		= "";
	private static String textButtonExitGame	= "";
	private static String textButtonExitMenu	= "";
	private static String textButtonGoBack		= "";
	
	private static String textControlMusic		= "";
	private static String textControlSound		= "";
	private static String textControlVoice		= "";
	private static String textControlOpacity	= "";
	private static String textControlFullscreen	= "";
	
	private static String textPromptExit		= "";
	private static String textPromptSave		= "";
	private static String textPromptLoad		= "";
	private static String textPromptLose		= "";
	private static String textPromptOverwrite	= "";
	private static String textPromptEmpty		= "";
	private static String textPromptAnswer		= "";
	private static String textPromptReplay		= "";
	
	// properties manager
	private static Properties 	settings = new Properties()		// program configuration
	{
		private static final long serialVersionUID = -166643657016210421L;

		@Override
		public synchronized Enumeration<Object> keys()
		{	return Collections.enumeration(new TreeSet<Object>(super.keySet())); }
	};
	
	
	// --- GETTERS ---
	public static String getTextNewGame()
	{	return textButtonNewGame; }
	
	public static String getTextLoadGame()
	{	return textButtonLoadGame; }
	
	public static String getTextSaveGame()
	{ 	return textButtonSaveGame; }
	
	public static String getTextOptions()
	{	return textButtonOptions; }
	
	public static String getTextExtras()
	{	return textButtonExtras; }
	
	public static String getTextExit()
	{	return textButtonExit; }
	
	public static String getTextBack()
	{ 	return textButtonBack; }
	
	public static String getTextCancel()
	{	return textButtonCancel; }
	
	public static String getTextExitGame()
	{	return textButtonExitGame; }
	
	public static String getTextExitMenu()
	{	return textButtonExitMenu; }
	
	public static String getTextGoBack()
	{	return textButtonGoBack; }
	
	public static String getTextMusic()
	{	return textControlMusic; }
	
	public static String getTextSound()
	{	return textControlSound; }
	
	public static String getTextVoice()
	{	return textControlVoice; }
	
	public static String getTextOpacity()
	{	return textControlOpacity; }
	
	public static String getTextFullscreen()
	{	return textControlFullscreen; }
	
	public static String getTextPromptExit()
	{	return textPromptExit; }
	
	public static String getTextPromptSave()
	{	return textPromptSave; }
	
	public static String getTextPromptLoad()
	{	return textPromptLoad; }
	
	public static String getTextPromptLose()
	{	return textPromptLose; }
	
	public static String getTextPromptOverwrite()
	{	return textPromptOverwrite; }
	
	public static String getTextPromptEmpty()
	{	return textPromptEmpty; }
	
	public static String getTextPromptAnswer()
	{	return textPromptAnswer; }
	
	public static String getTextPromptReplay()
	{	return textPromptReplay; }
	
	public static String getResourceURL(String path)
	{
		String output = "";													// url to the resource
		
		URL url   = LogicManager.class.getClassLoader().getResource(path);  // try to load the resource from .jar package
		if ( url != null)													// *  if there is such resource included
		{	output  = url.toString(); }										// 	  get the url to this resource
		else
		{																	// *  if not, presume it's a path to a file
			if (new File(path).exists())									// **	if the path is valid
			{	output = new File(path).toURI().toString(); }				//		get the url to this file
		}																	// **	else return empty string
		
		return output;
	}
	
	public static  boolean isExtrasEnabled()
	{	return extrasEnabled;	}
	
	public static  boolean isVoiceEnabled()
	{	return voicesEnabled;	}
	
	// --- METHODS ---
	public static void loadConfig()
	{
		File 				config 		= new File(Constants.PATH_CONFIG);
		InputStream	 		in 			= null;
		InputStreamReader 	ins			= null;
		OutputStream 		out			= null;
		
		try
		{
			if (config.exists())
			{
				try
				{
					in = new FileInputStream(config);
					ins = new InputStreamReader(in, "windows-1250");
					settings.load(ins);			
					applySettings();				
				}
				catch (IOException e)
				{	throw new GameEngineException(e.getMessage(), ResourcesManager.class.getName()); }
				finally
				{
					if (in != null)
					{
						try {	in.close();	}
						catch (IOException e)
						{	throw new GameEngineException(e.getMessage(), ResourcesManager.class.getName()); }
					}
				}
			}
			else
			{
				try
				{
					out = new FileOutputStream(config);
	
					// set the properties value
					settings.setProperty("window_title", 			"JaVN Alpha");
					settings.setProperty("path_icon", 				"resources/icon.png");
					settings.setProperty("path_menu_background",	"");
					
					settings.setProperty("volume_music",			"50");
					settings.setProperty("volume_sound",			"50");
					settings.setProperty("volume_voice",			"50");
					
					settings.setProperty("color_opacity",			"150");
					settings.setProperty("color_menu",				"0x525252");
					settings.setProperty("color_popup",				"0x323232");
					settings.setProperty("color_background",		"0x000000");
					settings.setProperty("color_text_inactive",		"0xD3D3D3");
					settings.setProperty("color_text",				"0xFFFFFF");
					settings.setProperty("font_text",				"Calibri");
					
					settings.setProperty("path_menu_music",			"");
					settings.setProperty("path_sample_click",		"resources/click.mp3");
					settings.setProperty("path_sample_voice",		"");
					
					settings.setProperty("enable_fullscreen",		"0");
					settings.setProperty("enable_voices",			"0");
					settings.setProperty("enable_extras",			"0");
	
					settings.setProperty("text_button_new_game", 	"New Game");
					settings.setProperty("text_button_load_game", 	"Load Game");
					settings.setProperty("text_button_save_game", 	"Save Game");
					settings.setProperty("text_button_options", 	"Options");
					settings.setProperty("text_button_extras", 		"Extras");
					settings.setProperty("text_button_exit", 		"Exit");
					settings.setProperty("text_button_back", 		"Back");
					settings.setProperty("text_button_cancel", 		"Cancel");
					settings.setProperty("text_button_exit_game", 	"Exit the game");
					settings.setProperty("text_button_exit_menu", 	"Exit to main menu");
					settings.setProperty("text_button_go_back", 	"Go back to the game");
					
					settings.setProperty("text_control_music", 		"Music volume:");
					settings.setProperty("text_control_sound", 		"Sound volume:");
					settings.setProperty("text_control_voice", 		"Voice volume:");
					settings.setProperty("text_control_opacity", 	"Menu opacity:");
					settings.setProperty("text_control_fullscreen", "Fullscreen:");
					
					settings.setProperty("text_prompt_exit", 		"Do you really want to exit the game?");
					settings.setProperty("text_prompt_save", 		"Save file already exists.");
					settings.setProperty("text_prompt_load", 		"Do you really want to load saved game?");
					settings.setProperty("text_prompt_lose", 		"Current progress will be lost.");
					settings.setProperty("text_prompt_overwrite",	"Do you want to overwrite it?");
					settings.setProperty("text_prompt_empty", 		"Warning: you must enter at least one character!");
					settings.setProperty("text_prompt_answer", 		"> Answer:");					
					settings.setProperty("text_prompt_replay", 	"REPLAY");
					
					// save properties to project root folder
					settings.store(out, "Game properties");
					applySettings();
				} 
				catch (IOException e)
				{	throw new GameEngineException(e.getMessage(), ResourcesManager.class.getName()); }
				finally
				{
					if (out != null)
					{
						try
						{	out.close(); }
						catch (IOException e)
						{	throw new GameEngineException(e.getMessage(), ResourcesManager.class.getName()); }
					}
				}
			}
		}
		catch (GameEngineException e)
		{	System.err.println(e.getDescription()); }
	}
	
	public static void storeConfig()
	{
		File 				config 		= new File(Constants.PATH_CONFIG);
		OutputStream 		out			= null;	
		OutputStreamWriter	osw			= null;
		
		try
		{
			if (config.exists())
			{
				try
				{
					out = new FileOutputStream(config);
					osw = new OutputStreamWriter(out, "windows-1250");
					int fscr = GraphicsManager.getFullscreen() ? 1 : 0;
					
					settings.setProperty("volume_music",	 Integer.toString(SoundManager.getSoundVolume(Constants.TYPE_MUSIC)));
					settings.setProperty("volume_sound",	 Integer.toString(SoundManager.getSoundVolume(Constants.TYPE_SOUND)));
					settings.setProperty("volume_voice",	 Integer.toString(SoundManager.getSoundVolume(Constants.TYPE_VOICE)));				
					settings.setProperty("color_opacity",	 Integer.toString(GraphicsManager.getOpacity()));			
					settings.setProperty("enable_fullscreen",Integer.toString(fscr));
	
					settings.store(osw, "Game properities");
				} 
				catch (IOException e)
				{	throw new GameEngineException(e.getMessage(), ResourcesManager.class.getName()); }
				finally
				{
					if (out != null)
					{
						try
						{	out.close(); }
						catch (IOException e)
						{	throw new GameEngineException(e.getMessage(), ResourcesManager.class.getName()); }
					}
				}
			}
		}
		catch (GameEngineException e)
		{	System.err.println(e.getDescription()); }
	}
	
	public static void applySettings()
	{
		Boolean b;
		Integer i;
		Color 	c;

		// apply enabled/disabled settings
		if ((b = verifyBoolean(settings.getProperty("enable_fullscreen"), "fullscreen mode")) != null)
		{	GraphicsManager.toggleFullscreen(b);}
		else
		{	GraphicsManager.toggleFullscreen(false); }
		
		if ((b = verifyBoolean(settings.getProperty("enable_voices"), "voices enabled")) != null)
		{	voicesEnabled = b; }
		else
		{	voicesEnabled = false; }
		
		if ((b = verifyBoolean(settings.getProperty("enable_extras"), "extras enabled")) != null)
		{	extrasEnabled = b; ;}
		else
		{	extrasEnabled = false; }
		
		// apply window settings
		GraphicsManager.setTitle			(settings.getProperty("window_title"));
		GraphicsManager.setWindowIcon		(settings.getProperty("path_icon"));
		GraphicsManager.setMenuBackground	(new Background("bckg_main", settings.getProperty("path_menu_background")));
		
		SoundManager.setMenuMusic			(settings.getProperty("path_menu_music"));
		SoundManager.setClickSound			(settings.getProperty("path_sample_click"));
		SoundManager.setVoiceSample			(settings.getProperty("path_sample_voice"));
		
		// apply localization settings
		textButtonNewGame		= settings.getProperty("text_button_new_game");
		textButtonLoadGame		= settings.getProperty("text_button_load_game");
		textButtonSaveGame		= settings.getProperty("text_button_save_game");
		textButtonOptions		= settings.getProperty("text_button_options");
		textButtonExtras		= settings.getProperty("text_button_extras");
		textButtonExit			= settings.getProperty("text_button_exit");
		textButtonBack			= settings.getProperty("text_button_back");
		textButtonCancel		= settings.getProperty("text_button_cancel");
		textButtonExitMenu		= settings.getProperty("text_button_exit_menu");
		textButtonExitGame		= settings.getProperty("text_button_exit_game");
		textButtonGoBack		= settings.getProperty("text_button_go_back");
		
		textControlMusic		= settings.getProperty("text_control_music");
		textControlSound		= settings.getProperty("text_control_sound");
		textControlVoice		= settings.getProperty("text_control_voice");
		textControlOpacity		= settings.getProperty("text_control_opacity");
		textControlFullscreen	= settings.getProperty("text_control_fullscreen");
		
		textPromptExit			= settings.getProperty("text_prompt_exit");
		textPromptSave			= settings.getProperty("text_prompt_save");
		textPromptLoad			= settings.getProperty("text_prompt_load");
		textPromptLose			= settings.getProperty("text_prompt_lose");
		textPromptOverwrite		= settings.getProperty("text_prompt_overwrite");
		textPromptEmpty			= settings.getProperty("text_prompt_empty");
		textPromptAnswer		= settings.getProperty("text_prompt_answer");
		textPromptReplay		= settings.getProperty("text_prompt_replay");
				
		// apply sound settings
		if ((i = verifyInteger(settings.getProperty("volume_music"), "music volume")) != null)
		{	
			SoundManager.setSoundVolume(Constants.TYPE_MUSIC, i);
			GraphicsManager.adjustSlider(Constants.TYPE_MUSIC, i);
		}
		else
		{	SoundManager.setSoundVolume(Constants.TYPE_MUSIC, 50); }
		
		if ((i = verifyInteger(settings.getProperty("volume_sound"), "sound volume")) != null)
		{	
			SoundManager.setSoundVolume(Constants.TYPE_SOUND, i);
			GraphicsManager.adjustSlider(Constants.TYPE_SOUND, i);
		}
		else
		{	SoundManager.setSoundVolume(Constants.TYPE_SOUND, 50); }
		
		if ((i = verifyInteger(settings.getProperty("volume_voice"), "voice volume")) != null)
		{	
			SoundManager.setSoundVolume(Constants.TYPE_VOICE, i);
			GraphicsManager.adjustSlider(Constants.TYPE_VOICE, i);	
		}
		else
		{	SoundManager.setSoundVolume(Constants.TYPE_VOICE, 50); }

		// apply font
		String 	 fontName 	= settings.getProperty("font_text");
		if ( verifyFont(fontName))
		{	GraphicsManager.setTextFont				(new Font(fontName, Font.PLAIN, 20)); }
		else
		{	GraphicsManager.setTextFont				(new Font("Calibri", Font.PLAIN, 20)); }
		
		// apply GUI colors		
		if ((c = verifyColor(settings.getProperty("color_menu"))) != null)
		{	GraphicsManager.setMenuColor(c); }
		else
		{	GraphicsManager.setMenuColor(new Color(82,82,82,150)); }
		
		if ((c = verifyColor(settings.getProperty("color_popup"))) != null)
		{	GraphicsManager.setPopupColor(c); }
		else
		{	GraphicsManager.setPopupColor(new Color(50,50,50,100)); }
		
		if ((c = verifyColor(settings.getProperty("color_background"))) != null)
		{	GraphicsManager.setDefaultBckgColor(c); }
		else
		{	GraphicsManager.setDefaultBckgColor(Color.BLACK); }		
		
		if ((c = verifyColor(settings.getProperty("color_text_inactive"))) != null)
		{	GraphicsManager.setInactiveTextColor(c); }
		else
		{	GraphicsManager.setInactiveTextColor(Color.LIGHT_GRAY); }
		
		if ((c = verifyColor(settings.getProperty("color_text"))) != null)
		{	GraphicsManager.setTextColor(c); }
		else
		{	GraphicsManager.setTextColor(Color.WHITE); }
		
		// apply opacity settings
		if ((i = verifyInteger(settings.getProperty("color_opacity"), "opacity")) != null)
		{	
			GraphicsManager.setMenuOpacity(i);
			GraphicsManager.adjustSlider(Constants.OPACITY, i);
		}
		else
		{	GraphicsManager.setMenuOpacity(50); }
		
		GraphicsManager.updateWindow();
	}

	// verification
	public static Boolean verifyBoolean(String b, String desc)
	{
		try
		{	
			int i = Integer.parseInt(b);
			
			if 		(i == 1)
			{ return true; }
			else if (i == 0)
			{ return false; }
		}
		catch (NumberFormatException e)	{}
			
		System.err.println("Incorrect " + desc + " flag: " + b + ". Property value set to default.");
		
		return null;	
	}
	
	public static Integer verifyInteger(String i, String desc)
	{
		Integer in;
		
		try
		{	in = Integer.parseInt(i); }
		catch (NumberFormatException e)
		{
			System.err.println("Incorrect " + desc + " value: " + i + ". Property value set to default.");
			in = null;
		}
		return in;	
	}
	
	public static Color verifyColor(String c)
	{
		Color col;
		
		try
		{	col = Color.decode(c); }
		catch (NumberFormatException e)
		{
			System.err.println("Incorrect color hex value: " + c + ". Color set to default.");
			col = null;
		}
		
		return col;		
	}
	
	public static boolean verifyFont(String fname)
	{
		String[] fonts	= GraphicsEnvironment.getLocalGraphicsEnvironment().getAvailableFontFamilyNames();
		for (String s : fonts)
		{
			if (fname.equalsIgnoreCase(s))
			{	return true; }
		}
		System.err.println("Unknown font family. Font set to default.");
		return false;	
	}
}
