package engine.logics;

import java.io.ByteArrayInputStream;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import javax.imageio.ImageIO;
import javax.imageio.stream.ImageInputStream;
import javax.swing.ImageIcon;

import datatypes.Memory;
import engine.Constants;
import engine.LogicManager;
import engine.exceptions.GameEngineException;
import engine.graphics.panels.PanelMenuSaveLoad;
import engine.graphics.panels.PanelNarration;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;

public class SaveManager
{
	private static boolean[] 			fileExists = new boolean[Constants.MAX_SLOTS];
	
	private static PanelNarration 		narration;
	private static PanelMenuSaveLoad	saveLoad;
	
	public static void init(PanelNarration n, PanelMenuSaveLoad s) throws GameEngineException
	{		
		narration = n;
		saveLoad = s;
		
		File saveDir = new File(".\\saves");
	    if (!saveDir.exists())
	    {
	    	if (saveDir.mkdir())
	    	{ 
	    		for (int i = 0; i< Constants.MAX_SLOTS; i++)
		    	{	fileExists[i] = false; }
	    	}
	    	else
	    	{	throw new GameEngineException("Save directory failed to create", SaveManager.class.getName()); }
	    }
	    else
	    { 
	    	for (int i = 0; i< Constants.MAX_SLOTS; i++)
	    	{ 
	    		if (fileExists[i] = (new File("./saves/slot" + i)).exists())
	    		{	saveLoad.setSlotThumbnail(i, loadThumbnail(i)); 	}
	    	}
	    }
	}
	
	public static boolean exists(int slotNo)
	{	return fileExists[slotNo];	}
	
	public static boolean saveGame(int slotNo)
	{
		Memory memory = LogicManager.getMemory();
		
		int w = narration.getWidth();
		int h = narration.getHeight(); 
    	
        BufferedImage screen = new BufferedImage( w, h, BufferedImage.TYPE_INT_ARGB);
        BufferedImage thumb  = new BufferedImage((int)(0.3 * w), (int)(0.3 * h), BufferedImage.TYPE_INT_ARGB);
        
        Graphics2D screenshot = screen.createGraphics();
        narration.printAll(screenshot);
        screenshot.dispose();
        
        Image i = screen.getScaledInstance((int)(0.3 * w), (int)(0.3 * h), 0);
        
        Graphics2D thumbnail = thumb.createGraphics();
        thumbnail.drawImage(i, 0, 0, null);
        thumbnail.dispose();
        
	    try
	    {   
	    	FileOutputStream memOut = new FileOutputStream("./saves/slot" + slotNo);
	        ObjectOutputStream out = new ObjectOutputStream(memOut);
	        out.writeObject(memory);
	        memOut.write(new String("\n%THUMBNAIL%\n").getBytes("UTF-8"));
	        ImageIO.write(thumb, "png", memOut);
	        out.close();
	        memOut.close();
	         
	        System.out.printf("Serialized memory data saved in ./saves/slot" + slotNo);
	    }
	    catch(IOException ex)
	    { 
	    	ex.printStackTrace();
	    	return false;
	    }
	    
	    saveLoad.setSlotThumbnail(slotNo, new ImageIcon(thumb));
	    
	    fileExists[slotNo] = true;
	    return true;
	}
	
	public static Memory loadGame(int slotNo)
	{
		if (fileExists[slotNo])
		{
			Memory m = new Memory(null);
		    try
		    {
		         FileInputStream memoryIn = new FileInputStream("./saves/slot" + slotNo);
		         ObjectInputStream in = new ObjectInputStream(memoryIn);
		         m = (Memory)in.readObject();
		         in.close();
		         memoryIn.close();
		    }
		    catch(Exception ex)
		    {	
		    	ex.printStackTrace();
		    	System.err.println("Could not read save data from slot" + slotNo + ".");
		    	return null;	      
		    }
	
		    System.out.println(m.printableMemoryState());
		    System.out.println("Last loaded line no: " + m.getStoryLoadLine());
		    
		    return m;
		}
		else
		{ 
			System.err.println("Could not read save data from slot"+ slotNo + ".\nSlot is empty.");
			return null;
		}
	}

	private static ImageIcon loadThumbnail(int slotNo)
	{
		BufferedImage 	thumbnail 	= null;
		String 			line   		= "";
		byte[]			imgData;
		
		try
		{
			FileInputStream  	stream	= new FileInputStream("./saves/slot" + slotNo);			
			ImageInputStream	input  	= ImageIO.createImageInputStream(stream);
			
			while (!(line = input.readLine()).contains("%THUMBNAIL%") && line != null);		// skip serialized memory data 
			
			imgData = new byte[stream.available()];
			stream.read(imgData);															// read thumbnail image data
			
			thumbnail = ImageIO.read(new ByteArrayInputStream(imgData));
			input.close();
			stream.close();
		}
		catch (IOException e)
		{ 
			System.err.println("Thumbnail for slot" + slotNo + "couldn't be loaded");
			return null;
		}
		
		return new ImageIcon(thumbnail);
	}

}
