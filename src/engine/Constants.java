package engine;

import java.awt.Color;

import engine.logics.elements.Actor;

public class Constants
{
	// Special characters
	public final static char 	 EOT 			=  0x03; 				// EndOfText guardian
	
	// Paths to constant resources
	public static final String	 PATH_CONFIG    = "./config.ini";					// path to config file
	public static final String	 PATH_SPEAKER_L = "resources/speaker_loud.png";		// path to the loud speaker icon
	public static final String	 PATH_SPEAKER_S = "resources/speaker_silent.png";	// path to the loud speaker icon
	public static final String	 PATH_MARKER	= "resources/selected.png";			// path to the selected mark	
	// Identificators of GUI panels
	public static final int 	 P_MAIN_MENU	= 0;								// game menu when game is inactive
	public static final int 	 P_INGAME_MENU	= 1;								// game menu when game is active
	public static final int 	 P_NARRATION	= 2;								// main gameplay panel showing narration 
	public static final int 	 P_SAVES_MENU	= 3;								// menu used to save or loade game
	public static final int 	 P_OPTIONS_MENU = 4;								// menu used to change program options
	public static final int 	 P_EXTRAS_MENU 	= 5;								// menu for any extra content (not used)
	// Identificators of GUI dialogs
	public static final int		 DG_NONE		= -1;								// no actual dialog
	public static final int 	 DG_EXIT_MM		= 0;								// dialog for exiting from main menu
	public static final int 	 DG_EXIT_IM		= 1;								// dialog for exiting from ingame menu 
	public static final int 	 DG_OPTIONS	 	= 2;								// dialog for choosing an option in decision points
	public static final int 	 DG_INPUT		= 3;								// dialog for inputing personalized data
	public static final int		 DG_CONFIRM		= 4;								// dialog for confirming or declining save/load decisions
	
	// Saving menu contexts
	public static final boolean  CONTEXT_SAVE 	= true;								// save context for save/load purpose
	public static final boolean  CONTEXT_LOAD 	= false;							// load context for save/load purpose
	// Non-standard colors
	public static final Color	 TRANSPARENT    = new Color(0, 0, 0, 0);			// transparent color
	public static final Color	 REPLAY_SHADE	= new Color(0, 0, 0, 80);			// screen shade when replay color
	public static final Color	 REPLAY_TEXT	= new Color(255,255,255,100);		// replay warning text color
	// Opacity control
	public static final int 	 OPACITY 	    = 3;								// index of opacity adjusting slider
	public static final int 	 OP_MIN 		= 0;								// minimal window opacity
	public static final int 	 OP_MAX 		= 255;								// maximal window opacity
	// Sprites control
	public static final double[] POS_SPRITES    = {0.025, 0.275, 0.525};			// array of sprites positions
	public static final int 	 NUM_SPRITES	= 3;								// number of sprites used	
	public static final int 	 POS_UNDEFINED	= -1; 								// undefined sprite position
	public static final int 	 POS_LEFT		= 0;								// sprite on left side of the screen
	public static final int 	 POS_CENTER		= 1;								// sprite at center of the screen
	public static final int 	 POS_RIGHT		= 2;								// sprite on right side of the screen
	// Sound control
	public static final int 	 NUM_SLIDERS 	= 3;								// number of sound adjusting sliders
	public static final int 	 TYPE_MUSIC     = 0;								// index of music slider/music type of sound
	public static final int 	 TYPE_SOUND 	= 1;								// index of sound slider/sound type of sound
	public static final int 	 TYPE_VOICE     = 2;								// index of voice slider/voice type of sound
	public static final int 	 VOL_MIN 	    = 0;								// minimal sound volume		
	public static final int 	 VOL_MAX 	    = 100;								// maximal sound volume
	// Gameplay control
	public static final int  	 NUM_OPTIONS 	= 9;								// maximal number of answers for question
	public static final int		 MAX_SLOTS		= 9;								// maximal number of save slots
	// Story size
	public static final int		 NUM_VERSES		= 100;								// maximal number of verses in the story
	// Default actors
	public static final Actor 	 ACTOR_ERROR 	= new Actor ("error", "== Error == ", "#FF0000FF");	// actor used for error messages
	public static final Actor 	 ACTOR_DEFAULT 	= new Actor ("default", "Narrator", "#FFFFFFFF");	// actor used for no-actor verses
}
