package main;

import engine.LogicManager;
import interpreter.Interpreter;
import javafx.application.Application;
import javafx.stage.Stage;

public class Main extends Application
{
	private static Interpreter interpreter = new Interpreter("./Story");
	
    public static void main(String args[])
    { launch(args); }

	@Override
	public void start(Stage arg0) throws Exception
	{	
		// Run the program
		java.awt.EventQueue.invokeLater
        ( 	
        	new Runnable()
        	{
	            @Override
	            public void run()
	            {	LogicManager.start(interpreter);	}
        	}
        );	
	}
}
