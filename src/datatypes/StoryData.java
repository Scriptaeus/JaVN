package datatypes;

import java.io.Serializable;

public class StoryData implements Serializable
{
	private static final long serialVersionUID = -6331254394775284306L;
	
	private String answer;					
	private String instruction;			

	public StoryData(String a, String i)
	{
		this.answer = a;
		this.instruction = i;
	}
	
	public String getAnswer()
	{	return answer; }
	
	public String getInstruction()
	{	return instruction; }	
}
