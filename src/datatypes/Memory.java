package datatypes;

import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import engine.graphics.elements.Background;
import engine.graphics.elements.Sprite;
import engine.logics.elements.Verse;
import engine.sounds.elements.Music;
import engine.sounds.elements.Sound;
import engine.sounds.elements.Voice;
import interpreter.Interpreter;
import interpreter.exceptions.InterpreterException;

public class Memory implements Serializable
{
	private static final long serialVersionUID = 7411919514146657797L;

	private HashMap<String,Data> 		variables;
	private HashMap<String, Integer> 	labels;
	private Story 						story;
	
	private Background					activeBackground;;
	private Sprite[]					activeSprites;
	
	private Music						activeMusic;
	private Sound						activeSound;
	private Voice						activeVoice;
	
	public Memory(Interpreter i)
	{	
		variables = new HashMap<String,Data>();
		labels = new HashMap<String, Integer>();
		story = new Story();
		
		activeSprites = new Sprite[3];		
	}
	
	// Setters
	
	public void setActiveBackground(Background bckg)
	{ 	activeBackground = bckg; }
	
	public void setActiveSprite(int pos, Sprite sprt)
	{ 	activeSprites[pos] = sprt; }
	
	public void setActiveMusic(Music m)
	{ 	activeMusic = m; }
	
	public void setActiveSound(Sound s)
	{ 	activeSound = s; }
	
	public void setActiveVoice(Voice v)
	{ 	activeVoice = v; }
	
	// Getters
	
	public Background getActiveBackground()
	{ return activeBackground; }
	
	public Sprite getActiveSprite(int pos)
	{ return activeSprites[pos]; }
	
	public Sprite[] getActiveSprites()
	{	return activeSprites; }
	
	public Music getActiveMusic()
	{ 	return activeMusic; }
	
	public Sound getActiveSound()
	{ 	return activeSound; }
	
	public Voice getActiveVoice()
	{ 	return activeVoice; }
	
	// === MEMORY STATE METHODS ===
	
	public void write(String name, Data value)
	{	variables.put(name, value);	}

	public Data read(String name)
	{	return variables.get(name); }
	
	public HashMap<String,Data> dump()
	{	return variables; }
	
	public boolean isDefined(String name)
	{	return variables.containsKey(name); }
	
	public boolean isDefinedComponent(String name)
	{
		if (variables.containsKey(name))
		{	return (variables.get(name).getType() == 'M'); }
		
		return false;
	}
	
	public String memoryState()
	{
		
		String mems = "{ ";
		for(Map.Entry<String, Data> e : variables.entrySet())
		{
			String key = e.getKey();
			Data value = e.getValue();
			  
			switch (value.getType())
			{
				case 'I':
			  		 mems += key + " = " + value.getInteger() + " ";
			  		 break;
			  	case 'F':
			  		mems += key + " = " + value.getFloat() + " ";
			  		break;
			  	case 'T':
			  		mems += key + " = \"" + value.getText() + "\" ";
			  		break;
			  	case 'B':
			  		mems += key + " = " + value.getBoolean() + " ";
			  		break;
			  	default:
			  		mems += value.getElement().defineSelf() + " ";
			 }
		}		
		mems += "}";
		return mems;
	}
	
	public String printableMemoryState()
	{
		String mems = "{\n";
		for(Map.Entry<String, Data> e : variables.entrySet())
		{
			String key = e.getKey();
			Data value = e.getValue();
			  
			switch (value.getType())
			{
				case 'I':
					mems += "\t" + key + " = " + value.getInteger() + "\n";
			  		break;
			  	case 'F':
			  		mems += "\t" + key + " = " + value.getFloat() + "\n";
			  		break;
			  	case 'T':
			  		mems += "\t" + key + " = \"" + value.getText() + "\"\n";
			  		break;
			  	case 'B':
			  		mems += "\t" + key + " = " + value.getBoolean() + "\n";
			  		break;
			  	default:
			  		mems += "\t" + key + " = " + value.getText() + "(" + value.getElement().defineType() + ") " + "\n";
			 }
		}		
		mems += "}";
		return mems;
	}
	
	public String printableSceneState()
	{
		String mems = "Active scene objects:\n+=+=+=+=+=+=+=\n";
		
		if ( activeBackground != null)
		{	mems += "active background: " + activeBackground.defineSelf() + "\n"; }
		else
		{	mems += "active background: none\n"; }
		if ( activeSprites[0] != null)
		{	mems += "active sprite left: " + activeSprites[0].defineSelf() + "\n"; }
		else
		{	mems += "active sprite left: none\n"; }
		if ( activeSprites[1] != null)
		{	mems += "active sprite center: " + activeSprites[1].defineSelf() + "\n";}
		else
		{	mems += "active sprite center: none\n"; }
		if ( activeSprites[2] != null)
		{	mems += "active sprite right: " + activeSprites[2].defineSelf() + "\n"; }
		else
		{	mems += "active sprite right: none\n"; }
		
		mems += "Active sounds:\n+=+=+=+=+=+=+=\n";
		
		if ( activeMusic != null)
		{	mems += "active music: " + activeMusic.defineSelf() + "\n"; }
		else
		{	mems += "active music: none\n"; }
		if ( activeSound != null)
		{	mems += "active sound: " + activeSound.defineSelf() + "\n"; }
		else
		{	mems += "active sound: none\n"; }
		if ( activeVoice != null)
		{	mems += "active voice: " + activeVoice.defineSelf() + "\n"; }
		else
		{	mems += "active voice: none\n"; }
		
		return mems;
	}
	
	// === STORY STATE METHODS ===
		
	public Story getStory()
	{	return story; };
	
	public int getStorySize()
    {	return story.getSize(); }
	
	public int getStoryLoadLine()
	{	return story.getloadLine(); }
	
	public Verse storyFirstVerse()
	{	return story.getFirst(); }
	
	public Verse storyActualVerse()
	{	return story.getActual(); }
	
	public Verse storyLastVerse()
	{	return story.getLast(); }
	
	public Verse storyNextVerse()
	{	return story.getNext(); }
	
	public Verse storyPreviousVerse()
	{	return story.getPrevious(); }
	
	public Verse storyForward()
	{	return story.next(); }
	
	public Verse storyBackward()
	{	return story.previous(); }
	
	public Verse storyToEnd()
	{	return story.last(); }
	
	public void cutStoryBranch()
	{ 	story.cutBranch(); }   
    
	public String printableStoryState()
	{
		String st = "Number of verses: " + getStorySize() + "\n";
		String h = "";
		Verse v = story.getFirst();
		
		for(int i = 0; i < getStorySize(); i++)
		{
			h = v.getStatement();
			if (h.length() > 30)
			{	h = h.substring(0,30);}
			
			st += v.getSpeakerName() + ": " + h + "\n";
			
			v = v.getNext();
		}	
		return st;
	}
	
	// === LABELS LIST METHODS ===
	
	public void addLabel(String name, int line) throws InterpreterException
	{
		if (labels.containsKey(name) == false)
		{	labels.put(name, line); }
		else
		{	throw new InterpreterException("Label already used");	}
	}
	
	public int getLabel(String name) throws InterpreterException
	{
		if (labels.containsKey(name))
		{	return labels.get(name); }
		throw new InterpreterException("Unknown label");
	}
	
	// === SERIALIZATION ===
	
    private void writeObject(java.io.ObjectOutputStream stream) throws IOException 
    { 
    	stream.writeObject(variables);
    	stream.writeObject(labels);
    	stream.writeObject(story);
    	
    	stream.writeObject(activeBackground);
    	for (int i = 0; i < 3; i++)
    	{	stream.writeObject(activeSprites[i]); }
    	
    	stream.writeObject(activeMusic);
    	stream.writeObject(activeSound);
    	stream.writeObject(activeVoice);
    	
    }
    
    
    @SuppressWarnings("unchecked")
	private void readObject(java.io.ObjectInputStream stream) throws IOException, ClassNotFoundException
    {
    	variables 	= (HashMap<String, Data>)stream.readObject();
    	labels		= (HashMap<String, Integer>)stream.readObject();
    	story		= (Story)stream.readObject();
    	
    	activeBackground = (Background)stream.readObject();
    	activeSprites = new Sprite[3];
    	for (int i = 0; i < 3; i++)
    	{	activeSprites[i] = (Sprite)stream.readObject(); }
    	
    	activeMusic = (Music)stream.readObject();
    	activeSound = (Sound)stream.readObject();
    	activeVoice = (Voice)stream.readObject();
    	

    }
}
