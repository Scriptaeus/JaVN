
package datatypes;

import java.io.IOException;
import java.io.Serializable;

import engine.logics.elements.Verse;

public class Story implements Serializable
{
	private static final long serialVersionUID = -7010334277194536854L;
	
	private transient int	loadLine;		// verse from which analyse should proceed after load
	private transient int 	size;			// total number of Verses in current Story	
    private transient Verse first;			// reference to first Verse
    private transient Verse actual;			// reference to actually selected Verse
    private transient Verse last;			// reference to last Verse
    
    public Story()
    {
    	loadLine = 1;
    	size = 0;
    	first = null;
        actual = null;
        last = null;
    }
   
    /**
     * Adds new Verse at end of current Story.
     * @param a - Verse to add*/
    public void add(Verse a)
    {
    	size++;
    	
        if (first == null)					// story has no verses
        {  first = actual = last = a;  }
        else								// story has at least one verse 
        {
        	last.setNext(a);		// add reference to the new verse to previously last one
        	a.setPrevious(last);	// add reference to previously last verse to the new one
       		a.setNext(null);		// new verse has no next verse
       		last = a;				// new verse is now last       		
        }
    }
    
    /**
     * Removes Verse from end of current Story.*/
    public void remove()
    {
        if(size > 0) 						// story has at least one verse:
        {        	
        	if (size == 1)							// has exactly one verse
        	{ first = actual = last = null; }
        	else									// has more than one verse
        	{
        		if (actual == last)					// if actual verse was the last one
        		{ 
        			last   = last.getPrevious();
        			last.setNext(null);
        			actual = last;    
        		}
        		else
        		{
        			last   = last.getPrevious();
        			last.setNext(null);
        		}
        	}
        	size--;
        }
        else
        { System.err.println("It's not possible to remove verse from empty story!"); }
    }
    
    /**
     * Removes all Verses past actual from current Story.
     * Used for cleaning Story after rewinding and selecting another route.
     * NOTE: Can be used only if actual verse precedes the last one! */    
    public void cutBranch()
    {
    	Verse v = actual;
    	
        if(v != null)							// actual has at least one predecessor
        {
            while (last != v)
            {	remove(); }            
        }
    }    
    
    /**
     *  Removes first Verse from current Story.  
     *  Used mainly for keeping only specified number of Verses in computer memory. */    
    public void rebase()
    { 
		first = first.getNext();
		first.setPrevious(null);
		size--;
    }
    
    // --- getters ---
    
    /**
     * Returns actual element from Story
     * @return - actual Verse */    
    public Verse getActual()
    { return actual; }
    
    /**
     * Returns first element from Story
     * @return - first Verse */
    public Verse getFirst()
    { return first; }  
    
    /**
     * Returns last element from Story
     * @return - last Verse */    
    public Verse getLast()
    { return last; }
    
    /**
     * Returns next to actual verse from Story
     * @return - next Verse */
    public Verse getNext()
    {
    	if (actual != null)
    	{	return actual.getNext(); }
    	else
    	{	return null; }
    }  
    
    /**
     * Returns previous to actual verse from Story
     * @return - previous Verse */    
    public Verse getPrevious()
    {
	 	if (actual != null)
		{	return actual.getPrevious(); }
		else
		{	return null; }
 	}
    
	/**
	 * Returns actual count of verses added to current story
	 * @return - number of verses in current Story */    
    public int getSize()
    { return size; }
    
	/**
	 * Returns number of line from which analyse will start after game load
	 * @return - number of first line to analyse */    
    public int getloadLine()
    { return loadLine; }
    
    // --- movement ---
        
    /**
     * Sets next Verse (if exists) as actual
     * @return - actual verse after update */    
    public Verse next()
    {
    	if (actual != null)
    	{
    		if (actual.getNext() != null)
    		{
    			actual = actual.getNext();
    			return actual;
    		}
    	}
    	return null;
    }
    
	/**
	 * Sets previous Verse (if exists) as actual
	 * @return - actual verse after update */    
    public Verse previous()
    {
    	if (actual != null)
    	{
    		if (actual.getPrevious() != null)
    		{
    			actual = actual.getPrevious();
    			return actual;
    		}
    	}
    	return null;
    }
    
    /**
     * Sets the last Verse (if exists) as actual
     * @return - actual verse after update */    
    public Verse last()
    {
    	if (last != null)
    	{
    		actual = last;
    		return actual;
    	}
    	return null;
    }

    private void writeObject(java.io.ObjectOutputStream stream) throws IOException 
    { 
    	if (last.getQ())
    	{	System.out.println("Surprise!");loadLine = last.getPrevious().getLineNr(); }
    	else
    	{ 	loadLine = last.getLineNr(); }
    	
    	stream.writeInt(loadLine);
    }
    
    
    private void readObject(java.io.ObjectInputStream stream) throws IOException, ClassNotFoundException
    {
    	loadLine = stream.readInt();
    	
    	size   = 0; // only one verse was saved
    	first  = null;
    	last   = null;
    	actual = null;
    }
}
