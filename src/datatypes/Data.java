package datatypes;

import java.io.Serializable;

// import java.io.IOException;
// import java.util.HashMap;

public class Data implements Serializable
{
	private static final long serialVersionUID = 5112822993362073558L;

	private char type = 'I';
	
	private int intValue = 0;
	private double floatValue = 0;
	private boolean boolValue = false;
	private String textValue = "";
	private Memorizable defValue = null;
	
	// ---- CONSTRUCTORS ----
	
	public Data(int i)
	{
		type = 'I';
		this.intValue = i;
	}
	
	public Data(double f)
	{
		type = 'F';
		this.floatValue = f;
	}
	
	public Data(boolean b)
	{
		type = 'B';
		this.boolValue = b;
	}
	
	public Data(String s)
	{
		type = 'T';
		this.textValue = s;
	}
	
	public Data(Memorizable m)
	{
		type = 'M';
		this.defValue = m;
	}
	
	// ---- GETTERS ----
	
	// 1. Type
	public char getType()
	{
		return type;
	}
		
	// 2. Values
	public String getText()
	{
		if (type == 'I')
		{
			return Integer.toString(intValue);
		}
		else if (type == 'F')
		{
			return Double.toString(floatValue);
		}
		else if (type =='B')
		{
			return Boolean.toString(boolValue);
		}
		else if (type == 'T')
		{
			return textValue;
		}
		else if (type == 'M')
		{
			return (defValue).getNameString();
		}
		else
		{
			return "";
		}
	}
	
	public double getFloat()
	{
		if (type == 'I')
		{
			return (double)intValue;
		}
		else if (type == 'F')
		{
			return floatValue;
		}
		else if (type == 'B')
		{
			return boolValue?1.0:0.0; // 1 if true, 0 when false
		}
		else if (type == 'T')
		{
			try
			{
				return Double.parseDouble(textValue);
			}
			catch (NumberFormatException e)
			{
				return 0.0;
			}
		}
		else
		{
			return 0.00;
		}
	}
	
	public int getInteger()
	{
		if (type == 'I')
		{
			return intValue;
		}
		else if (type == 'F')
		{
			return (int)floatValue;
		}
		else if (type == 'B')
		{
			return boolValue?1:0; // 1 if true, 0 when false
		}
		else if (type == 'T')
		{
			try
			{
				return Integer.parseInt(textValue);
			}
			catch (NumberFormatException e)
			{
				return 0;
			}
		}
		else
		{
			return 0;
		}
	}
	
	public boolean getBoolean()
	{
		if (type == 'I')
		{
			return intValue!=0;
		}
		else if (type == 'F')
		{
			return floatValue!=0.0;
		}
		else if (type == 'B')
		{
			return boolValue;
		}
		else if (type == 'T')
		{
			return false;
		}
		else
		{
			return false;
		}
	}
	
	public Memorizable getElement()
	{
		if (type == 'M')
		{
			return this.defValue;
		}
		else
		{
			return null;
		}
	}
	
	@Override
	public String toString()
	{
		if (this.type == 'T')
		{ return "\"" + this.getText() + "\""; }
		else
		{ return this.getText(); }
	}
	
	/*
	// ---- test case ----
		
	static HashMap<String, Data> Memory = new HashMap<String,Data>();
	
	public static void main(String args[]) throws IOException
	{
		int x = 2;
		
		Memory.put("x", new Data(x));
		Memory.put("y", new Data(10));
		Memory.put("z", new Data(5.67));
		Memory.put("ex", new Data("Strazak Florian mial psa wyzla"));
		Memory.put("b",new Data(true));
		
		double y = Memory.get("z").getFloat();
		int i = Memory.get("ex").getInteger();
		boolean b = Memory.get("ex").getBoolean();
		double j = Memory.get("b").getFloat();
		boolean z = Memory.get("z").getBoolean();
		
		System.out.println(y);
		System.out.println(i);	
		System.out.println(b);
		System.out.println(j);	
		System.out.println(z);	

	}
	*/
}
