package datatypes;

import java.io.Serializable;

import interpreter.exceptions.InterpreterException;

public interface Memorizable extends Serializable
{
	public void define(Memory m) throws InterpreterException;
	public String defineSelf();
	public String defineType();
	public String getNameString();
}
