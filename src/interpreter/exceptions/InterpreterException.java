package interpreter.exceptions;

import interpreter.Analyzer;

public class InterpreterException extends Exception
{
	private static final long serialVersionUID = 3301203397079891216L;
	private String description;
	private String code;
	private int line;

	public InterpreterException(String c)
	{
		this.description = c;
		this.code = Analyzer.getOutput();
		this.line = Analyzer.getActualLine();
	}
	
	public String getDescription()
	{ return description; }
	
	public String getCode()
	{ return code; }
	
	public int getLine()
	{ return line; }
}
