package interpreter;

import java.util.ArrayList;

import datatypes.Data;
import datatypes.Memory;
import datatypes.StoryData;
import engine.Constants;
import engine.graphics.elements.Background;
import engine.graphics.elements.Sprite;
import engine.logics.elements.Actor;
import engine.logics.elements.Verse;
import engine.logics.elements.VerseInput;
import engine.logics.elements.VerseOption;
import engine.sounds.SoundManager;
import engine.sounds.elements.Music;
import engine.sounds.elements.Sound;
import engine.sounds.elements.Voice;
import interpreter.exceptions.InterpreterException;
import interpreter.expressions.BinaryOperator;
import interpreter.expressions.Constant;
import interpreter.expressions.Expression;
import interpreter.expressions.TextConstant;
import interpreter.expressions.Variable;
import interpreter.programs.AddDefinition;
import interpreter.programs.AddVerse;
import interpreter.programs.Assign;
import interpreter.programs.Composition;
import interpreter.programs.Define;
import interpreter.programs.HideSprite;
import interpreter.programs.PlaySound;
import interpreter.programs.Program;
import interpreter.programs.ShowBackground;
import interpreter.programs.ShowSprite;
import interpreter.programs.Skip;

public class Parser
{
	private static Memory 	memory 		= null;		// memory, on which parser operates
	
	private static String 	input 		= "";  		// analyzed text
    private static int 		position 	= 0; 		// pointer at actually processed character    
    private static boolean 	loopActive 	= false;	// true if "while" is allowed
    private static int 		elAllowed 	= 0;		// greater than one when "else" is allowed
    private static boolean 	elActive 	= false;	// true if "else" is active and it's block will be executed   
    private static boolean 	initd 		= false;	// true if parser was initialized
    
    // === INITIALIZATION ===
    /** Initializes the <code>Parser</code> before the first use.
     * @param m - <code>Memory</code> which will be used by this <code>Parser</code> */
    public static void init(Memory m)
    {
    	if (m != null)
    	{ initd = true; }
    	else
    	{ initd = false; }
    	memory = m;
    }
    
    // === METHODS ===
    /** Sets the input code to be parsed.
     * @param in - code to be parsed */
    public static void setInput(String in)
    {
    	input = in + Constants.EOT;
    	position = 0;
    }
    
    // -- PUBLIC METHODS --
    /** Parses the arithmetic expression on numeral or text values.
     * @return - arithmetic expression
     * @throws InterpreterException */
    public static Expression parseExpression() throws InterpreterException 		// Parses expression
    {
    	Expression e = parseSum();
    	if (lookAhead() != Constants.EOT)
    	{	throw new InterpreterException("ParseSum() Error");	}
    	return e;	
    }
    
    /** Parses the command or set of commands which will affect application flow.
     * @return - command or sets of commands
     * @throws InterpreterException */
    public static Program parseProgram() throws InterpreterException			// Parses program
    {   	
    	if (initd)								// if the parser is already initiated
    	{
    		if (input != null)					// if the input is not empty
    		{ 
    			if (elAllowed > 0 )				// --- check for conditional expression			
    			{ 								// reduces area of "else" allowed to one command
    				if (--elAllowed == 0)		// if after reduction else is no longer allowed
    				{	elActive = false; }		// make sure it's inactive too (no else case)
    			}
    			
    			if (loopActive == true)			// --- check for loop 
    			{ 								// loops instructions block after "while"
    				System.out.println("Looping!");
    				Analyzer.jumpLoopLine();	
    				loopActive = false;
    			}
    			
	    		Program p = parseBlock();		// parse the input code
	    		
		    	if (lookAhead() == Constants.EOT)			// if all the code was parsed - as it should be
		    	{	return p; }					// return the parsed program
		    	else
		    	{	throw new InterpreterException("Unexpected end of stream"); }
    		}
    		else
    		{	throw new InterpreterException("Unexpected null input"); }
    	}
    	else
    	{	throw new InterpreterException("Memory not set"); }
    }

    // -- PRIVATE METHODS --
    /** Ignores the next character if it's a white character. */
    private static void skipWhitespaces()
    {
    	while (Character.isWhitespace(input.charAt(position)))
    	{ position++; }
    }
  
    /** Ignores all the following white characters and returns the first meaningful character.
     * @return first non-white character */
    private static char lookAhead()
    {
    	skipWhitespaces();
    	return input.charAt(position);
    }
    
    /** Reads all the arguments passed to the command in the parenthesis. 
     * @return <code>String[]</code> - array of arguments passed to the command
     * @throws InterpreterException if the parenthesis is unclosed or a variable argument is undefined.*/
    private static String[] readArguments() throws InterpreterException
    {
     	ArrayList<String> args = new ArrayList<String>();		// array of arguments
     	
    	String buffer;						// text analysis buffer
    	int argc = 0;						// counter of arguments
    	int unclosedP = 1;					// counter of unclosed parenthesis
    	boolean unclosedQ = false;			// true if there are unclosed quotation marks
    	boolean separator = false;			// true if separator character encountered

    	while(unclosedP != 0)
    	{
    		buffer = "";						// empty argument string    		
   			lookAhead();						// find the beginning of next argument
   			separator = false;					// reset separator flag 						
    		
    		while (input.charAt(position) != Constants.EOT && !separator && unclosedP > 0)
    		{
    			if (input.charAt(position) == '(')
    			{ unclosedP++; }
    			if (input.charAt(position) == ')')
    			{ if(--unclosedP == 0) { separator = true; }}
    			if ( input.charAt(position) == ',' && !unclosedQ && unclosedP == 1)	// if outermost comma encountered...
    			{ separator = true; }												// break the loop
    			if (!separator)														// if none of above (common case)...
    			{ buffer += input.charAt(position); } 								// add character to buffer
    			
    			position++;															// proceed with next character
    		}

    		if (input.charAt(position) == Constants.EOT && unclosedP != 0)										
    		{	throw new InterpreterException("Syntax error - \")\" expected"); }
    		else if (buffer.equals(""))	
    		{	throw new InterpreterException("Syntax error - next argument expected after \",\""); }	
    		
    		buffer = buffer.replace("\"","");
    		    	
    		if(buffer.charAt(0) == '$')			// check if argument should be replaced with value of variable 
    		{
    			buffer = buffer.substring(1);
    			if (memory.isDefined(buffer))
    			{	buffer = memory.read(buffer).getText(); }
    			else
    			{	throw new InterpreterException("Bad argument: variable not defined"); }
    		}
    		
    		argc ++;
    		args.add(buffer);
    	}
    	
    	return args.toArray(new String[argc]);
    }
       
    /** Reads the specified expected number of arguments passed to the command in the parenthesis. 
     * @param n - expected number of arguments
     * @return <code>String[]</code> - array of arguments passed to the command
     * @throws InterpreterException if the parenthesis is unclosed or a variable argument is undefined.*/
    private static String[] readArguments(int n) throws InterpreterException
    {
    	String[] buffer = new String[n];
    	String arg;
    	lookAhead();
    	
    	for(int i=0; i<n; i++)
    	{
    		arg = "";																	// empty argument string
    		while ((!Character.isWhitespace(input.charAt(position)))
    				&& input.charAt(position) != ',' && input.charAt(position) != ')')
    		{
    			arg += input.charAt(position);											// add character to buffer
    			position++;																// proceed with next character
    		}
    		
    		if(input.charAt(position) == ',') 											// if string terminated by argument separator
    		{
    			position++;																// move to next character
    			lookAhead();															// find the beginning of next argument
    		}

    		else if(input.charAt(position) == ')')										// if closing brace found
    		{  
    			if ( i < n-1)															// ...and it's not last expected argument
    			{	throw new InterpreterException("Premature end of arguments list"); }
    		}
    		else																		// if there's unnecessary whitespace after argument
    		{	throw new InterpreterException("Bad argument format"); }
    		    	
    		if(arg.charAt(0) == '$')
    		{
    			arg = arg.substring(1);
    			if (memory.isDefined(arg))
    			{	arg = memory.read(arg).getText(); }
    			else
    			{	throw new InterpreterException("Bad argument: variable not defined"); }
    		}
    		
    		buffer[i] = arg;
    	}

    	if(input.charAt(position) != ')')												// if reading ends before last argument on list
		{	throw new InterpreterException("Delayed end of argument list"); }
    	
    	position++;
    	return buffer;
    }
    
    	
     private static String[] readConditions(int no) throws InterpreterException
    {
    	String[] buffer = new String[no];
    	String arg;
    	lookAhead();
    	
    	for(int i=0; i<no; i++)
    	{
    		arg = "";																	// empty argument string
    		
    		while (!Character.isWhitespace(input.charAt(position)) && input.charAt(position) != ')')
    		{
    			arg += input.charAt(position);											// add character to buffer
    			position++;																// proceed with next character
    		}
    		
    		if (input.charAt(position) == ' ')
    		{
    			position++;																// move to next character
    			lookAhead();															// find the beginning of next argument
    		}
    		else if(input.charAt(position) == ')')										// if closing brace found
    		{  
    			if ( i < no-1)															// ...and it's not last expected argument
    			{	throw new InterpreterException("Premature end of arguments list"); }
    		}
    		else																		// if there's unnecessary whitespace after argument
    		{	throw new InterpreterException("Bad argument format"); }
    		    	
    		if(arg.charAt(0) == '$')
    		{
    			arg = arg.substring(1);
    			if (memory.isDefined(arg))
    			{	arg = memory.read(arg).getText(); }
    			else
    			{	throw new InterpreterException("Bad argument: variable not defined"); }
    		}
    		
    		buffer[i] = arg;
    	}

    	if(input.charAt(position) != ')')												// if reading ends before last argument on list
		{	throw new InterpreterException("Delayed end of argument list"); }
    	
    	position++;
    	return buffer;
    }
    
    
    // -- parseExpression helper methods --
    
    private static Expression parseSum() throws InterpreterException       				// Parses sum
    {
    	Expression e = parseMultiplication();
    	char c = lookAhead();
    	while(c == '+' || c == '-')
    	{
    		position++;
    		e = new BinaryOperator(c, e, parseMultiplication());
    		c = lookAhead();
    	}
    	return e;
    }
    
    private static Expression parseMultiplication() throws InterpreterException       	// Parses multiplication
    {
    	Expression e = parseLogicalExpression();
    	char c = lookAhead();
    	while(c == '*' || c == '/' || c == '%')
    	{
    		position++;
    		e = new BinaryOperator(c, e, parseLogicalExpression());
    		c = lookAhead();
    	}
    	return e;
    }
    
    private static Expression parseLogicalExpression() throws InterpreterException		// Parses logical expression
   	{
    	Expression e = parseLogicalConnector();
    	char c = lookAhead();
    	while( c == '<' || c == '>' || c == '=' )
    	{
    		position++;
    		e = new BinaryOperator(c, e, parseLogicalConnector());
    		c = lookAhead();
    	}
    	return e;
   	}
    
    private static Expression parseLogicalConnector() throws InterpreterException 		// Parses logical expression
   	{
    	Expression e = parseTerm();
    	char c = lookAhead();
    	while( c == '&'|| c == '|')
    	{
    		position++;
    		e = new BinaryOperator(c, e, parseTerm());
    		c = lookAhead();
    	}
    	return e;
   	}
    
    private static Expression parseTerm() throws InterpreterException
    {
    	char c = lookAhead();
    	
    	if (Character.isDigit(c))
    	{	return parseConstant(); }
    	else if (Character.isLetter(c))
    	{	return parseVariable(); }
    	else if (c == '(')
    	{	return parseParenthesis(); }
    	else if (c == '"')
    	{	return parseTextConstant(); }	
    	else
    	{	throw new InterpreterException("Unable to parse term"); }
    }
    
    private static Expression parseConstant()
    {
	    int n = 0;
	    while (Character.isDigit(input.charAt(position)))
	    {
		    n *= 10;
		    n += Character.getNumericValue(input.charAt(position));
		    position++;
	    }
	    return new Constant(n);
    }
    
    private static Expression parseTextConstant() throws InterpreterException
    {
    	String s = "";
    	
    	position++; 							// thanks to parseTerm pointer points at opening '"'
    	while (input.charAt(position) != '"')
	    {
		    s += input.charAt(position);
		    position++;
	    }
    	position++;
    	return new TextConstant(s);
    }
    
    private static Expression parseVariable()
    {
    	String s = "";
    	while (Character.isLetterOrDigit(input.charAt(position)))
    	{
    		s += input.charAt(position);
    		position++;
    	}
    	return new Variable(s);
    }
        
    private static Expression parseParenthesis() throws InterpreterException
    {
    	position++; 							// thanks to parseTerm pointer points at opening '('
    	Expression e = parseSum();
    	if (lookAhead() == ')')
    	{
    		position++;
    		return e;
    	}
    	else
    	{	throw new InterpreterException("ParseParenthesis() Error"); }
    }
    
    private static String parseIdentifier()
    {
    	String s = "";
    	while (Character.isLetterOrDigit(input.charAt(position)))
    	{
    		s += input.charAt(position);
    		position++;
    	}
    	return s;
    }
    
    // -- parseProgram helper methods --
    
    private static Program parseBlock() throws InterpreterException
    {
    	  Program p = parseInstruction();
    	  char c = lookAhead();
    	  while(c != '}' && c != Constants.EOT)
    	  {
    	    Program q = parseInstruction();
    	    p = new Composition(p, q);
    	    c = lookAhead();
    	  }
    	  return p; // can exit with position at EOT !!
    }
    
    private static Program parseInstruction() throws InterpreterException
    {
    	char c = lookAhead();
    	if (c == '{')
    	{
    		position++;
    		Program p = parseBlock();
    		if (lookAhead() == '}')
    		{
    			position++;
    			return p;
    		}
    		else
    		{	throw new InterpreterException("Closing brace expected"); }
    	}
    	else if (c == '"')						// creating new verse with default actor
    	{	return parseVerse(); }
    	else if (Character.isLetter(c))
    	{
    		String s = parseIdentifier();
    		
    		if (s.equals("read"))
    		{	return parseRead(); } 					// reading variable
    		else if (s.equals("if"))
    		{	return parseCondition(); }				// checking condition
    		else if (s.equals("else"))
    		{	return parseAltCondition(); }			// checking  alternative condition
    		else if (s.equals("while"))
    		{	return parseLoop(); } 					// beginning loop
    		else if (s.equals("skip"))
    		{	return new Skip(); } 					// skipping
    		else if (s.equals("define"))
    		{	return parseDefine(); } 				// defining game element
    		else if (s.equals("show"))
    		{	return parseShow(); }					// showing graphical element on screen
    		else if (s.equals("hide"))
    		{	return parseHide(); }					// hiding sprite already on screen
    		else if (s.equals("play"))
    		{	return parsePlay(); }					// playing sound file
    		else if (s.equals("stop"))
    		{	return parseStop(); }					// stopping to play sound file
    		else if (s.equals("label"))
    		{	return parseLabel();}					// processing label
    		else if (s.equals("jump"))
    		{	return parseJump();}					// processing jump
    		else if (s.equals("menu"))
    		{	return parseQuestion();}				// processing question verse
    		else if (memory.isDefinedComponent(s))		// creating new verse:
			{											// if verse starts with actor(?) name
				c = lookAhead();						// try to create new verse with actor(?)
				return parseVerse(s);					// as speaker(will be checked later) 
			}
    		return parseAssign(s);	 				// assigning identifier	
    	}
    	else
    	{	throw new InterpreterException("Identifier or a keyword expected"); }
    }

    private static Program parseRead() throws InterpreterException
    {
		char c = lookAhead();
		
		if (Character.isLetter(c))
		{
			String s = parseIdentifier();
			VerseInput i = new VerseInput(memory.getStory().getLast(), s, Analyzer.getPreviousLine()-1);
			return new AddVerse(i);
		}
		else
		{	throw new InterpreterException("Bad variable name"); }
	}
    
    private static Program parseCondition() throws InterpreterException
    {
    	String[] args;
    	int[] val;
    	Expression e;
		
		char c = lookAhead();
    	if (c == '(')
    	{
    		position++;
    		args = readConditions(3);
    		val = new int[2];
    		
    		// first argument
    		if (memory.isDefined(args[0]) )						// if read token is valid variable
    		{	val[0] = memory.read(args[0]).getInteger(); }	// get it's integer value
    		else												// if it's not variable
    		{ 
    			try												// try to treat it as it's integer
    			{ val[1] = Integer.parseInt(args[2]); }			
    			catch (NumberFormatException ex)				// but if it's just bad variable
    			{ throw new InterpreterException("Bad argument format");}	// inform
    		}
    		
    		//second argument    		
    		if (memory.isDefined(args[2]) )						// if read token is valid variable
    		{	val[1] = memory.read(args[2]).getInteger(); }		// get it's integer value
    		else												// if it's not variable
    		{ 
    			try												// try to treat it as it's integer
    			{	val[1] = Integer.parseInt(args[2]); }			
    			catch (NumberFormatException ex)			// but if it's just bad variable
    			{	throw new InterpreterException("Bad argument format");}	// inform
    		}
    		
    		e = new BinaryOperator(args[1].charAt(0),
    			new Constant(val[0]), new Constant(val[1]));
    		
    		System.out.println(val[0] + " " + args[1] + " " + val[1] + ": " + e.evaluate(memory).getBoolean());
    		
			elAllowed = 2;								// makes else allowed in next two instructions
    		
    		if (e.evaluate(memory).getBoolean() == false)	// condition not met:
  	      	{
    			elActive = true;						// makes "else" active in next parsed expression
    			Analyzer.analyze();						// skips next instruction(s) ("false" will be executed)
  	      	}
  			return new Skip();    			
		}
    	else
    	{	throw new InterpreterException("Logical expression in \"()\" expected"); }
    }
    
    private static Program parseAltCondition() throws InterpreterException
    {
    	if (elAllowed > 0 )
    	{ 
    		if (elActive)
    		{	elActive = false; }						// skips next instruction(s) (instructions for "false")
    		else
    		{	Analyzer.analyze(); }						// skips next instruction(s) ("true" was executed)
    	}							
    	else
    	{	throw new InterpreterException("Identifier \"else\" should follow \"if\" instruction block"); }
    	
    	return new Skip();	   
    }
    
    private static Program parseLoop() throws InterpreterException
    {    	
    	System.err.println("entered parseLoop()!");
    	
    	String[] args;
    	int[] val;
    	Expression e = null;
    			
		char c = lookAhead();
    	if (c == '(')
    	{
    		position++;
    		args = readConditions(3);
    		val = new int[2];
    		
    		// first argument
    		if (memory.isDefined(args[0]) )					// if read token is valid variable
    		{	val[0] = memory.read(args[0]).getInteger(); }	// get it's integer value
    		else											// if it's not variable
    		{ 
    			try											// try to treat it as it's integer
    			{	val[1] = Integer.parseInt(args[2]); }			
    			catch (NumberFormatException ex)			// but if it's just bad variable
    			{	throw new InterpreterException("Bad argument format");}	// inform
    		}
    		
    		//second argument    		
    		if (memory.isDefined(args[2]) )						// if read token is valid variable
    		{	val[1] = memory.read(args[2]).getInteger(); }	// get it's integer value
    		else												// if it's not variable
    		{ 
    			try												// try to treat it as it's integer
    			{	val[1] = Integer.parseInt(args[2]); }			
    			catch (NumberFormatException ex)				// but if it's just bad variable
    			{	throw new InterpreterException("Bad argument format");}	// inform
    		}
    		
    		e = new BinaryOperator(
    								args[1].charAt(0),
    								new Constant(val[0]),
    								new Constant(val[1])
    							  );
    		boolean ev = e.evaluate(memory).getBoolean();
    		
    		if (ev == true)					// condition met:
  	      	{	loopActive = true; }		// prepares jump after instructions block
    		else
    		{	Analyzer.analyze(); }		// skip looped instruction(s)
    		
  			return new Skip();
		}
		else
		{	throw new InterpreterException("Logical expression in \"()\" expected"); }
    }
    
    private static Program parseDefine () throws InterpreterException
    {
    	Program p = parseTypeIdetifier();
    	return new Define(p);
    }
       
    private static Program parseLabel() throws InterpreterException
    {
    	char c = lookAhead();
    	if (Character.isAlphabetic(c))
    	{
    		parseIdentifier();
    		return new Skip();
    	}
    	else
    	{	throw new InterpreterException ("Label name expected"); }
    }
    
    private static Program parseJump() throws InterpreterException
    {
    	char c = lookAhead();
    	if (Character.isAlphabetic(c))
    	{
    		String st = parseIdentifier();
    		int line = memory.getLabel(st);
    		Analyzer.jumpLine(line);
    		return new Skip();
    	}
    	else
    	{	throw new InterpreterException ("Label name expected"); }
    }
    
    private static Program parseAssign (String var) throws InterpreterException
    {
    	char c = lookAhead();
    	if (c == '=')
    	{
    		position++;
    		Expression e = parseSum();
    		return new Assign(var, e);
    	}
    	else
    	{	throw new InterpreterException ("Operator \"=\" expected"); }
    }
    
    
    // ===== Defining Game Elements =====
    
    private static Program parseTypeIdetifier() throws InterpreterException
    {
    	char c = lookAhead();
    	if (Character.isLetter(c))
    	{
    		String s = parseIdentifier();
    		
    		if (s.equals("actor"))
    		{	return parseActor(); }			// defining actor
    		
    		else if (s.equals("background"))
    		{	return parseBackground(); }		// defining background graphics
    		
    		else if (s.equals("sprite"))
    		{	return parseSprite(); }			// defining character sprite
    		
    		else if (s.equals("music"))
    		{	return parseMusic(); }			// defining music
    		
    		else if (s.equals("sound"))
    		{	return parseSound(); }			// defining sound
    		
    		else if (s.equals("voice"))
    		{	return parseVoice(); }			// defining voice
    
     		else
    		{	throw new InterpreterException("Unknown type identifier"); }
    	}
    	else
		{	throw new InterpreterException("Type identifier expected"); }
    }
    
    private static Program parseShow() throws InterpreterException
    {
    	char c = lookAhead();
    	if (Character.isLetter(c))
    	{
    		String s = parseIdentifier();
    		
    		if (s.equals("background"))
    		{	return showBackground(); }	// show background
    		
    		else if (s.equals("sprite"))
    		{	return showSprite(); }		// show character sprite
    
     		else
    		{	throw new InterpreterException("Unknown graphics type"); }
    	}
    	else
		{	throw new InterpreterException("Graphics type expected"); }
    }
    
    private static Program parseHide() throws InterpreterException
    {
    	char c = lookAhead();
    	if (Character.isLetter(c))
    	{
    		String s = parseIdentifier();
   		
    		if (s.equals("sprite"))
    		{	return hideSprite(); }		// hide character sprite
     		else
    		{	throw new InterpreterException("Unknown graphics type"); }
    	}
    	else
		{	throw new InterpreterException("Graphics type expected"); }
    }
    
    private static Program parsePlay() throws InterpreterException
    {
    	char c = lookAhead();
    	if (Character.isLetter(c))
    	{
    		String s = parseIdentifier();
    		
    		if (s.equals("music"))
    		{	return playMusic(); }		// play background music
    		
    		else if (s.equals("sound"))
    		{	return playSound(); }		// play sound effect
    		
    		else if (s.equals("voice"))
    		{	return playVoice(); }		// play character voice dubbbing
    
     		else
    		{	throw new InterpreterException("Unknown sound type"); }
    	}
    	else
		{	throw new InterpreterException("Sound type expected"); }
    }
    
    private static Program parseStop() throws InterpreterException
    {
    	char c = lookAhead();
    	if (Character.isLetter(c))
    	{
    		String s = parseIdentifier();
    		
    		if (s.equals("music"))
    		{	SoundManager.stopMusic(); }		// stop background music
     		else
    		{	throw new InterpreterException("Unknown sound type"); }
    	}
    	else
		{	throw new InterpreterException("Sound type expected"); }
    	
    	return new Skip();
    }
  
	private static Program parseActor() throws InterpreterException
	{
		String[] args;
		
		char c = lookAhead();
    	if (c == '(')
    	{
    		position++;
    		args = readArguments(3);
    		return new AddDefinition(new Actor(args[0],args[1],args[2]));
		}
    	else
    	{	throw new InterpreterException("Arguments list in \"()\" expected"); }
	}
    
    private static Program parseBackground() throws InterpreterException
    {
    	String[] args;
		
		char c = lookAhead();
    	if (c == '(')
    	{
    		position++;
    		args = readArguments(2);
    		return new AddDefinition(new Background(args[0],args[1]));
		}
    	else
    	{	throw new InterpreterException("Arguments list in \"()\" expected"); }
	}
    
    private static Program parseSprite() throws InterpreterException
    {
    	String[] args;
		
		char c = lookAhead();
    	if (c == '(')
    	{
    		position++;
    		args = readArguments(2);
    		return new AddDefinition(new Sprite(args[0],args[1]));
		}
    	else
    	{	throw new InterpreterException("Arguments list in \"()\" expected"); }
	}

    // szymi est niemaderkiem a nie madzi. o. 
    
	private static Program parseMusic() throws InterpreterException
	{
		String[] args;
		
		char c = lookAhead();
		if (c == '(')
		{
			position++;
			args = readArguments(2);
			return new AddDefinition(new Music(args[0],args[1]));
		}
		else
		{	throw new InterpreterException("Arguments list in \"()\" expected"); }
	}
	
	private static Program parseSound() throws InterpreterException
	{
		String[] args;
		
		char c = lookAhead();
		if (c == '(')
		{
			position++;
			args = readArguments(2);
			return new AddDefinition(new Sound(args[0],args[1]));
		}
		else
		{	throw new InterpreterException("Arguments list in \"()\" expected"); }
	}
	
	private static Program parseVoice() throws InterpreterException
	{
		String[] args;
		
		char c = lookAhead();
		if (c == '(')
		{
			position++;
			args = readArguments(2);
			return new AddDefinition(new Voice(args[0],args[1]));
		}
		else
		{	throw new InterpreterException("Arguments list in \"()\" expected"); }
	}

	private static Program parseVerse() throws InterpreterException
	{
		String s = "";
		String r = "";
    	
    	position++; 							// thanks to parseTerm pointer points at opening '"'
    	while (input.charAt(position) != '"' && input.charAt(position) != Constants.EOT)
	    {
    		if (input.charAt(position) == '$')
    		{
    			position++;
    			while (input.charAt(position) != '"' && input.charAt(position) != Constants.EOT && Character.isLetterOrDigit(input.charAt(position)))
    		    {	
    				r+= input.charAt(position);
    				position++;
    			}
    			
    			Data d = memory.read(r);
    			
    			if (d != null)
    			{	s += d.getText(); }
    			else
    			{	s += "$" + r; }    			
    		}
    		else
    		{
			    s += input.charAt(position);
			    position++;
    		}
	    }
    	if (input.charAt(position) == Constants.EOT)	//  EOT encountered
    	{	throw new InterpreterException("Closing quotation mark expected"); }
    	else								// verse ended with '"'
    	{
    		position++;
    		Verse v = new Verse(s); 
    		return new AddVerse(v);
    	}
	}
	
	private static Program parseVerse(String id) throws InterpreterException
	{
		String s = "";
		String r = "";
		
    	position++; 						// thanks to parseTerm pointer points at opening '"'
    	while (input.charAt(position) != '"' && input.charAt(position) != Constants.EOT)
	    {
    		if (input.charAt(position) == '$')
    		{
    			position++;
    			while (input.charAt(position) != '"' && input.charAt(position) != Constants.EOT && Character.isLetterOrDigit(input.charAt(position)))
    		    {	
    				r+= input.charAt(position);
    				position++;
    			}
    			
    			Data d = memory.read(r);
    			
    			if (d != null)
    			{	s += d.getText(); }
    			else
    			{	s += "$" + r; }    			
    		}
    		else
    		{
			    s += input.charAt(position);
			    position++;
    		}
	    }
    	if (input.charAt(position) == Constants.EOT)	//  EOT encountered
    	{	throw new InterpreterException("Closing quotation mark expected"); }
    	else								// verse ended with "\""
    	{
    		position++;
    		
    		try
    		{
    			Actor a = (Actor)(memory.read(id).getElement()); 
    			Verse v = new Verse(a, s); 
        		return new AddVerse(v);
    		}
    		catch (Exception e)
    		{	throw new InterpreterException("Component is not an actor!"); }
    	}
	}
	
	private static Program parseQuestion() throws InterpreterException
	{
		String[] ans;				// array of all arguments read
		ArrayList<StoryData> opt;	// array list of answer-instruction pairs
    	VerseOption v;				// verse to be added to story
    	
    	char c = lookAhead();
    	if (c == '(')
    	{
    		int optc = 0;
    		
    		position++;
    		  		
    		ans = readArguments();
    		opt = new ArrayList<StoryData>();
    		
	    	if(ans.length > 0 && ans.length % 2 == 0)
	    	{
	    		for(int i = 0; i < ans.length; i += 2)
	    		{	opt.add(new StoryData(ans[i], ans[i+1])); optc ++; }
	    	}
	    	else
	    	{	throw new InterpreterException("Bad number of arguments"); }
	    	
	    	if(memory.getStory().getLast() != null)
	    	{	
	    		v = new VerseOption
	    		(
					memory.getStory().getLast(),
					opt.toArray(new StoryData[optc]),
					Analyzer.getPreviousLine()-1
	    		);
	    	}
	    	else
	    	{	throw new InterpreterException("No menu without question allowed"); } 	
    	}
		else
		{	throw new InterpreterException("Options list in \"()\" expected"); }

   		return new AddVerse(v);
	}
	
	private static Program showBackground() throws InterpreterException
    {
    	String[] args;
		
		char c = lookAhead();
    	if (c == '(')
    	{
    		position++;
    		args = readArguments(1);
    		return new ShowBackground(args[0]);
		}
    	else
    	{	throw new InterpreterException("Arguments list in \"()\" expected"); }
	}
    
    private static Program showSprite() throws InterpreterException
    {
    	String[] args;
		
		char c = lookAhead();
    	if (c == '(')
    	{
    		position++;
    		args = readArguments(2);
    		return new ShowSprite(args[0],args[1]);
		}
    	else
    	{	throw new InterpreterException("Arguments list in \"()\" expected"); }
	}
    
    private static Program hideSprite() throws InterpreterException
    {
    	String[] args;
		
		char c = lookAhead();
    	if (c == '(')
    	{
    		position++;
    		args = readArguments(1);
    		return new HideSprite(args[0]);
		}
    	else
    	{	throw new InterpreterException("Arguments list in \"()\" expected"); }
    }
    
    private static Program playMusic() throws InterpreterException
    {
    	String[] args;
		
		char c = lookAhead();
    	if (c == '(')
    	{
    		position++;
    		args = readArguments(1);
    		return new PlaySound(0, args[0]);
		}
    	else
    	{	throw new InterpreterException("Arguments list in \"()\" expected"); }
	}
    
    private static Program playSound() throws InterpreterException
    {
    	String[] args;
		
		char c = lookAhead();
    	if (c == '(')
    	{
    		position++;
    		args = readArguments(1);
    		return new PlaySound(1, args[0]);
		}
    	else
    	{	throw new InterpreterException("Arguments list in \"()\" expected"); }
	}
    
    private static Program playVoice() throws InterpreterException
    {
    	String[] args;
		
		char c = lookAhead();
    	if (c == '(')
    	{
    		position++;
    		args = readArguments(1);
    		return new PlaySound(2, args[0]);
		}
    	else
    	{	throw new InterpreterException("Arguments list in \"()\" expected"); }
	}
}
