package interpreter;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import datatypes.Memory;
import engine.Constants;
import interpreter.exceptions.InterpreterException;

public final class Analyzer
{
	private static boolean	initd;				// flag determined if Analyzer is already initialized
	private static String 	path;				// path to analysed source file
	private static Memory 	memory;	 			// memory containing list of labels used in current source file
	
	private static InputStreamReader input;		// the source file on which the analyzer operates  
	private static BufferedReader 	 reader;	// the reader which operates on the source file
	
	private static String output 	 = "";		// analysed output
	private static int actualLine	 = 1;		// actual line number
	private static int previousLine	 = 1;		// previous line number
	private static int loopFirstLine = 0;		// last loop starting line
		
	private static class Term					// single word, used by analyser to process arguments list
	{
		private int 	end;					// end line of the term
		private String 	term;					// content of the term
		
		protected Term (int e, String t)
		{ end = e;	term = t; }
		
		protected int getEnd()
		{ return end; }
		
		protected String getTerm()
		{ return term; }
	}
	
	// === INITIALIZATION ===
	/** Initializes the <code>Analyzer</code> before the first use.
	 * @param p - path to the input file
	 * @param m - memory to be used by the analyzer */
	public static void init(String p, Memory m) throws InterpreterException
	{
		initd = false;		// reset initialization flag
		
		setPath(p);
		setMemory(m);
		resetReader();
		
		initd = true;		// if the whole procedure to this point went as planned, consider initialized
	}

	// === SETTERS ===	
	public static void setPath(String p)
	{ path = p; }
	
	public static void setMemory(Memory m)
	{ memory = m; }
	
	// === GETTERS ===
	public static boolean isInitialized()
	{ return initd; }
	
	public static String getOutput()
	{ return output; }
	
	public static int getActualLine()
	{ return actualLine; }
	
	public static int getPreviousLine()
	{ return previousLine; }
	
	public static Term loadTerm(String input, int pos)
	{
		while (pos < input.length() && input.charAt(pos) == ' ')	// remove leading whitespace characters
		{ pos++; }
		
    	String buffer = "";
    	
    	while (pos < input.length() && Character.isLetterOrDigit(input.charAt(pos))) 	// get next word from a line 
    	{																				// (if there's no word, buffer should be empty)
    		buffer += input.charAt(pos);
    		pos++;
    	}
    	
    	return new Analyzer.Term(pos, buffer);
	}
	
	// === METHODS ===
	/** Skip the specified number of lines in input file. */
	public static void skipLines(int n)
	{
		for (int i = 0; i < n; i++)
		{
			try 
			{ reader.readLine(); }
			catch (IOException e)
			{ e.printStackTrace(); }
		}
		actualLine += n;
	}
	
	/** Resets reader position to the beginning of input file. 
	 * @throws InterpreterException **/
	public static void resetReader() throws InterpreterException
	{
		try
		{
			input = new InputStreamReader(new FileInputStream(path), "windows-1250");
			reader = new BufferedReader(input);
			actualLine = 1;
			
			String ln;
			
			while ((ln = reader.readLine()) != null && !ln.contains("%GAME%"))
			{	actualLine++; }
			
			if (ln == null)
			{ throw new InterpreterException("No game code beginning marker found!"); }
		}
		catch (IOException e)
		{	throw new InterpreterException("Unexpected behaviour during reading source file!");	}
		previousLine = actualLine;
	}
	
	/** Sets reader position to the specified line.
	 * @param n - number of line to which reader position should be set */
	public static void jumpLine(int n)
	{
		int toSkip = n;
		
		if ( actualLine > n)		// destined line is behind current position
		{
			try
			{
				input = new InputStreamReader(new FileInputStream(path), "windows-1250");
				reader = new BufferedReader(input);
				actualLine = 1;
				
				while (!reader.readLine().contains("%GAME%") && !reader.readLine().contains("" + Constants.EOT))
				{	actualLine++; }
			}
			catch (IOException e)
			{ e.printStackTrace(); }
		}
		else						// destined line is ahead of current position
		{ toSkip -= actualLine; }
		
		for (int i = 0; i < toSkip; i++)
		{
			try 
			{ reader.readLine(); }
			catch (IOException e)
			{ e.printStackTrace(); }
		}
		actualLine = previousLine = n;
	}

	/** Sets the reader position to the first line of the loop. */
	public static void jumpLoopLine()
	{ jumpLine(loopFirstLine);	}
	
	/** Prepares a list of all labels in input file together with their corresponding line numbers. */
	public static void listLabels() throws InterpreterException
	{
		try
		{
			Term 	term;
			String 	word 		= "";
			String 	line		= "";
			int 	position	= 0;
			
			while ((line = reader.readLine()) != null)			// as long as source has lines
			{
				term 		= loadTerm(line, position);
				word 		= term.getTerm();
		    	position	= term.getEnd();
				
		    	if (word.equals("label"))						// when line begins with word "label"
		    	{
					term = loadTerm(line, position);
					word = term.getTerm();
					if (!word.equals(""))
					{ memory.addLabel(word, actualLine); }
					else
					{ throw new InterpreterException("Label should have proper name"); }
		    	}
		    	
		    	position = 0;
		    	actualLine++;
			}
		}
		catch (IOException e)
		{ e.printStackTrace(); }
	}
	
	/** Analyzes the input file in order to get a logical instruction possible to be parsed as one. */
	public static void analyze() 
	{	
		previousLine = actualLine;								// first verse, in which block begins
		
		String code = "";
		String line = "";

		try
		{			
			boolean block = true;				// unclosed block
			boolean unclosedQ = false;			// unclosed quotation
			int unclosedP = 0;					// number of unclosed parenthesis
			int unclosedB = 0;					// number of unclosed braces
			
			do
			{	
				line = reader.readLine();
				if (line == null)
				{ 
					code = null;
					block = false;
				}
				else
				{								
					for (char c : line.toCharArray() )
				    {			
						switch (c)
						{
							case '(':
					    		if (!unclosedQ) unclosedP++; block = true; break;				    		
							case '{':
								if (!unclosedQ) unclosedB++; block = true; break;				    	
							case ')':
								if (!unclosedQ) unclosedP--; break;		    		
					    	case '}':
					    		if (!unclosedQ) unclosedB--; break;
					    	case '"':							
					    		unclosedQ = !unclosedQ;	break;
						}
				    }
				    	
				    if (unclosedP == 0 && unclosedB == 0 && unclosedQ == false) // no next verse in block
			    	{		
				    	if (line.contains("%%"))								// if line contains comment
				    	{ line = line.substring(0, line.indexOf("%%"));	}		// ignore the comment part 
				    	
				    	if (line.trim().length() > 0) 		// if last line is not whitespace-only or empty
		    			{
		    				code += line; 					// add current line to code
				    		block = false;					// and end block
		    			}
				    					    	
				    	if (line.contains("while") && !unclosedQ)		// if "while" not in a verse
				    	{ loopFirstLine = previousLine; }									// store this line's number
				    	
				    	if (line.contains("menu") && !unclosedQ)		// if "menu" not in a verse
				    	{ block = true; }								// treat as part of block
			    	}
				    else									// if current line is part of incomplete block
			    	{
				    	if (line.trim().length() > 0) 		// and last line is not whitespace-only or empty
		    			{ code += line + " "; }				// add current line to code
			    	}
				    actualLine++;
				}
			} while (block == true);
			
			System.out.println("++ Analysed line no " + actualLine + ": " + code);
		}
		catch (IOException e)
		{ e.printStackTrace(); }
		output = code;
	}
	
	/** Closes the reader if it's open. */
	public static void terminate()
	{
		try
		{
			if (reader != null)
			{ reader.close(); }
		}
		catch (IOException e)
		{ e.printStackTrace(); }
	}
}