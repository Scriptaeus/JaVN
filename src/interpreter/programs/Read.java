package interpreter.programs;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import datatypes.Data;
import datatypes.Memory;
import interpreter.exceptions.InterpreterException;


public class Read extends Program
{
	static BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
	
	private String var;
	 
	public Read(String v)
	{
		this.var = v;		
	}
	
	@Override
	public void evaluate(Memory m) throws InterpreterException
	{
		int in = 0;
		
		try
		{
			System.out.print("> ");
			in = Integer.parseInt(reader.readLine());
		}
		catch(IOException io)
		{
			throw new InterpreterException("Variable \"" + var + "\"can't be read");
		}
		
	    m.write(var, new Data(in));
	}
	
	@Override
	public char type()
	{ return 'o'; }
}
