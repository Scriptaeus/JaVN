package interpreter.programs;

import datatypes.Memory;
import engine.logics.elements.Verse;
import interpreter.exceptions.InterpreterException;

public class AddVerse extends Program
{
	Verse verse;
	
	public AddVerse(Verse v)
	{ this.verse = v;	}
	
	@Override
	public void evaluate(Memory m) throws InterpreterException
	{ verse.addToStory(m); }

	@Override
	public char type()								// v when it's normal verse, q when it's question verse
	{ return verse.getQ() == false ? 'v' : 'q'; }		
}