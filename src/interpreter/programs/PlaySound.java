package interpreter.programs;

import datatypes.Memory;
import engine.LogicManager;
import engine.logics.elements.Verse;
import engine.sounds.SoundManager;
import engine.sounds.elements.Music;
import engine.sounds.elements.Sound;
import engine.sounds.elements.Voice;
import interpreter.exceptions.InterpreterException;
import interpreter.programs.Program;

public class PlaySound extends Program
{
	int		type;
	String 	name;
	
	public PlaySound(int t, String n)
	{	
		this.type = t;
		this.name = n;
	}

	@Override
	public void evaluate(Memory m) throws InterpreterException
	{ 
		if (m.isDefined(name))											// 1) Variable name must be defined 
		{
			if (m.read(name).getType() == 'M')							// 2) Variable must be of type "Memorizable"
			{
				try														// 3) Variable must be of one from sound classes 
				{	
					if 		(type == 0)		// music type
					{
						Music mu = (Music)(m.read(name).getElement());
						
						Verse v = LogicManager.readActualVerse();
						if (v != null && SoundManager.isMusicChanged() == false)	// if there is at least one verse
						{	v.setMusicChange(m.getActiveMusic()); }		// bound old music to the last verse using it 
						SoundManager.toggleMusicChanged(true);			// prepare to bound new music to the next verse
						
						m.setActiveMusic(mu); 							// Sets music to be played as actual
					}
					else if (type == 1)		// sound type
					{
						Sound s = (Sound)(m.read(name).getElement());
						m.setActiveSound(s); 							// Sets sound to be played as actual
						SoundManager.stopSound(); 						// Stops currently playing sound
						SoundManager.playSound(s.getPath());			// Plays the sound specified in argument
					}
					else if (type == 2)		// voice type
					{
						Voice v = (Voice)(m.read(name).getElement());
						m.setActiveVoice(v); 							// Sets voice to be played as actual
						SoundManager.stopVoice(); 						// Stops currently playing voice
						SoundManager.playVoice(v.getPath());			// Plays the voice specified in argument
					}
				}
				catch (ClassCastException e)
				{ 	throw new InterpreterException("Sound file not of expected type"); }
			}
			else 
			{ throw new InterpreterException("Data not of the Memorizable type"); }			
		}
		else
		{ throw new InterpreterException("Background not defined"); }
		
	}		

	@Override
	public char type()
	{ return 'o'; }	
}
