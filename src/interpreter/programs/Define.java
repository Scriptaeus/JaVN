package interpreter.programs;

import datatypes.Memory;
import interpreter.exceptions.InterpreterException;

public class Define extends Program
{
	private Program def;
	
	public Define (Program d)
	{
		this.def = d;
	}

	@Override
	public void evaluate(Memory m) throws InterpreterException
	{
		//System.out.println("defining");
		def.evaluate(m);
	}
	
	@Override
	public char type()
	{ return 'o'; }

}
