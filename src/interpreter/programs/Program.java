package interpreter.programs;

import datatypes.Memory;
import interpreter.exceptions.InterpreterException;

abstract public class Program
{
	public abstract void evaluate(Memory m) throws InterpreterException;
	
	/**
	 * Program sub-class type, which may be:
	 *  v - AddVerse;
	 *  d - AddDefinition;
	 *  o - all other operations;
	 *  
	 * @return type of current sub-class 
	 */
	
	public abstract char type();
}
