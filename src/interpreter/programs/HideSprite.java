package interpreter.programs;

import interpreter.exceptions.InterpreterException;
import interpreter.programs.Program;
import datatypes.Memory;
import engine.Constants;
import engine.LogicManager;
import engine.graphics.GraphicsManager;
import engine.logics.elements.Verse;

public class HideSprite extends Program
{
	int 	position;
	
	public HideSprite(String pos)
	{	
		if 		(pos.equals("left"))
		{	position = Constants.POS_LEFT; }
		else if	(pos.equals("right"))
			{position = Constants.POS_RIGHT; }
		else if	(pos.equals("center"))
		{	position = Constants.POS_CENTER; }
		else
		{	position = Constants.POS_UNDEFINED; }
	}

	@Override
	public void evaluate(Memory m) throws InterpreterException
	{	
		Verse v = LogicManager.readActualVerse();
		if (v != null && GraphicsManager.areSpritesChanged() == false)		// if there is at least one verse
		{		v.setSpritesChange(m.getActiveSprites()); }		// bound old sprites to the last verse using it
		GraphicsManager.toggleSpritesChanged(true);				// prepare to bound new sprites to next verse
		
		m.setActiveSprite(position, null);
	}

	@Override
	public char type()
	{ return 'o'; }	
}
