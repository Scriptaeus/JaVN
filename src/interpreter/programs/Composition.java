package interpreter.programs;

import datatypes.Memory;
import interpreter.exceptions.InterpreterException;

public class Composition extends Program
{
	private Program left, right;
	
	public Composition(Program l, Program r)
	{
		this.left = l;
		this.right = r;		
	}
	 
	@Override
	public void evaluate(Memory m) throws InterpreterException
	{
	      left.evaluate(m);
	      right.evaluate(m);
	}
	
	@Override
	public char type()
	{ return 'o'; }
}
