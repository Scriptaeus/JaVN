package interpreter.programs;

import interpreter.exceptions.InterpreterException;
import interpreter.programs.Program;

import datatypes.Memory;
import engine.Constants;
import engine.LogicManager;
import engine.graphics.GraphicsManager;
import engine.graphics.elements.Sprite;
import engine.logics.elements.Verse;

public class ShowSprite extends Program
{
	String 	name;
	int 	position;
	
	public ShowSprite(String n, String pos)
	{	
		this.name = n;
		
		if 		(pos.equals("left"))
		{	position = Constants.POS_LEFT; }
		else if	(pos.equals("right"))
			{position = Constants.POS_RIGHT; }
		else if	(pos.equals("center"))
		{	position = Constants.POS_CENTER; }
		else
		{	position = Constants.POS_UNDEFINED; }
	}

	@Override
	public void evaluate(Memory m) throws InterpreterException
	{ 
		if (m.isDefined(name))												// 1) Variable name must be defined 
		{
			if (m.read(name).getType() == 'M')								// 2) Variable must be of type "Memorizable"
			{
				try															// 3) Variable must be a Sprite
				{
					Sprite s = (Sprite)(m.read(name).getElement());
					
					Verse v = LogicManager.readActualVerse();
					if (v != null && GraphicsManager.areSpritesChanged() == false)		// if there is at least one verse
					{	v.setSpritesChange(m.getActiveSprites()); }			// bound old sprites to the last verse using it
					GraphicsManager.toggleSpritesChanged(true);				// prepare to bound new sprites to next verse
					
					m.setActiveSprite(position, s);					
				}
				catch (ClassCastException e)
				{ 	throw new InterpreterException(e.getMessage()); }
			}
			else 
			{ throw new InterpreterException("Data not of the sprite type"); }			
		}
		else
		{ throw new InterpreterException("Sprite not defined"); }		
	}		

	@Override
	public char type()
	{ return 'o'; }	
}
