package interpreter.programs;

import interpreter.exceptions.InterpreterException;
import interpreter.programs.Program;

import datatypes.Memory;
import engine.LogicManager;
import engine.graphics.GraphicsManager;
import engine.graphics.elements.Background;
import engine.logics.elements.Verse;

public class ShowBackground extends Program
{
	String name;
	
	public ShowBackground(String n)
	{	this.name = n; }

	@Override
	public void evaluate(Memory m) throws InterpreterException
	{ 
		if (m.isDefined(name))												// 1) Variable name must be defined 
		{
			if (m.read(name).getType() == 'M')								// 2) Variable must be of type "Memorizable"
			{
				try															// 3) Variable must be a Background
				{	
					Background b = (Background)(m.read(name).getElement());
					
					Verse v = LogicManager.readActualVerse();
					if (v != null && GraphicsManager.isBackgroundChanged() == false)											// if there is at least one verse
					{	v.setBackgroundChange(m.getActiveBackground()); }	// bound old background to last verse using it
  					GraphicsManager.toggleBackgroundChanged(true);			// prepare to bound new background to next verse
		
					m.setActiveBackground(b);								// Set background as the active one						
				}
				catch (ClassCastException e)
				{ 	throw new InterpreterException(e.getMessage()); }
			}
			else 
			{ throw new InterpreterException("Data not of the background type"); }			
		}
		else
		{ throw new InterpreterException("Background not defined"); }
		
	}		

	@Override
	public char type()
	{ return 'o'; }	
}
