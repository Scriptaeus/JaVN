package interpreter.programs;

import datatypes.Memorizable;
import datatypes.Memory;
import interpreter.exceptions.InterpreterException;

public class AddDefinition extends Program
{
	Memorizable mem;
	 
	public AddDefinition(Memorizable m)
	{	this.mem = m; }

	@Override
	public void evaluate(Memory m) throws InterpreterException
	{	mem.define(m); }

	@Override
	public char type()
	{ return 'd'; }	
}
