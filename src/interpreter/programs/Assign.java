package interpreter.programs;

import datatypes.Memory;
import interpreter.exceptions.InterpreterException;
import interpreter.expressions.Expression;

public class Assign extends Program
{
	String var;
	Expression ex;
	 
	public Assign(String v, Expression e)
	{
		this.var = v;
		this.ex = e;
	}

	
	@Override
	public void evaluate(Memory m) throws InterpreterException
	 {
		m.write(var, ex.evaluate(m));
	 }

	@Override
	public char type()
	{ return 'o'; }
	
}
