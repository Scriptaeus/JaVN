package interpreter.programs;

import datatypes.Memory;

public class Skip extends Program
{
	@Override
	public void evaluate(Memory m)	{}
	
	@Override
	public char type()
	{ return 'o'; }

}
