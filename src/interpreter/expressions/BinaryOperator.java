package interpreter.expressions;

import datatypes.Data;
import datatypes.Memory;
import interpreter.exceptions.InterpreterException;

public class BinaryOperator extends Expression
{
	private char symbol;
	private Expression left, right;
	
	public BinaryOperator(char s, Expression l, Expression r)
	{
		this.symbol = s;
		this.left = l;
		this.right = r;
	}
	
	@Override
	public Data evaluate(Memory m) throws InterpreterException
	{
		switch(symbol)
		{
			case '+':
				if (left.evaluate(m).getType() == 'T')
				{	return new Data(left.evaluate(m).getText() + right.evaluate(m).getText()); }
				else
				{	return new Data(left.evaluate(m).getInteger() + right.evaluate(m).getInteger()); }
			case '-':
				return new Data(left.evaluate(m).getInteger() - right.evaluate(m).getInteger());
			case '*':
				return new Data(left.evaluate(m).getInteger() * right.evaluate(m).getInteger());
			case '/':
				return new Data(left.evaluate(m).getInteger() / right.evaluate(m).getInteger());
			case '%':
				return new Data(left.evaluate(m).getInteger() % right.evaluate(m).getInteger());
			case '<':
				return new Data(left.evaluate(m).getInteger() < right.evaluate(m).getInteger());
			case '>':
				return new Data(left.evaluate(m).getInteger() > right.evaluate(m).getInteger());
			case '=':
				return new Data(left.evaluate(m).getInteger() == right.evaluate(m).getInteger());
			case '&':
				return new Data(left.evaluate(m).getBoolean() && right.evaluate(m).getBoolean());
			case '|':
				return new Data(left.evaluate(m).getBoolean() || right.evaluate(m).getBoolean());
			default:
				return new Data(0);
		}		
	}
	
}
