package interpreter.expressions;

import datatypes.Data;
import datatypes.Memory;
import interpreter.exceptions.InterpreterException;

public class Variable extends Expression
{
	private String name;

	public Variable(String n)
	{
		this.name = n;
	}
	
	@Override
	public Data evaluate(Memory m) throws InterpreterException
	{
		if (m.isDefined(name))
		{
			return m.read(name);
		}
		else
		{
			throw new InterpreterException("Variable \"" + name + "\" not defined!");
		}		
	}
}
