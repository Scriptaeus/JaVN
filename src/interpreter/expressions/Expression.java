package interpreter.expressions;

import datatypes.Data;
import datatypes.Memory;
import interpreter.exceptions.InterpreterException;

public abstract class Expression
{
	public abstract Data evaluate(Memory m) throws InterpreterException;
}
