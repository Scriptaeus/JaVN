package interpreter.expressions;

import datatypes.Data;
import datatypes.Memory;
import interpreter.exceptions.InterpreterException;

public class Constant extends Expression
{
	private int value;
	public Constant(int v)
	{
		this.value = v;
	}
	
	@Override
	public Data evaluate(Memory m) throws InterpreterException
	{
		return new Data(value);
	}
}
