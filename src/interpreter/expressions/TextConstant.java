package interpreter.expressions;

import datatypes.Data;
import datatypes.Memory;
import interpreter.exceptions.InterpreterException;

public class TextConstant extends Expression
{
	private String value;
	
	public TextConstant (String v)
	{
		this.value = v;
	}
	
	@Override
	public Data evaluate(Memory m) throws InterpreterException
	{
		return new Data(value);
	}
}