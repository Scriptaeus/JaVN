package interpreter;

import datatypes.Memory;
import engine.Constants;
import engine.LogicManager;
import engine.logics.elements.Verse;
import interpreter.exceptions.InterpreterException;
import interpreter.programs.Program;

public class Interpreter
{
	private Memory 	memory;
	private String 	path;
	private boolean intStop = false;						// true when question is processing

	// === CONSTRUCTOR ===	
	public Interpreter(String path)
	{	this.path = path;	}
	
	// === INITIALIZATION ===
	/** Initializes the instance of <code>Interpreter</code> before the first use after starting a new interpretation. */
	public void initialize()
	{		
		memory = new Memory(this);							// prepare clear memory 
	
		try
		{
			Parser.init(memory);							// initialize parser
			Analyzer.init(path, memory);					// initialize analyzer 
			Analyzer.listLabels();							// map all the labels in source 
			Analyzer.resetReader();							// make sure that reader is set to beginning of source
			resume();										// analyze first line of code
		}
		catch (InterpreterException e)
		{	
			LogicManager.addVerse(new Verse
			(
				Constants.ACTOR_ERROR,
				"Error occured during interpreter initialization.\n" + "    " + e.getDescription())
			);
		}		
	}
	
	/** Initializes the instance of <code>Interpreter</code> before the first use after resuming stopped interpretation. */
	public void initializeLoaded()
	{
		try
		{
			Parser.init(memory);								// initialize parser
			Analyzer.init(path, memory);						// initialize analyzer 
			Analyzer.jumpLine(memory.getStoryLoadLine());		// make sure that reader is set to the proper line	
			resume();											// analyze proper line of code
		}
		catch (InterpreterException e)
		{	
			LogicManager.addVerse(new Verse
			(
				Constants.ACTOR_ERROR,
				"Error occured during interpreter initialization.\n" + "    " + e.getDescription())
			);
		}	
	}
	
	// === GETTERS ===
	public Verse getStoryActualVerse()
	{ 	return memory.storyActualVerse(); }
		
    public Verse getStoryFirstVerse()
    { 	return memory.storyFirstVerse();}
    
    public Verse getStoryLastVerse()
    { 	return memory.storyLastVerse();}
    
    public Verse getStoryNextVerse()
    { 	return memory.storyNextVerse(); }
    
    public Verse getStoryPreviousVerse()
    { 	return memory.storyPreviousVerse(); }

    public int getStorySize()
    { 	return memory.getStorySize(); }

    public Memory getMemory()
    { 	return memory; }
    
    // === SETTERS ===
    public void setMemory(Memory m)
    {	memory = m; }
    
    // === METHODS ===
	/** Interprets the code from the input file until the verse is found. 
	 * @throws InterpreterException */
	public void process() throws InterpreterException
	{
		if (!intStop && Analyzer.isInitialized())
		{
			Program p;
			boolean q = false;
	
			if (Analyzer.getOutput() != null)
			{
				do
				{
					Parser.setInput(Analyzer.getOutput()); 		// setting input to analyzer output
					p = Parser.parseProgram();
					p.evaluate(memory);
					
					if (p.type() == 'q')
					{	
						q = true;
						intStop = true;
					}
					else
					{	Analyzer.analyze(); }
					
				} while (Analyzer.getOutput() != null && p.type() != 'v' && !q);
			}
		}
	}
		
	/** Resets the state of the flag indicating that interpretation is halted. */
	public void resume()
	{	
		Analyzer.analyze();
		intStop = false;
	}
   
	/** Adds the specified verse to the <code>Story</code>.
	 * @param v - verse to be added to the story */
	public void add(Verse v)
	{	memory.getStory().add(v); }
	
	/** Removes the last verse in the code>Story</code>. */
	public void remove()
	{	memory.getStory().remove(); }
	
	/** Removes the first verse from the story in order to limit rewinding reach. */
	public void rebase()
	{	memory.getStory().rebase(); }
	
	/** Sets the next verse in the code>Story</code> as actual. */
	public Verse moveToNextVerse()									// forward the story to the next verse	
	{	return memory.storyForward(); }															
	
	/** Sets the previous verse in the code>Story</code> as actual. */
	public Verse moveToPreviousVerse() throws InterpreterException	// rewinds the story to the previous verse
	{ 
		Verse v = memory.storyBackward();
		/*
		if (v != null && v.getQ())				// if previous verse is a question dialog
		{
			Analyzer.jumpLine(v.getLineNr()); 	// set analyzer to that verse
			Analyzer.analyze();			 		// analyze it once again for future use
			
			v = memory.storyBackward();			// then rewind the story to question verse
			memory.cutStoryBranch();			// and cut part of branch following it 		
		}*/
		return v;
	}
	
	/** Sets the last verse in the code>Story</code> as actual. */
	public Verse moveToLastVerse()									// forward the story to the next verse	
	{	return memory.storyToEnd(); }	
}
